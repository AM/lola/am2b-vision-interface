cmake_minimum_required(VERSION 3.10)

set(META_PROJECT_NAME        "am2b-vision-interface")
set(META_PROJECT_DESCRIPTION "Interface between control system and vision system")
set(META_AUTHOR_ORGANIZATION "Chair of Applied Mechanics, Technical University of Munich")
set(META_AUTHOR_DOMAIN       "https://www.mec.ed.tum.de/am")
set(META_AUTHOR_MAINTAINER   "code@amm.mw.tum.de")
set(META_VERSION_MAJOR       "2")
set(META_VERSION_MINOR       "5")
set(META_VERSION_PATCH       "0")
set(META_VERSION             "${META_VERSION_MAJOR}.${META_VERSION_MINOR}.${META_VERSION_PATCH}")
set(META_NAME_VERSION        "${META_PROJECT_NAME} v${META_VERSION} (${META_VERSION_REVISION})")

project(${META_PROJECT_NAME})

add_library(${PROJECT_NAME} INTERFACE)

target_include_directories(${PROJECT_NAME} INTERFACE
                                           $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
                                           $<INSTALL_INTERFACE:include>)

include(CMakePackageConfigHelpers)
write_basic_package_version_file("${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
                                 VERSION ${META_VERSION}
                                 COMPATIBILITY SameMajorVersion)
configure_file(cmake/${PROJECT_NAME}Config.cmake.in
               ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Config.cmake
               COPYONLY)

install(TARGETS ${PROJECT_NAME}
        EXPORT ${PROJECT_NAME}_Targets)

install(DIRECTORY ${PROJECT_SOURCE_DIR}/include/${PROJECT_NAME} DESTINATION include)

install(EXPORT ${PROJECT_NAME}_Targets
        FILE ${PROJECT_NAME}Targets.cmake
        DESTINATION lib/cmake/${PROJECT_NAME})

install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}ConfigVersion.cmake"
              "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Config.cmake"
        DESTINATION lib/cmake/${PROJECT_NAME})

export(EXPORT ${PROJECT_NAME}_Targets
       FILE ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}/${PROJECT_NAME}Targets.cmake)
export(PACKAGE ${PROJECT_NAME})
