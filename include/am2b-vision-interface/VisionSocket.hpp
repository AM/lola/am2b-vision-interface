/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "BaseSocket.hpp"
#include "ControlToVisionContainer.hpp"
#include "VisionSocketOptions.hpp"
#include "VisionToControlContainer.hpp"

namespace am2b {
namespace vision {
    //! Network socket for connecting to the vision system
    class VisionSocket : public BaseSocket<ControlToVisionContainer, VisionToControlContainer, VisionSocketOptions> {
        // Construction
        // ------------
    public:
        //! Constructor (runs in parent thread)
        VisionSocket(const VisionSocketOptions& options = VisionSocketOptions())
            : BaseSocket<ControlToVisionContainer, VisionToControlContainer, VisionSocketOptions>(options)
        {
        }

        //! Destructor
        virtual ~VisionSocket()
        {
        }

        // Extended object handling
        // ------------------------
    protected:
        // Pre-processing of objects to send (runs in background thread) (see base class for details)
        virtual bool preProcessObjectToSend(ControlToVisionContainer& object) override
        {
            object.m_timeMaster = currentTimeInMasterClock();
            object.m_timeSending = object.m_timeMaster;
            return true;
        }

        // Post-processing of received objects (runs in background thread) (see base class for details)
        virtual bool postProcessReceivedObject(VisionToControlContainer& object) override
        {
            object.m_timeReceiving = currentTimeInMasterClock();
            return true;
        }

        // Setters (thread-safe)
        // ---------------------
    public:
        //! Updates the master clock (**thread-safe setter**)
        inline void updateMasterClock(const double& sensorTime) { setCurrentTimeInMasterClock(sensorTime); }
    };
} // namespace vision
} // namespace am2b
