/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/network/TCPServerOptions.hpp"

// Internal libraries
#include "InterfaceConfiguration.hpp"

namespace am2b {
namespace vision {
    //! Options for creating a \ref ControlSocket (meant to be passed to the constructor of \ref ControlSocket)
    class ControlSocketOptions : public broccoli::io::TCPServerOptions {
    public:
        //! Specialized constructor
        /*!
         * \param [in] name Initializes \ref m_name - \copybrief m_name
         * \param [in] multiThreaded Initializes \ref broccoli::parallel::BackgroundWorker::multiThreaded() - \copybrief broccoli::parallel::BackgroundWorker::multiThreaded()
         * \param [in] threadPriority Initializes \ref broccoli::parallel::BackgroundWorker::threadPriority() - \copybrief broccoli::parallel::BackgroundWorker::threadPriority()
         */
        ControlSocketOptions(const std::string& name, const bool& multiThreaded, const int& threadPriority = 0)
            : broccoli::io::TCPServerOptions(name, INTERFACE_TCP_SERVER_PORT, multiThreaded, threadPriority, 0.001 /* <-- max. 1kHz for thread */, false, 1000 /* Receive-buffer for ~10sec @ 100Hz */, 1000 /* Send-buffer for ~10sec @ 100Hz */)
        {
        }

        //! Default constructor
        ControlSocketOptions()
            : ControlSocketOptions("ControlSocket", true)
        {
        }
    };
} // namespace vision
} // namespace am2b
