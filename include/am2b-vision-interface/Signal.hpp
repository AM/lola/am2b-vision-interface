/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <algorithm>
#include <assert.h>

// External libraries
// ...

// Internal libraries
#include "BinaryData.hpp"
#include "SignalDatabase.hpp"
#include "TOMLData.hpp"

namespace am2b {
namespace vision {
    //! Class specifying a signal between the vision system and the control system (bi-directional)
    class Signal : public BinaryData, public TOMLData {
    public:
        // Type definitions
        using IDType = SignalDataBase::IDType;
        using ID = SignalDataBase::ID;
        using DataType = SignalDataBase::DataType;

        // Construction
        // ------------
    public:
        //! Default constructor
        Signal() = default;

        //! Constructor for signal without data
        Signal(const ID& id) { setId(id); }

        //! Generic constructor for untyped (raw) data
        Signal(const ID& id, const size_t& length, const uint8_t* rawData) { setRawSignal(id, length, rawData); }

        //! Virtual destructor
        virtual ~Signal() {}

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const Signal& reference) const
        {
            return m_id == reference.m_id && //
                m_rawData == reference.m_rawData;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const Signal& reference) const { return !operator==(reference); }

        // Members
        // -------
    protected:
        ID m_id = ID::UNKNOWN; //!< \copybrief id()
        std::vector<uint8_t> m_rawData; //!< Raw data storage

        // Getters
        // -------
    public:
        //! ID of the signal
        inline const ID& id() const { return m_id; }

        //! ID of the signal (as string)
        inline std::string idString() const { return SignalDataBase::toString(m_id); }

        //! Datatype of the signal
        virtual DataType dataType() const { return SignalDataBase::dataTypeFromID(m_id); }

        //! Datatype of the signal (as string)
        inline std::string dataTypeString() const { return SignalDataBase::toString(dataType()); }

        //! Length of the payload data in bytes
        inline size_t length() const { return m_rawData.size(); }

        //! Pointer to (raw) payload data
        const uint8_t* rawData() const { return &m_rawData[0]; }

        //! Decode raw buffer and extract typed data
        template <class T>
        T decodeRawData() const
        {
            // Check, if size is correct
            if (m_rawData.size() != sizeof(T)) {
                assert(false);
                return T();
            }

            // Copy data
            T data;
            std::copy(m_rawData.cbegin(), m_rawData.cend(), (uint8_t*)(&data));
            return data;
        }

        // Setters
        // -------
    public:
        //! Setter for \ref id()
        void setId(const ID& id) { m_id = id; }

        //! Generic setter for untyped (raw) data
        void setRawData(const size_t& length, const uint8_t* rawData)
        {
            // Re-allocate memory
            m_rawData.resize(length);

            // Copy data
            if (length > 0)
                std::copy(rawData, rawData + length, m_rawData.begin());
        }

        //! Generic setter for untyped (raw) signal
        void setRawSignal(const ID& id, const size_t& length, const uint8_t* rawData)
        {
            setId(id);
            setRawData(length, rawData);
        }

        //! Encode typed data to raw buffer
        template <class T>
        void encodeRawData(const T& data)
        {
            // Re-allocate memory
            m_rawData.resize(sizeof(T));

            // Copy data
            if (sizeof(T) > 0)
                std::copy((uint8_t*)(&data), (uint8_t*)(&data) + sizeof(T), m_rawData.begin());
        }

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(static_cast<IDType>(m_id), m_rawData);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, static_cast<IDType>(m_id), m_rawData);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            IDType id = 0;
            const auto returnValue = broccoli::io::serialization::deSerialize(stream, index, endianness, id, m_rawData);
            if (id >= static_cast<IDType>(ID::ID_COUNT))
                return 0; // Invalid ID!
            m_id = static_cast<ID>(id);
            return returnValue;
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + context.encodeValuePhantom("ID", static_cast<IDType>(m_id), "Data", (double)0.0) + 50 /* <-- buffer for long strings */;
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            using namespace broccoli::io;

            // Encode ID
            context.encodeSectionHeader();
            context.encodeValue("ID", idString());

            // Encode data
            const DataType type = dataType();
            static const std::string dataKey = "Data";
            if (type == Signal::DataType::EMPTY)
                return true; // <- we are done here
            else if (type == Signal::DataType::BOOL)
                context.encodeValue(dataKey, decodeRawData<bool>());
            else if (type == Signal::DataType::UINT8)
                context.encodeValue(dataKey, decodeRawData<uint8_t>());
            else if (type == Signal::DataType::UINT16)
                context.encodeValue(dataKey, decodeRawData<uint16_t>());
            else if (type == Signal::DataType::UINT32)
                context.encodeValue(dataKey, decodeRawData<uint32_t>());
            else if (type == Signal::DataType::UINT64)
                context.encodeValue(dataKey, decodeRawData<uint64_t>());
            else if (type == Signal::DataType::INT8)
                context.encodeValue(dataKey, decodeRawData<int8_t>());
            else if (type == Signal::DataType::INT16)
                context.encodeValue(dataKey, decodeRawData<int16_t>());
            else if (type == Signal::DataType::INT32)
                context.encodeValue(dataKey, decodeRawData<int32_t>());
            else if (type == Signal::DataType::INT64)
                context.encodeValue(dataKey, decodeRawData<int64_t>());
            else if (type == Signal::DataType::FLOAT)
                context.encodeValue(dataKey, decodeRawData<float>());
            else if (type == Signal::DataType::DOUBLE)
                context.encodeValue(dataKey, decodeRawData<double>());
            else if (type == Signal::DataType::STRING)
                context.encodeValue(dataKey, std::string(m_rawData.cbegin(), m_rawData.cend()));
            else
                return context.handleError("Could not encode data of signal! Datatype not implemented!");

            // Success
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            // Decode ID
            std::string idAsString = "";
            if (context.decodeValue("ID", idAsString) == false)
                return false;
            m_id = SignalDataBase::idFromString(idAsString);
            if (m_id == Signal::ID::UNKNOWN)
                return context.handleError("Could not decode signal! Unknown id '" + idAsString + "'!");

            // Decode data
            const DataType type = dataType();
            if (type == Signal::DataType::EMPTY)
                return true; // <- we are done here
            else if (type == Signal::DataType::BOOL) {
                if (decodeDataFromTOML<bool, bool>(context) == false)
                    return false;
            } else if (type == Signal::DataType::UINT8) {
                if (decodeDataFromTOML<int64_t, uint8_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::UINT16) {
                if (decodeDataFromTOML<int64_t, uint16_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::UINT32) {
                if (decodeDataFromTOML<int64_t, uint32_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::UINT64) {
                if (decodeDataFromTOML<int64_t, uint64_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::INT8) {
                if (decodeDataFromTOML<int64_t, int8_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::INT16) {
                if (decodeDataFromTOML<int64_t, int16_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::INT32) {
                if (decodeDataFromTOML<int64_t, int32_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::INT64) {
                if (decodeDataFromTOML<int64_t, int64_t>(context) == false)
                    return false;
            } else if (type == Signal::DataType::FLOAT) {
                if (decodeDataFromTOML<double, float>(context) == false)
                    return false;
            } else if (type == Signal::DataType::DOUBLE) {
                if (decodeDataFromTOML<double, double>(context) == false)
                    return false;
            } else if (type == Signal::DataType::STRING) {
                if (decodeDataFromTOML<std::string, std::string>(context) == false)
                    return false;
            } else
                return context.handleError("Could not decode signal! Datatype not implemented!");

            // Success
            return true;
        }

        // Helpers
        // -------
    protected:
        //! Decodes the data from the TOML context
        /*!
         * \param [in] context Current TOML context
         * \return `true` on success, `false` otherwise
         */
        template <typename TOMLType, typename SignalType>
        inline bool decodeDataFromTOML(const TOMLDecodingContext& context)
        {
            TOMLType tomlValue;
            if (context.decodeValue("Data", tomlValue) == false)
                return false;
            encodeRawData((SignalType)tomlValue);
            return true;
        }
    };

    //! Decode raw buffer and extract typed data (explicit specialization for `std::string`)
    template <>
    inline std::string Signal::decodeRawData<std::string>() const
    {
        return std::string(m_rawData.cbegin(), m_rawData.cend());
    }

    //! Encode typed data to raw buffer (explicit specialization for `std::string`)
    template <>
    inline void Signal::encodeRawData<std::string>(const std::string& data)
    {
        m_rawData = std::vector<uint8_t>(data.cbegin(), data.cend());
    }

    //! Specialization of typed signals
    template <class T>
    class TypedSignal : public Signal {
    public:
        // Type definitions
        using Data = T; //!< Type of the stored data

        // Construction
        // ------------
    public:
        //! Specialized constructor (from typed value)
        TypedSignal(const ID& id, const Data& data) { setSignal(id, data); }

        // Getters
        // -------
    public:
        //! Datatype of the signal
        DataType dataType() const { return SignalDataBase::dataTypeFromStaticType<T>(); }

        //! Typed data getter
        Data data() const { return decodeRawData<Data>(); }

        //! Typed data getter
        static Data data(const Signal& signal) { return signal.decodeRawData<Data>(); }

        // Setters
        // -------
    public: //! Setter for typed data
        void setData(const Data& data) { encodeRawData(data); }

        //! Setter for typed data
        static void setData(Signal& signal, const Data& data) { signal.encodeRawData<Data>(data); }

        //! Setter for typed signal
        void setSignal(const ID& id, const Data& data)
        {
            setId(id);
            setData(data);
        }

        //! Setter for typed signal
        static void setSignal(Signal& signal, const ID& id, const Data& data)
        {
            signal.setId(id);
            return setData(signal, data);
        }

    private:
        // Hide base class interface
        TypedSignal();
        TypedSignal(const ID& id);
        TypedSignal(const ID& id, const size_t& length, const uint8_t* rawData);
    };

    // Definition of typed signals
    // ---------------------------
    using SignalBool = TypedSignal<bool>; //!< Specialization of signal for booleans
    using SignalUInt8 = TypedSignal<uint8_t>; //!< Specialization of signal for integers (unsigned, 8bit)
    using SignalUInt16 = TypedSignal<uint16_t>; //!< Specialization of signal for integers (unsigned, 16bit)
    using SignalUInt32 = TypedSignal<uint32_t>; //!< Specialization of signal for integers (unsigned, 32bit)
    using SignalUInt64 = TypedSignal<uint64_t>; //!< Specialization of signal for integers (unsigned, 64bit)
    using SignalInt8 = TypedSignal<int8_t>; //!< Specialization of signal for integers (signed, 8bit)
    using SignalInt16 = TypedSignal<int16_t>; //!< Specialization of signal for integers (signed, 16bit)
    using SignalInt32 = TypedSignal<int32_t>; //!< Specialization of signal for integers (signed, 32bit)
    using SignalInt64 = TypedSignal<int64_t>; //!< Specialization of signal for integers (signed, 64bit)
    using SignalFloat = TypedSignal<float>; //!< Specialization of signal for floating point numbers (32bit)
    using SignalDouble = TypedSignal<double>; //!< Specialization of signal for floating point numbers (64bit)
    using SignalString = TypedSignal<std::string>; //!< Specialization of signal for strings
} // namespace vision
} // namespace am2b
