/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "BaseSocket.hpp"
#include "ControlSocketOptions.hpp"
#include "ControlToVisionContainer.hpp"
#include "VisionToControlContainer.hpp"

namespace am2b {
namespace vision {
    //! Network socket for connecting to the control system
    class ControlSocket : public BaseSocket<VisionToControlContainer, ControlToVisionContainer, ControlSocketOptions> {
        // Construction
        // ------------
    public:
        //! Constructor (runs in parent thread)
        ControlSocket(const ControlSocketOptions& options = ControlSocketOptions())
            : BaseSocket<VisionToControlContainer, ControlToVisionContainer, ControlSocketOptions>(options)
        {
        }

        //! Destructor
        virtual ~ControlSocket()
        {
        }

        // Extended object handling
        // ------------------------
    protected:
        // Pre-processing of objects to send (runs in background thread) (see base class for details)
        virtual bool preProcessObjectToSend(VisionToControlContainer& object) override
        {
            object.m_timeSending = currentTimeInMasterClock();
            return true;
        }

        // Post-processing of received objects (runs in background thread) (see base class for details)
        virtual bool postProcessReceivedObject(ControlToVisionContainer& object) override
        {
            // Assume that there is no significant delay from transimission over the network
            setCurrentTimeInMasterClock(object.m_timeMaster);
            object.m_timeReceiving = object.m_timeMaster;
            return true;
        }
    };
} // namespace vision
} // namespace am2b
