/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

#define INTERFACE_TCP_SERVER_ADDRESS "192.168.0.8" //!< IP address of tcp server of interface
#define INTERFACE_TCP_SERVER_PORT 5008 //!< Port of tcp server of interface
