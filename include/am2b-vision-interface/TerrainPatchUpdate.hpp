/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/pnm/PNMFile.hpp"

// Internal libraries
#include "BinaryData.hpp"
#include "TOMLData.hpp"
#include "TerrainSpecification.hpp"

namespace am2b {
namespace vision {
    //! Data container for an update of **multiple** neighboring terrain patches forming a rectangle
    class TerrainPatchUpdate : public BinaryData, public TOMLData {
        // Type definitions
        // ----------------
    public:
        using HeightValue = TerrainSpecification::HeightValue;
        using Confidence = TerrainSpecification::Confidence;
        using PatchLocationIndex = TerrainSpecification::PatchLocationIndex;
        using PatchLocation = TerrainSpecification::PatchLocation;
        using PatchHeightMap = TerrainSpecification::PatchHeightMap;
        using PatchConfidenceMap = TerrainSpecification::PatchConfidenceMap;

        // Construction
        // ------------
    public:
        //! Constructor
        TerrainPatchUpdate() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const TerrainPatchUpdate& reference) const
        {
            return m_gridLocation == reference.m_gridLocation && //
                m_heightMap == reference.m_heightMap && //
                m_confidenceMap == reference.m_confidenceMap;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const TerrainPatchUpdate& reference) const { return !operator==(reference); }

        //! Checks, if the container is properly defined
        /*! \return `true` if the container is valid, `false` otherwise */
        inline bool isValid() const
        {
            // Check size of height map
            const auto& heightMapSize = m_heightMap.size();
            if (heightMapSize[0] == 0 || heightMapSize[0] % TerrainSpecification::cellsPerPatchX() > 0 || //
                heightMapSize[1] == 0 || heightMapSize[1] % TerrainSpecification::cellsPerPatchY() > 0)
                return false;

            // Check size of confidence map
            if (m_confidenceMap.size() != m_heightMap.size())
                return false;

            // Check grid location
            const int64_t countOfPatchesInX = heightMapSize[0] / TerrainSpecification::cellsPerPatchX();
            if ((int64_t)m_gridLocation[0] + countOfPatchesInX - 1 > std::numeric_limits<PatchLocationIndex>::max())
                return false;
            const int64_t countOfPatchesInY = heightMapSize[1] / TerrainSpecification::cellsPerPatchY();
            if ((int64_t)m_gridLocation[1] + countOfPatchesInY - 1 > std::numeric_limits<PatchLocationIndex>::max())
                return false;

            // No errors detected -> valid
            return true;
        }

        // Members
        // -------
    public:
        PatchLocation m_gridLocation{ { 0, 0 } }; //!< Location of the "top left" (image) patch in the terrain grid (remaining patches are natural extension of the patch)
        PatchHeightMap m_heightMap; //!< Map (x, y) of height values for each cell
        PatchConfidenceMap m_confidenceMap; //!< Map (x, y) of confidence values for each cell

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_gridLocation, m_heightMap, m_confidenceMap);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_gridLocation, m_heightMap, m_confidenceMap);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_gridLocation, m_heightMap, m_confidenceMap);
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            broccoli::io::encoding::CharacterStreamSize returnValue = context.encodeSectionHeaderPhantom() + context.encodeValuePhantom("GridLocation", m_gridLocation);
            returnValue += context.encodeDataStreamPhantom("HeightMap", 50 /* <-- header */ + m_heightMap.elementCount() * sizeof(TerrainSpecification::HeightValue));
            returnValue += context.encodeDataStreamPhantom("ConfidenceMap", 50 /* <-- header */ + m_confidenceMap.elementCount() * sizeof(TerrainSpecification::Confidence));
            return returnValue;
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            // Generic
            using namespace broccoli::io;
            context.encodeSectionHeader();
            context.encodeValue("GridLocation", m_gridLocation);

            // Height map
            PNMFile heightMapFile;
            heightMapFile.setData(m_heightMap);
            encoding::CharacterStream heightMapStream;
            if (heightMapFile.encode(heightMapStream, PNMFileEncoding::Type::BINARY) == false)
                return context.handleError("Could not encode height map to stream!");
            if (context.encodeDataStream("HeightMap", heightMapStream, context.sectionNameGlobal() + ".HeightMap.pgm") == false)
                return false;

            // Confidence map
            PNMFile confidenceMapFile;
            confidenceMapFile.setData(m_confidenceMap);
            encoding::CharacterStream confidenceMapStream;
            if (confidenceMapFile.encode(confidenceMapStream, PNMFileEncoding::Type::BINARY) == false)
                return context.handleError("Could not encode confidence map to stream!");
            if (context.encodeDataStream("ConfidenceMap", confidenceMapStream, context.sectionNameGlobal() + ".ConfidenceMap.pgm") == false)
                return false;

            // Success
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            // Generic
            using namespace broccoli::io;
            if (context.decodeValue("GridLocation", m_gridLocation) == false)
                return false;

            // Height map
            encoding::CharacterStream heightMapStream;
            if (context.decodeDataStream("HeightMap", heightMapStream) == false)
                return false;
            PNMFile heightMapFile;
            if (heightMapFile.decode(heightMapStream) == false)
                return context.handleError("Could not decode height map from stream!");
            heightMapFile.convertTo(PNMFile::Type::PGM_16BIT);
            m_heightMap = heightMapFile.dataPGM16();

            // Confidence map
            encoding::CharacterStream confidenceMapStream;
            if (context.decodeDataStream("ConfidenceMap", confidenceMapStream) == false)
                return false;
            PNMFile confidenceMapFile;
            if (confidenceMapFile.decode(confidenceMapStream) == false)
                return context.handleError("Could not decode confidence map from stream!");
            confidenceMapFile.convertTo(PNMFile::Type::PGM_8BIT);
            m_confidenceMap = confidenceMapFile.dataPGM8();

            // Success
            return true;
        }
    };
} // namespace vision
} // namespace am2b
