/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include <broccoli/geometry/ssv/SSVSegment.hpp>

// Internal libraries
#include "BinaryData.hpp"
#include "TOMLData.hpp"
#include "TOMLSSVElementList.hpp"

namespace am2b {
namespace vision {
    //! Model of the volume of an object as part of the environment model
    /*!
     * The volume is modeled through swept-sphere volumes. For consistency, the ID of the underlying
     * SSV segment should be the same as the ID of the corresponding object. Moreover, the position and
     * orientation of the SSV segment should specify the pose of the segment relative to the vision-world
     * frame and should be equal to the pose of the object frame. This allows to re-use the integrated
     * efficient transformation methods. This implies, that the local SSVPoint-, SSVLine-, and SSVTriangle-
     * elements are described in the local object frame.
     */
    class ObjectVolumeModel : public BinaryData, public TOMLData {
        // Construction
        // ------------
    public:
        //! Default constructor
        ObjectVolumeModel() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ObjectVolumeModel& reference) const
        {
            return m_ssvSegment.id() == reference.m_ssvSegment.id() && //
                m_ssvSegment.name() == reference.m_ssvSegment.name() && //
                m_ssvSegment.position() == reference.m_ssvSegment.position() && //
                m_ssvSegment.orientation() == reference.m_ssvSegment.orientation() && //
                m_ssvSegment.localPointElements() == reference.m_ssvSegment.localPointElements() && //
                m_ssvSegment.localLineElements() == reference.m_ssvSegment.localLineElements() && //
                m_ssvSegment.localTriangleElements() == reference.m_ssvSegment.localTriangleElements();
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ObjectVolumeModel& reference) const { return !operator==(reference); }

        // Members
        // -------
    public:
        broccoli::geometry::SSVSegment m_ssvSegment; //!< SSV representation of the volume (in local object coordinate system)

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = serializeHeaderPhantom();
            returnValue += sizeof(broccoli::io::serialization::BinaryStreamSize) + serializePhantom(m_ssvSegment.id(), m_ssvSegment.name(), m_ssvSegment.position(), m_ssvSegment.orientation());
            static const auto sizeVector3d = serializePhantom(Eigen::Vector3d::Zero());
            returnValue += sizeof(broccoli::io::serialization::BinaryStreamSize) + m_ssvSegment.localPointElements().size() * (sizeof(broccoli::io::serialization::BinaryStreamSize) + 1 * sizeVector3d + sizeof(double));
            returnValue += sizeof(broccoli::io::serialization::BinaryStreamSize) + m_ssvSegment.localLineElements().size() * (sizeof(broccoli::io::serialization::BinaryStreamSize) + 2 * sizeVector3d + sizeof(double));
            returnValue += sizeof(broccoli::io::serialization::BinaryStreamSize) + m_ssvSegment.localTriangleElements().size() * (sizeof(broccoli::io::serialization::BinaryStreamSize) + 3 * sizeVector3d + sizeof(double));
            return returnValue;
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_ssvSegment);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_ssvSegment);
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            broccoli::io::encoding::CharacterStreamSize returnValue = context.encodeSectionHeaderPhantom();
            returnValue += context.encodeValuePhantom("Name", m_ssvSegment.name());
            returnValue += TOMLSSVElementList().estimatedMaximumTOMLStreamSize(context);
            return returnValue;
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            context.encodeSectionHeader();
            context.encodeValue("Name", m_ssvSegment.name());
            return TOMLSSVElementList(m_ssvSegment).encodeToTOML(context);
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            std::string name = "";
            if (context.decodeValue("Name", name) == false)
                return false;
            m_ssvSegment.setName(name);
            TOMLSSVElementList elementList;
            if (elementList.decodeFromTOML(context) == false)
                return false;
            elementList.convertTo(m_ssvSegment);
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
