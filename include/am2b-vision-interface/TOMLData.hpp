/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLInputData.hpp"
#include "TOMLOutputData.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of data objects which can be encoded and decoded to/from the TOML format
    class TOMLData : public TOMLInputData, public TOMLOutputData {
    };
} // namespace vision
} // namespace am2b
