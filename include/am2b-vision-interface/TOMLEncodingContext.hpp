/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/compression.hpp"
#include "broccoli/io/encoding.hpp"
#include "broccoli/io/filesystem.hpp"
#include "broccoli/io/serialization/serialization.hpp"
#include <Eigen/Dense>
#include <Eigen/StdVector>

// Internal libraries
#include "TOMLBaseContext.hpp"

namespace am2b {
namespace vision {
    //! Context used for encoding to TOML
    class TOMLEncodingContext : public TOMLBaseContext {
        // Construction
        // ------------
    public:
        //! Default constructor (for root context)
        TOMLEncodingContext()
            : m_stream(std::make_shared<broccoli::io::encoding::CharacterStream>())
        {
        }

        //! Specialized constructor (for root context)
        /*!
         * \param [in] dataStreamEmbedded Initializes \ref dataStreamEmbedded() - \copybrief dataStreamEmbedded()
         * \param [in] dataStreamCompression Initializes \ref dataStreamCompression() - \copybrief dataStreamCompression()
         * \param [in] dataStreamFilePathPrefix Initializes \ref dataStreamFilePathPrefix() - \copybrief dataStreamFilePathPrefix()
         */
        TOMLEncodingContext(const bool& dataStreamEmbedded, const broccoli::io::compression::CompressionOptions& dataStreamCompression = broccoli::io::compression::CompressionOptions::gzip(), const std::string& dataStreamFilePathPrefix = "")
            : m_stream(std::make_shared<broccoli::io::encoding::CharacterStream>())
            , m_dataStreamEmbedded(dataStreamEmbedded)
            , m_dataStreamCompression(dataStreamCompression)
            , m_dataStreamFilePathPrefix(dataStreamFilePathPrefix)
        {
        }

        //! Specialized constructor (for child context)
        /*!
         * \param [in] parent The parent context
         * \param [in] sectionNameLocal Initializes \ref sectionNameLocal() - \copybrief sectionNameLocal()
         */
        TOMLEncodingContext(const TOMLEncodingContext& parent, const std::string& sectionNameLocal)
            : TOMLBaseContext(parent, sectionNameLocal)
            , m_stream(parent.m_stream)
            , m_indentationLevel(parent.m_indentationLevel + 1)
            , m_indentationCharactersPerLevel(parent.m_indentationCharactersPerLevel)
            , m_indentationCharacter(parent.m_indentationCharacter)
            , m_numericFormat(parent.m_numericFormat)
            , m_dataStreamEmbedded(parent.m_dataStreamEmbedded)
            , m_dataStreamCompression(parent.m_dataStreamCompression)
            , m_dataStreamFilePathPrefix(parent.m_dataStreamFilePathPrefix)
        {
        }

        // Members
        // -------
    protected:
        std::shared_ptr<broccoli::io::encoding::CharacterStream> m_stream; //!< \copybrief stream() const
        int64_t m_indentationLevel = -1; //!< \copybrief indentationLevel()
        size_t m_indentationCharactersPerLevel = 2; //!< \copybrief indentationCharactersPerLevel()
        char m_indentationCharacter = ' '; //!< \copybrief indentationCharacter()
        std::string m_numericFormat = "^%+.16e"; //!< \copybrief numericFormat()
        bool m_dataStreamEmbedded = true; //!< \copybrief dataStreamEmbedded()
        broccoli::io::compression::CompressionOptions m_dataStreamCompression = broccoli::io::compression::CompressionOptions::gzip(); //!< \copybrief dataStreamCompression()
        std::string m_dataStreamFilePathPrefix = ""; //!< \copybrief dataStreamFilePathPrefix()

        // Getters
        // -------
    public:
        //! Reference to stream to which the data should be encoded to
        inline broccoli::io::encoding::CharacterStream& stream() const { return *m_stream; }
        //! Current level in TOML hierarchy (-1=root)
        inline const int64_t& indentationLevel() const { return m_indentationLevel; }
        //! Count of indentation characters per level
        inline const size_t& indentationCharactersPerLevel() const { return m_indentationCharactersPerLevel; }
        //! Character used for indentation of levels
        inline const char& indentationCharacter() const { return m_indentationCharacter; }
        //! Format specifier for numeric values
        inline const std::string& numericFormat() const { return m_numericFormat; }
        //! If `true`, all data streams are embedded into the TOML stream, otherwise external files are created and referred to
        inline const bool& dataStreamEmbedded() const { return m_dataStreamEmbedded; }
        //! Compression options for data streams (embedded or external)
        inline const broccoli::io::compression::CompressionOptions& dataStreamCompression() const { return m_dataStreamCompression; }
        //! File path prefix for external files storing data streams
        inline const std::string& dataStreamFilePathPrefix() const { return m_dataStreamFilePathPrefix; }

        // Setters
        // -------
    public:
        //! Setter for \ref numericFormat()
        inline void setNumericFormat(const std::string& value) { m_numericFormat = value; }
        //! Setter for \ref dataStreamEmbedded()
        inline void setDataStreamEmbedded(const bool& value) { m_dataStreamEmbedded = value; }
        //! Setter for \ref dataStreamCompression()
        inline void setDataStreamCompression(const broccoli::io::compression::CompressionOptions& value) { m_dataStreamCompression = value; }
        //! Setter for \ref dataStreamFilePathPrefix()
        inline void setDataStreamFilePathPrefix(const std::string& value) { m_dataStreamFilePathPrefix = value; }

        // Encoding
        // --------
    public:
        //! Adds the current indentation to the stream
        inline void encodeIndentation() const
        {
            if (m_indentationLevel > 0 && m_indentationCharactersPerLevel > 0) {
                const size_t indentationCharacters = m_indentationLevel * m_indentationCharactersPerLevel;
                m_stream->resize(m_stream->size() + indentationCharacters, m_indentationCharacter);
            }
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeIndentationPhantom() const
        {
            if (m_indentationLevel > 0 && m_indentationCharactersPerLevel > 0)
                return m_indentationLevel * m_indentationCharactersPerLevel;
            else
                return 0;
        }

        //! Adds the given count of linebreaks to the stream
        inline void encodeLineBreak(const size_t& count = 1) const { m_stream->resize(m_stream->size() + count, (char)'\n'); }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeLineBreakPhantom(const size_t& count = 1) const { return count; }

        //! Encodes plain data to the string (no formatting, key or indentation)
        inline void encodePlain(const std::string& data) const { broccoli::io::encoding::encode(*m_stream, data); }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodePlainPhantom(const std::string& data) const { return data.size(); }

        //! Encodes the current section header to the stream
        inline void encodeSectionHeader() const
        {
            using namespace broccoli::io;
            if (m_sectionNameGlobal.size() == 0)
                return; // Root -> no header
            encodeIndentation();
            encoding::encode(*m_stream, (char)'[');
            encoding::encode(*m_stream, m_sectionNameGlobal);
            encoding::encode(*m_stream, "]\n");
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeSectionHeaderPhantom() const { return encodeIndentationPhantom() + m_sectionNameGlobal.size() + 3; }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        inline void encodeRawValue(const bool& value) const
        {
            using namespace broccoli::io;
            if (value == true)
                encoding::encode(*m_stream, "true");
            else
                encoding::encode(*m_stream, "false");
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const bool&) const { return 5; }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename T, typename std::enable_if<std::is_integral<T>::value && !std::is_same<T, bool>::value, int>::type = 0>
        inline void encodeRawValue(const T& value) const
        {
            using namespace broccoli::io;
            encoding::encode(*m_stream, value);
        }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename T, typename std::enable_if<std::is_floating_point<T>::value, int>::type = 0>
        inline void encodeRawValue(const T& value) const
        {
            using namespace broccoli::io;
            encoding::encode(*m_stream, value, m_numericFormat);
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T, typename std::enable_if<(std::is_integral<T>::value && !std::is_same<T, bool>::value) || std::is_floating_point<T>::value, int>::type = 0>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const T&) const { return 24; }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        inline void encodeRawValue(const std::string& value) const
        {
            using namespace broccoli::io;
            encoding::encode(*m_stream, (char)'\"');
            encoding::encode(*m_stream, value);
            encoding::encode(*m_stream, '\"');
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const std::string& value) const { return value.size() + 2; }

        //! Encodes a raw array to the stream (without key)
        /*! \param [in] array The array to encode */
        template <typename T>
        inline void encodeRawArray(const T& array) const
        {
            using namespace broccoli::io;
            encoding::encode(*m_stream, (char)'[');
            for (int i = 0; i < (int)array.size(); i++) {
                if (i > 0)
                    encoding::encode(*m_stream, ", ");
                encodeRawValue(array[i]);
            }
            encoding::encode(*m_stream, (char)']');
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawArrayPhantom(const T& array) const
        {
            broccoli::io::encoding::CharacterStreamSize returnValue = 2 + array.size() * 2;
            if (array.size() > 2)
                returnValue += array.size() * encodeRawValuePhantom(array[0]);
            else {
                for (int i = 0; i < (int)array.size(); i++)
                    returnValue += encodeRawValuePhantom(array[i]);
            }
            return returnValue;
        }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename T, size_t N>
        inline void encodeRawValue(const std::array<T, N>& value) const { encodeRawArray(value); }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T, size_t N>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const std::array<T, N>& value) const { return encodeRawArrayPhantom(value); }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename T, typename Allocator = std::allocator<T>>
        inline void encodeRawValue(const std::vector<T, Allocator>& value) const { encodeRawArray(value); }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T, typename Allocator = std::allocator<T>>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const std::vector<T, Allocator>& value) const { return encodeRawArrayPhantom(value); }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename T, size_t MaximumStaticElementCount = 0, typename Allocator = std::allocator<T>>
        inline void encodeRawValue(const broccoli::memory::SmartVector<T, MaximumStaticElementCount, Allocator>& value) const { encodeRawArray(value); }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T, size_t MaximumStaticElementCount = 0, typename Allocator = std::allocator<T>>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const broccoli::memory::SmartVector<T, MaximumStaticElementCount, Allocator>& value) const { return encodeRawArrayPhantom(value); }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename Derived>
        inline void encodeRawValue(const Eigen::MatrixBase<Derived>& value) const
        {
            using namespace broccoli::io;
            if (value.RowsAtCompileTime != Eigen::Dynamic && value.rows() == 1) {
                // Guaranteed only 1 row
                if (value.ColsAtCompileTime != Eigen::Dynamic && value.cols() == 1) {
                    // Guaranteed only 1 column -> scalar
                    encodeRawValue(value(0, 0));
                } else {
                    // Arbitrary count of columns -> row vector
                    encoding::encode(*m_stream, (char)'[');
                    for (Eigen::Index i = 0; i < value.cols(); i++) {
                        if (i > 0)
                            encoding::encode(*m_stream, ", ");
                        encodeRawValue(value(0, i));
                    }
                    encoding::encode(*m_stream, (char)']');
                }
            } else {
                // Arbitrary count of rows
                if (value.ColsAtCompileTime != Eigen::Dynamic && value.cols() == 1) {
                    // Guaranteed only 1 column -> column vector
                    encoding::encode(*m_stream, (char)'[');
                    for (Eigen::Index i = 0; i < value.rows(); i++) {
                        if (i > 0)
                            encoding::encode(*m_stream, ", ");
                        encodeRawValue(value(i, 0));
                    }
                    encoding::encode(*m_stream, (char)']');
                } else {
                    // Arbitrary count of columns -> matrix
                    encoding::encode(*m_stream, (char)'[');
                    for (Eigen::Index i = 0; i < value.rows(); i++) {
                        if (i > 0)
                            encoding::encode(*m_stream, ", ");
                        encoding::encode(*m_stream, (char)'[');
                        for (Eigen::Index j = 0; j < value.cols(); j++) {
                            if (j > 0)
                                encoding::encode(*m_stream, ", ");
                            encodeRawValue(value(i, j));
                        }
                        encoding::encode(*m_stream, (char)']');
                    }
                    encoding::encode(*m_stream, (char)']');
                }
            }
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename Derived>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const Eigen::MatrixBase<Derived>& value) const
        {
            if (value.size() == 0)
                return 2;
            else
                return 2 + value.size() * (encodeRawValuePhantom(value(0, 0)) + 2) + value.rows() * (2 + 2);
        }

        //! Encodes a raw value to the stream (without key)
        /*! \param [in] value The value to encode */
        template <typename Derived>
        inline void encodeRawValue(const Eigen::QuaternionBase<Derived>& value) const
        {
            using namespace broccoli::io;
            encoding::encode(*m_stream, (char)'[');
            encodeRawValue(value.w());
            encoding::encode(*m_stream, ", ");
            encodeRawValue(value.x());
            encoding::encode(*m_stream, ", ");
            encodeRawValue(value.y());
            encoding::encode(*m_stream, ", ");
            encodeRawValue(value.z());
            encoding::encode(*m_stream, (char)']');
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename Derived>
        inline broccoli::io::encoding::CharacterStreamSize encodeRawValuePhantom(const Eigen::QuaternionBase<Derived>& value) const
        {
            return 2 + 6 + 4 * encodeRawValuePhantom(value.w());
        }

        //! Encodes a value to the stream (with indentation and key)
        /*!
         * \param [in] key TOML key for this element
         * \param [in] data The data to encode
         */
        template <typename T>
        inline void encodeValue(const std::string& key, const T& data) const
        {
            using namespace broccoli::io;
            encodeIndentation();
            encoding::encode(*m_stream, key + " = ");
            encodeRawValue(data);
            encoding::encode(*m_stream, (char)'\n');
        }

        //! Dummy method to terminate recursive call of corresponding variadic template
        inline void encodeValue() const {}

        //! Encodes a value to the stream (with indentation and key)
        template <typename firstType, typename secondType, typename... remainingTypes>
        inline void encodeValue(const std::string& firstKey, const firstType& firstValue, const std::string& secondKey, const secondType& secondValue, const remainingTypes&... remainingValues) const
        {
            encodeValue(firstKey, firstValue);
            encodeValue(secondKey, secondValue);
            encodeValue(remainingValues...);
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename T>
        inline broccoli::io::encoding::CharacterStreamSize encodeValuePhantom(const std::string& key, const T& data) const { return encodeIndentationPhantom() + key.size() + encodeRawValuePhantom(data) + 4; }

        //! Dummy method to terminate recursive call of corresponding variadic template
        /*! \return 0 */
        inline broccoli::io::encoding::CharacterStreamSize encodeValuePhantom() const { return 0; }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        template <typename firstType, typename secondType, typename... remainingTypes>
        inline broccoli::io::encoding::CharacterStreamSize encodeValuePhantom(const std::string& firstKey, const firstType& firstValue, const std::string& secondKey, const secondType& secondValue, const remainingTypes&... remainingValues) const
        {
            return encodeValuePhantom(firstKey, firstValue) + encodeValuePhantom(secondKey, secondValue) + encodeValuePhantom(remainingValues...);
        }

        //! Encodes a data stream to the stream (embedded or external) (with indentation and key)
        /*!
         * \param [in] keyPrefix TOML key prefix for this element
         * \param [in] data The data to encode
         * \param [in] dataStreamFilePathSuffix Suffix to append to data stream file path (external)
         * \return `true` on success, `false` otherwise
         */
        inline bool encodeDataStream(const std::string& keyPrefix, const broccoli::io::serialization::BinaryStream& data, const std::string& dataStreamFilePathSuffix) const
        {
            using namespace broccoli::io;

            // Initialize helpers
            std::string key = keyPrefix; // Final TOML key for element
            std::string filePath = m_dataStreamFilePathPrefix + dataStreamFilePathSuffix; // Final path of the external file

            // Check compression options
            if (m_dataStreamCompression.compressed() == true) {
                if (m_dataStreamCompression.method() == broccoli::io::compression::Method::RAW) {
                    key += "Raw";
                    filePath += ".raw";
                } else if (m_dataStreamCompression.method() == broccoli::io::compression::Method::ZLIB) {
                    key += "Zlib";
                    filePath += ".zlib";
                } else {
                    key += "Gzip";
                    filePath += ".gz";
                }
            }
            if (m_dataStreamEmbedded == true)
                key += "Base64";
            else
                key += "Path";

            // Start TOML element
            encodeIndentation();
            encoding::encode(*m_stream, key);
            encoding::encode(*m_stream, " = \"");

            // Check embedding type
            if (m_dataStreamEmbedded == true) {
                std::string dataBase64;
                if (m_dataStreamCompression.compressed() == false)
                    encoding::encodeToBase64(data, dataBase64);
                else {
                    serialization::BinaryStream dataStreamCompressed;
                    if (compression::compress(data, dataStreamCompressed, m_dataStreamCompression) == false)
                        return handleError("Compression of datastream failed!");
                    encoding::encodeToBase64(dataStreamCompressed, dataBase64);
                }
                encoding::encode(*m_stream, dataBase64);
            } else {
                if (filesystem::writeFile(filePath, data, m_dataStreamCompression) == false)
                    return handleError("Writing external file '" + filePath + "' failed!");
                encoding::encode(*m_stream, filePath);
            }

            // End TOML element
            encoding::encode(*m_stream, "\"\n");

            // Success
            return true;
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        inline broccoli::io::encoding::CharacterStreamSize encodeDataStreamPhantom(const std::string& keyPrefix, const broccoli::io::encoding::CharacterStreamSize& estimatedEmbeddedDataStreamSize) const
        {
            return encodeIndentationPhantom() + keyPrefix.size() + 10 + 4 + (m_dataStreamEmbedded ? (4 * estimatedEmbeddedDataStreamSize) / 3 /* <-- base64 */ : 4095 /* <-- max file path length */) + 2;
        }
    };
} // namespace vision
} // namespace am2b
