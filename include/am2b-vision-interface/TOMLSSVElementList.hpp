/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <vector>

// External libraries
#include "broccoli/geometry/ssv/SSVSegment.hpp"

// Internal libraries
#include "TOMLSSVElement.hpp"

namespace am2b {
namespace vision {
    //! Data container for a list of SSV elements in TOML format
    class TOMLSSVElementList : public std::vector<TOMLSSVElement, Eigen::aligned_allocator<TOMLSSVElement>>, public TOMLData {
        // Construction
        // ------------
    public:
        //! Default constructor
        TOMLSSVElementList() = default;

        //! Specialized constructor (from SSV segment)
        TOMLSSVElementList(const broccoli::geometry::SSVSegment& segment) { convertFrom(segment); }

        // Converstion
        // -----------
    public:
        //! Fills **this** object with elements from the given segment
        void convertFrom(const broccoli::geometry::SSVSegment& segment)
        {
            clear();
            reserve(segment.localPointElements().size() + segment.localLineElements().size() + segment.localTriangleElements().size());
            for (size_t i = 0; i < segment.localPointElements().size(); i++)
                emplace_back(segment.localPointElements()[i]);
            for (size_t i = 0; i < segment.localLineElements().size(); i++)
                emplace_back(segment.localLineElements()[i]);
            for (size_t i = 0; i < segment.localTriangleElements().size(); i++)
                emplace_back(segment.localTriangleElements()[i]);
        }

        //! Fills the given segment with elements from **this** object
        void convertTo(broccoli::geometry::SSVSegment& segment) const
        {
            size_t pointElementCount = 0;
            size_t lineElementCount = 0;
            size_t triangleElementCount = 0;
            for (size_t i = 0; i < size(); i++) {
                const TOMLSSVElement& currentElement = operator[](i);
                if (currentElement.m_points.size() == 1)
                    pointElementCount++;
                else if (currentElement.m_points.size() == 2)
                    lineElementCount++;
                else if (currentElement.m_points.size() == 3)
                    triangleElementCount++;
            }
            segment.clearElements();
            segment.reserveElements(pointElementCount, lineElementCount, triangleElementCount);
            for (size_t i = 0; i < size(); i++) {
                const TOMLSSVElement& currentElement = operator[](i);
                if (currentElement.m_points.size() == 1)
                    segment.addElement(broccoli::geometry::SSVPointElement(currentElement.m_points[0], currentElement.m_radius));
                else if (currentElement.m_points.size() == 2)
                    segment.addElement(broccoli::geometry::SSVLineElement(currentElement.m_points[0], currentElement.m_points[1], currentElement.m_radius));
                else if (currentElement.m_points.size() == 3)
                    segment.addElement(broccoli::geometry::SSVTriangleElement(currentElement.m_points[0], currentElement.m_points[1], currentElement.m_points[2], currentElement.m_radius));
            }
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            static const TOMLSSVElement dummyTriangleElement = createDummyTriangleElement();
            const TOMLEncodingContext contextList(context, "SSVElements");
            const size_t elementCount = std::max((size_t)size(), (size_t)100); // Estimate maximum count of elements in case no elements are available
            return contextList.encodeSectionHeaderPhantom() + elementCount * dummyTriangleElement.estimatedMaximumTOMLStreamSize(TOMLEncodingContext(contextList, "9999"));
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const { return encodeSortedListToTOML(context, "SSVElements", *this); }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context) { return decodeSortedListFromTOML(context, "SSVElements", *this); }

        // Helpers
        // -------
    protected:
        //! Creates a dummy triangle element
        static inline TOMLSSVElement createDummyTriangleElement()
        {
            TOMLSSVElement element;
            element.m_points.resize(3);
            element.m_points[0] = Eigen::Vector3d(0.5, -0.5, 0.5);
            element.m_points[1] = Eigen::Vector3d(0.5, 0.5, 0.5);
            element.m_points[2] = Eigen::Vector3d(0.5, 0.0, 1.0);
            element.m_radius = 1.0;
            return element;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
