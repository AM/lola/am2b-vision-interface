/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "ObjectUpdatePartial.hpp"

namespace am2b {
namespace vision {
    //! Data container for a full update of an object
    class ObjectUpdateFull : public ObjectUpdatePartial {
        // Type definitions
        // ----------------
    public:
        using SurfaceModel = ObjectSpecification::SurfaceModel;
        using VolumeModel = ObjectSpecification::VolumeModel;

        // Construction
        // ------------
    public:
        //! Constructor
        ObjectUpdateFull() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ObjectUpdateFull& reference) const
        {
            return ObjectUpdatePartial::operator==(reference) && //
                m_surfaceModel == reference.m_surfaceModel && //
                m_volumeModel == reference.m_volumeModel;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ObjectUpdateFull& reference) const { return !operator==(reference); }

        // Members
        // -------
    public:
        SurfaceModel m_surfaceModel; //!< \copybrief ObjectSpecification::SurfaceModel
        VolumeModel m_volumeModel; //!< \copybrief ObjectSpecification::VolumeModel

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return ObjectUpdatePartial::estimatedMaximumBinaryStreamSize() + m_surfaceModel.estimatedMaximumBinaryStreamSize() + m_volumeModel.estimatedMaximumBinaryStreamSize();
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return ObjectUpdatePartial::serializePayload(stream, endianness) + broccoli::io::serialization::serialize(stream, endianness, m_surfaceModel, m_volumeModel);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            const broccoli::io::serialization::BinaryStreamSize baseClassSize = ObjectUpdatePartial::deSerializePayload(stream, index, payloadSize, endianness);
            const broccoli::io::serialization::BinaryStreamSize totalSize = baseClassSize + broccoli::io::serialization::deSerialize(stream, index + baseClassSize, endianness, m_surfaceModel, m_volumeModel);
            forceConsistency();
            return totalSize;
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return ObjectUpdatePartial::estimatedMaximumTOMLStreamSize(context) + //
                m_surfaceModel.estimatedMaximumTOMLStreamSize(TOMLEncodingContext(context, "SurfaceModel")) + //
                context.encodeLineBreakPhantom() + //
                m_volumeModel.estimatedMaximumTOMLStreamSize(TOMLEncodingContext(context, "VolumeModel"));
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            if (ObjectUpdatePartial::encodeToTOML(context) == false || //
                m_surfaceModel.encodeToTOML(TOMLEncodingContext(context, "SurfaceModel")) == false)
                return false;
            context.encodeLineBreak();
            if (m_volumeModel.encodeToTOML(TOMLEncodingContext(context, "VolumeModel")) == false)
                return false;
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            if (ObjectUpdatePartial::decodeFromTOML(context) == false || //
                m_surfaceModel.decodeFromTOML(TOMLDecodingContext(context, "SurfaceModel")) == false || //
                m_volumeModel.decodeFromTOML(TOMLDecodingContext(context, "VolumeModel")) == false)
                return false;
            forceConsistency();
            return true;
        }

        // Helpers
        // -------
    public:
        //! Forces consistency in the object description
        void forceConsistency()
        {
            m_volumeModel.m_ssvSegment.setID(m_id);
            m_volumeModel.m_ssvSegment.setPosition(m_position);
            m_volumeModel.m_ssvSegment.setOrientation(m_orientation.toRotationMatrix());
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
