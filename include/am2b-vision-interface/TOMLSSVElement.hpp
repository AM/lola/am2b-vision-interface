/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/geometry/ssv/SSVLineElement.hpp"
#include "broccoli/geometry/ssv/SSVPointElement.hpp"
#include "broccoli/geometry/ssv/SSVTriangleElement.hpp"
#include "broccoli/memory/SmartVector.hpp"
#include <Eigen/Dense>
#include <Eigen/StdVector>

// Internal libraries
#include "TOMLData.hpp"

namespace am2b {
namespace vision {
    //! Data container for a point/line/triangle SSV element in TOML format
    class TOMLSSVElement : public TOMLData {
        // Construction
        // ------------
    public:
        //! Constructor
        TOMLSSVElement() = default;

        //! Constructor from point-SSV
        TOMLSSVElement(const broccoli::geometry::SSVPointElement& element)
            : m_radius(element.radius())
        {
            m_points.resize(1);
            m_points[0] = element.point0();
        }

        //! Constructor from line-SSV
        TOMLSSVElement(const broccoli::geometry::SSVLineElement& element)
            : m_radius(element.radius())
        {
            m_points.resize(2);
            m_points[0] = element.point0();
            m_points[1] = element.point1();
        }

        //! Constructor from triangle-SSV
        TOMLSSVElement(const broccoli::geometry::SSVTriangleElement& element)
            : m_radius(element.radius())
        {
            m_points.resize(3);
            m_points[0] = element.point0();
            m_points[1] = element.point1();
            m_points[2] = element.point2();
        }

        // Members
        // -------
    public:
        broccoli::memory::SmartVector<Eigen::Vector3d, 3, Eigen::aligned_allocator<Eigen::Vector3d>> m_points; //!< List of points (1=point, 2=line, 3=triangle)
        double m_radius = 0.0; //!< Radius of the element

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + context.encodeValuePhantom("Points", m_points, "Radius", m_radius);
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            context.encodeSectionHeader();
            context.encodeValue("Points", m_points, "Radius", m_radius);
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            if (context.decodeValue("Points", m_points, "Radius", m_radius) == false)
                return false;
            if (m_points.size() == 0 || m_points.size() > 3)
                return context.handleError("Could not decode points! Count of points has to be either 1, 2, or 3!");
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
