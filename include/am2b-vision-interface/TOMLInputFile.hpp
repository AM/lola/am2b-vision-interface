/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLInputData.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of a data object which can be read from a TOML file
    class TOMLInputFile : public TOMLInputData {
    public:
        //! Reads data of this container from a TOML file
        /*!
         * \param [in] filePath Path to the input file
         * \param [in] compression Specifies compression options
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromTOMLFile(const std::string& filePath, const broccoli::io::compression::DecompressionOptions& compression, std::string* const error = nullptr)
        {
            // Read file
            broccoli::io::encoding::CharacterStream stream;
            if (broccoli::io::filesystem::readFile(filePath, stream, compression) == false) {
                if (error != nullptr)
                    *error = "Failed to read TOML file '" + filePath + "'! File IO error!";
                return false;
            }

            // Parse TOML structure
            TOMLDecodingContext context(stream);
            if (context.isValid() == false) {
                if (error != nullptr)
                    *error = "Failed to read TOML file '" + filePath + "'! Context invalid! " + context.errorsStacked();
                return false;
            }

            // Decode
            if (decodeFromTOML(context) == false) {
                if (error != nullptr)
                    *error = "Failed to read TOML file '" + filePath + "'! Decoding failed! " + context.errorsStacked();
                return false;
            }

            // Success
            return true;
        }

        //! Reads data of this container from a TOML file (automatically detects compression options)
        /*!
         * \param [in] filePath Path to the input file
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromTOMLFile(const std::string& filePath, std::string* const error = nullptr) { return readFromTOMLFile(filePath, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), error); }
    };
} // namespace vision
} // namespace am2b
