/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include <Eigen/Dense>
#include <Eigen/StdVector>

// Internal libraries
#include "InterfaceContainer.hpp"
#include "Signal.hpp"
#include "ViconFrame.hpp"

namespace am2b {
namespace vision {
    //! Data container for the data sent from the control system to the vision system
    /*!
     * Notes on provided pose data
     * ---------------------------
     * The provided pose data of the vision-TCP and torso-IMU frame use a data-fusion of odometry and torso-IMU data. In particular
     * they are computed from
     *   * the 6D pose of the torso (root body of the topology) and
     *   * the joint angles for each segment of the robot.
     *
     * The 6D pose of the torso is computed from
     *   * planned ("ideal") data: position (x, y, z), translational velocity (x, y, z) and orientation around the VERTICAL axis (z) and
     *   * measured ("real") data: orientation around HORIZONTAL axes (x, y) and angular velocity (x, y, z).
     *
     * The joint angles (and angular velocities) are obtained from the mounted encoders (sensors) and reflect the "real" configuration
     * of the robot.
     *
     * Notes on provided raw IMU sensor data
     * -------------------------------------
     * The provided *raw* sensor data of the high-precision IMU located in the robot's torso may be used for extended sensor data fusion.
     * Note that the provided pose data (see above) already uses (parts of) the sensor data of this IMU.
     */
    class ControlToVisionContainer : public InterfaceContainer {
        // Construction
        // ------------
    public:
        //! Default constructor
        ControlToVisionContainer() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ControlToVisionContainer& reference) const
        {
            return //
                // Synchronous data
                // ----------------
                // Timing
                m_timeMaster == reference.m_timeMaster && //
                m_timeDataAcquisition == reference.m_timeDataAcquisition && //
                m_timeSending == reference.m_timeSending && //
                m_timeReceiving == reference.m_timeReceiving && //
                // Pose of vision-TCP frame (in planning-world frame)
                m_PW_r_visionTCP == reference.m_PW_r_visionTCP && //
                m_PW_q_visionTCP.coeffs() == reference.m_PW_q_visionTCP.coeffs() && //
                m_PW_dotr_visionTCP == reference.m_PW_dotr_visionTCP && //
                m_PW_omega_visionTCP == reference.m_PW_omega_visionTCP && //
                // Pose of torso-IMU frame (in planning-world frame)
                m_PW_r_torsoIMU == reference.m_PW_r_torsoIMU && //
                m_PW_q_torsoIMU.coeffs() == reference.m_PW_q_torsoIMU.coeffs() && //
                m_PW_dotr_torsoIMU == reference.m_PW_dotr_torsoIMU && //
                m_PW_omega_torsoIMU == reference.m_PW_omega_torsoIMU && //
                // Raw sensor data of high-precision torso-IMU (in torso-IMU frame)
                m_torsoIMU_rawIMURollPitchYaw == reference.m_torsoIMU_rawIMURollPitchYaw && //
                m_torsoIMU_rawIMUAngularVelocity == reference.m_torsoIMU_rawIMUAngularVelocity && //
                m_torsoIMU_rawIMUAcceleration == reference.m_torsoIMU_rawIMUAcceleration && //
                // Extrinsic data
                m_viconFrame == reference.m_viconFrame & //
                // Asynchronous data
                // -----------------
                // High-level signals
                m_signals == reference.m_signals;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ControlToVisionContainer& reference) const { return !operator==(reference); }

        // Members (SYNCHRONOUS)
        // -------
    public:
        /* Timing
         * ------
         * Both systems (control and vision) have to use a common time basis which is called "master clock" in the following. The master
         * clock is linked to the synchronized real-time communication bus of the control system. The control system cyclically sends the
         * "current" time according to the master clock to the vision system. The vision system should use this information to synchronize
         * an internal clock used for communication with the control system.
         */
        double m_timeMaster = 0; //!< Current time [s] according to the master clock
        double m_timeDataAcquisition = 0; //!< Time [s] of data aquisition (reading sensor input) according to the master clock
        double m_timeSending = 0; //!< Time [s] of sending this message according to the master clock (to be set by the sender)
        double m_timeReceiving = 0; //!< Time [s] of receiving this message according to the master clock (to be set by the receiver)

        /* Pose of vision-TCP frame (in planning-world frame)
         * ------------------------
         * The vision-TCP frame is a coordinate system which is fixed to the camera assembly, i.e., there is no relative motion to the cameras.
         * However, it does NOT represent the optical center of the cameras. The transformation between vision-TCP and the optical center of the
         * cameras has to be identified through a calibration of the vision system. The vision-TCP frame is defined such that the positive x-axis
         * points in walking direction, the positive y-axis points to the left of the robot and the positive z-axis points upwards.
         */
        Eigen::Vector3d m_PW_r_visionTCP = Eigen::Vector3d::Zero(); //!< Position of vision-TCP frame relative to planning-world frame measured in planning-world frame [m]
        Eigen::Quaterniond m_PW_q_visionTCP = Eigen::Quaterniond::Identity(); //!< Orientation of vision-TCP frame relative to planning-world frame
        Eigen::Vector3d m_PW_dotr_visionTCP = Eigen::Vector3d::Zero(); //!< Absolute translational velocity of vision-TCP frame measured in planning-world frame [m/s]
        Eigen::Vector3d m_PW_omega_visionTCP = Eigen::Vector3d::Zero(); //!< Absolute angular velocity of vision-TCP frame measured in planning-world frame [rad/s]

        /* Pose of torso-IMU frame (in planning-world frame)
         * -----------------------
         * The torso-IMU frame is a coordinate system linked to the high-precision IMU mounted in the torso of the robot. It is aligned such that
         * the positive x-axis points in walking direction, the positive y-axis points to the left of the robot and the positive z-axis points upwards.
         * The origin is set to the "measurement" center, i.e. the (mean) location of the accelerometers and gyroscopes within the IMU.
         */
        Eigen::Vector3d m_PW_r_torsoIMU = Eigen::Vector3d::Zero(); //!< Position of torso-IMU frame relative to planning-world frame measured in planning-world frame [m]
        Eigen::Quaterniond m_PW_q_torsoIMU = Eigen::Quaterniond::Identity(); //!< Orientation of torso-IMU frame relative to planning-world frame
        Eigen::Vector3d m_PW_dotr_torsoIMU = Eigen::Vector3d::Zero(); //!< Absolute translational velocity of torso-IMU frame measured in planning-world frame [m/s]
        Eigen::Vector3d m_PW_omega_torsoIMU = Eigen::Vector3d::Zero(); //!< Absolute angular velocity of torso-IMU frame measured in planning-world frame [rad/s]

        /* Raw sensor data of high-precision torso-IMU (in torso-IMU frame)
         * -------------------------------------------
         * This data is directly obtained from the high-precision IMU located in the torso of the robot (unfiltered!).
         * IMPORTANT:
         *   * The data is represented in the torso-IMU frame which is fixed to the IMU (moving!).
         *   * The yaw-angle is initialized with a random value since the IMU does not have a magnetometer. Moreover, the yaw angle is subject to drift (in contrast to roll and pitch)
         */
        Eigen::Vector3d m_torsoIMU_rawIMURollPitchYaw = Eigen::Vector3d::Zero(); //!< Raw sensor data of torso-IMU: \f$ [roll, pitch, yaw]^T \f$ describing rotation between the IMU and the "world" (roll- and pitch: gravity and drift-compensated)
        Eigen::Vector3d m_torsoIMU_rawIMUAngularVelocity = Eigen::Vector3d::Zero(); //!< Raw sensor data of torso-IMU: absolute angular velocity of IMU measured in torso-IMU frame
        Eigen::Vector3d m_torsoIMU_rawIMUAcceleration = Eigen::Vector3d::Zero(); //!< Raw sensor data of torso-IMU: absolute translational acceleration of IMU measured in torso-IMU frame

        /* Extrinsic data
         * --------------
         * This sections contains data obtained from extrinsic components, which may be used for logging or offline-comparisons (ground-truth). However, these data MUST NOT
         * be used by the online / onboard algorithms since this would violate the rule of AUTONOMY: NO EXTERNAL SENSORS!
         */
        ViconFrame m_viconFrame; //!< Data container for the last received Vicon data frame (including general status information and current state of tracked objects)

        // Members (ASYNCHRONOUS)
        // -------
    public:
        // High-level signals
        std::vector<Signal> m_signals; //!< List of high-level signals to the vision system (first element = oldest, last element = newest)

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_timeMaster, m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                       m_PW_r_visionTCP, m_PW_q_visionTCP, m_PW_dotr_visionTCP, m_PW_omega_visionTCP, //
                       m_PW_r_torsoIMU, m_PW_q_torsoIMU, m_PW_dotr_torsoIMU, m_PW_omega_torsoIMU, //
                       m_torsoIMU_rawIMURollPitchYaw, m_torsoIMU_rawIMUAngularVelocity, m_torsoIMU_rawIMUAcceleration, //
                       m_viconFrame, //
                       m_signals);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            stream.reserve(stream.size() + (3 * estimatedMaximumBinaryStreamSize()) / 2 /* <-- + 50% */);
            return broccoli::io::serialization::serialize(stream, endianness, //
                m_timeMaster, m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                m_PW_r_visionTCP, m_PW_q_visionTCP, m_PW_dotr_visionTCP, m_PW_omega_visionTCP, //
                m_PW_r_torsoIMU, m_PW_q_torsoIMU, m_PW_dotr_torsoIMU, m_PW_omega_torsoIMU, //
                m_torsoIMU_rawIMURollPitchYaw, m_torsoIMU_rawIMUAngularVelocity, m_torsoIMU_rawIMUAcceleration, //
                m_viconFrame, //
                m_signals);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, //
                m_timeMaster, m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                m_PW_r_visionTCP, m_PW_q_visionTCP, m_PW_dotr_visionTCP, m_PW_omega_visionTCP, //
                m_PW_r_torsoIMU, m_PW_q_torsoIMU, m_PW_dotr_torsoIMU, m_PW_omega_torsoIMU, //
                m_torsoIMU_rawIMURollPitchYaw, m_torsoIMU_rawIMUAngularVelocity, m_torsoIMU_rawIMUAcceleration, //
                m_viconFrame, //
                m_signals);
        }

        // Input/Output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            broccoli::io::encoding::CharacterStreamSize returnValue = 0;

            // Header
            returnValue += context.encodePlainPhantom(defaultTOMLFileHeader());

            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLEncodingContext contextTiming(context, "Timing");
            returnValue += contextTiming.encodeSectionHeaderPhantom();
            returnValue += contextTiming.encodeValuePhantom("TimeMaster", m_timeMaster, "TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving);

            // Pose of vision-TCP frame (in planning-world frame)
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            returnValue += contextVisionTCPInPlanningWorld.encodeSectionHeaderPhantom();
            returnValue += contextVisionTCPInPlanningWorld.encodeValuePhantom("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP, "VelocityXYZ", m_PW_dotr_visionTCP, "AngularVelocityXYZ", m_PW_omega_visionTCP);

            // Pose of torso-IMU frame (in planning-world frame)
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextTorsoIMUInPlanningWorld(context, "TorsoIMUInPlanningWorld");
            returnValue += contextTorsoIMUInPlanningWorld.encodeSectionHeaderPhantom();
            returnValue += contextTorsoIMUInPlanningWorld.encodeValuePhantom("PositionXYZ", m_PW_r_torsoIMU, "OrientationWXYZ", m_PW_q_torsoIMU, "VelocityXYZ", m_PW_dotr_torsoIMU, "AngularVelocityXYZ", m_PW_omega_torsoIMU);

            // Raw sensor data of high-precision torso-IMU (in torso-IMU frame)
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextRawIMUSensorData(context, "RawIMUSensorData");
            returnValue += contextRawIMUSensorData.encodeSectionHeaderPhantom();
            returnValue += contextRawIMUSensorData.encodeValuePhantom("RollPitchYaw", m_torsoIMU_rawIMURollPitchYaw, "AngularVelocityXYZ", m_torsoIMU_rawIMUAngularVelocity, "AccelerationXYZ", m_torsoIMU_rawIMUAcceleration);

            // Extrinsic data
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextViconFrame(context, "ViconFrame");
            returnValue += m_viconFrame.estimatedMaximumTOMLStreamSize(contextViconFrame);

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            returnValue += context.encodeLineBreakPhantom();
            returnValue += estimatedSortedListMaximumTOMLStreamSize(context, "Signals", m_signals);

            // Return sum
            return returnValue;
        }

        // Encodes **this** object to the given stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            // Pre-allocate memory
            context.stream().reserve(context.stream().size() + (3 * estimatedMaximumTOMLStreamSize(context)) / 2 /* <-- +50% */);

            // Header
            context.encodePlain(defaultTOMLFileHeader());

            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLEncodingContext contextTiming(context, "Timing");
            contextTiming.encodeSectionHeader();
            contextTiming.encodeValue("TimeMaster", m_timeMaster, "TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving);

            // Pose of vision-TCP frame (in planning-world frame)
            context.encodeLineBreak();
            const TOMLEncodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            contextVisionTCPInPlanningWorld.encodeSectionHeader();
            contextVisionTCPInPlanningWorld.encodeValue("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP, "VelocityXYZ", m_PW_dotr_visionTCP, "AngularVelocityXYZ", m_PW_omega_visionTCP);

            // Pose of torso-IMU frame (in planning-world frame)
            context.encodeLineBreak();
            const TOMLEncodingContext contextTorsoIMUInPlanningWorld(context, "TorsoIMUInPlanningWorld");
            contextTorsoIMUInPlanningWorld.encodeSectionHeader();
            contextTorsoIMUInPlanningWorld.encodeValue("PositionXYZ", m_PW_r_torsoIMU, "OrientationWXYZ", m_PW_q_torsoIMU, "VelocityXYZ", m_PW_dotr_torsoIMU, "AngularVelocityXYZ", m_PW_omega_torsoIMU);

            // Raw sensor data of high-precision torso-IMU (in torso-IMU frame)
            context.encodeLineBreak();
            const TOMLEncodingContext contextRawIMUSensorData(context, "RawIMUSensorData");
            contextRawIMUSensorData.encodeSectionHeader();
            contextRawIMUSensorData.encodeValue("RollPitchYaw", m_torsoIMU_rawIMURollPitchYaw, "AngularVelocityXYZ", m_torsoIMU_rawIMUAngularVelocity, "AccelerationXYZ", m_torsoIMU_rawIMUAcceleration);

            // Extrinsic data
            context.encodeLineBreak();
            const TOMLEncodingContext contextViconFrame(context, "ViconFrame");
            if (m_viconFrame.encodeToTOML(contextViconFrame) == false)
                return false;

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            context.encodeLineBreak();
            if (encodeSortedListToTOML(context, "Signals", m_signals) == false)
                return false;

            // Success
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLDecodingContext contextTiming(context, "Timing");
            if (contextTiming.isValid() == false || contextTiming.decodeValue("TimeMaster", m_timeMaster, "TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving) == false)
                return false;

            // Pose of vision-TCP frame (in planning-world frame)
            const TOMLDecodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            if (contextVisionTCPInPlanningWorld.isValid() == false || contextVisionTCPInPlanningWorld.decodeValue("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP, "VelocityXYZ", m_PW_dotr_visionTCP, "AngularVelocityXYZ", m_PW_omega_visionTCP) == false)
                return false;

            // Pose of torso-IMU frame (in planning-world frame)
            const TOMLDecodingContext contextTorsoIMUInPlanningWorld(context, "TorsoIMUInPlanningWorld");
            if (contextTorsoIMUInPlanningWorld.isValid() == false || contextTorsoIMUInPlanningWorld.decodeValue("PositionXYZ", m_PW_r_torsoIMU, "OrientationWXYZ", m_PW_q_torsoIMU, "VelocityXYZ", m_PW_dotr_torsoIMU, "AngularVelocityXYZ", m_PW_omega_torsoIMU) == false)
                return false;

            // Raw sensor data of high-precision torso-IMU (in torso-IMU frame)
            const TOMLDecodingContext contextRawIMUSensorData(context, "RawIMUSensorData");
            if (contextRawIMUSensorData.isValid() == false || contextRawIMUSensorData.decodeValue("RollPitchYaw", m_torsoIMU_rawIMURollPitchYaw, "AngularVelocityXYZ", m_torsoIMU_rawIMUAngularVelocity, "AccelerationXYZ", m_torsoIMU_rawIMUAcceleration) == false)
                return false;

            // Extrinsic data
            const TOMLDecodingContext contextViconFrame(context, "ViconFrame");
            if (contextViconFrame.isValid() == false || m_viconFrame.decodeFromTOML(contextViconFrame) == false)
                return false;

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            if (decodeSortedListFromTOML(context, "Signals", m_signals) == false)
                return false;

            // Success
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
