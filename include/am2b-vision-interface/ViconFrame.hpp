/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <stdint.h>

// External libraries
#include <Eigen/StdVector>

// Internal libraries
#include "ViconObject.hpp"

namespace am2b {
namespace vision {
    //! Data container for a complete Vicon data frame (including general status information and current state of tracked objects)
    class ViconFrame : public BinaryData, public TOMLData {
        // Construction
        // ------------
    public:
        //! Constructor
        ViconFrame() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ViconFrame& reference) const
        {
            return m_frameNumber == reference.m_frameNumber && //
                m_totalLatency == reference.m_totalLatency && //
                m_frameRate == reference.m_frameRate && //
                m_objects == reference.m_objects;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ViconFrame& reference) const { return !operator==(reference); }

        // Members
        // -------
    public:
        uint64_t m_frameNumber = 0; //!< Number of the current frame
        double m_totalLatency = 0; //!< Total latency of the whole pipeline [s]
        double m_frameRate = 0; //!< Camera framerate [Hz]
        std::vector<ViconObject, Eigen::aligned_allocator<ViconObject>> m_objects; //!< List of tracked objects

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_frameNumber, m_totalLatency, m_frameRate, m_objects);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_frameNumber, m_totalLatency, m_frameRate, m_objects);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_frameNumber, m_totalLatency, m_frameRate, m_objects);
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + //
                context.encodeValuePhantom("FrameNumber", m_frameNumber, "TotalLatency", m_totalLatency, "FrameRate", m_frameRate) + //
                estimatedSortedListMaximumTOMLStreamSize(context, "Objects", m_objects);
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            context.encodeSectionHeader();
            context.encodeValue("FrameNumber", m_frameNumber, "TotalLatency", m_totalLatency, "FrameRate", m_frameRate);
            if (encodeSortedListToTOML(context, "Objects", m_objects) == false)
                return false;
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            if (context.decodeValue("FrameNumber", m_frameNumber, "TotalLatency", m_totalLatency, "FrameRate", m_frameRate) == false || //
                decodeSortedListFromTOML(context, "Objects", m_objects) == false)
                return false;
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
