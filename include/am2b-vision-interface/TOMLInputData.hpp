/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLDecodingContext.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of data objects which can be decoded from the TOML format
    class TOMLInputData {
        // Constructor
        // -----------
    public:
        //! Default constructor
        TOMLInputData() = default;

        //! Virtual destructor
        virtual ~TOMLInputData() {}

        // Main Interface
        // --------------
    public:
        //! Decodes **this** object from the context
        /*!
         * \param [in] context Current TOML context
         * \return `true` on success, `false` otherwise
         */
        virtual bool decodeFromTOML(const TOMLDecodingContext& context) = 0;

        // Helpers
        // -------
    public:
        //! Decodes a list of TOML data objects from the TOML context
        /*!
         * \param [in] context Current TOML context
         * \param [in] sectionNameLocal Local section name of the (governing) section
         * \param [in] sortedList Sorted list to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename SortedList>
        static inline bool decodeSortedListFromTOML(const TOMLDecodingContext& context, const std::string& sectionNameLocal, SortedList& sortedList)
        {
            const TOMLDecodingContext contextList(context, sectionNameLocal);
            if (contextList.isValid() == false)
                return false;
            std::vector<TOMLDecodingContext> sortedSections;
            if (contextList.getSortedSubSections(sortedSections) == false)
                return false;
            sortedList.clear();
            sortedList.resize(sortedSections.size());
            for (size_t i = 0; i < sortedSections.size(); i++) {
                if (sortedList[i].decodeFromTOML(sortedSections[i]) == false)
                    return false;
            }
            return true;
        }
    };
} // namespace vision
} // namespace am2b
