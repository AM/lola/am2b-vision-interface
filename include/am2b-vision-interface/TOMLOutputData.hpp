/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLEncodingContext.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of data objects which can be encoded to the TOML format
    class TOMLOutputData {
        // Constructor
        // -----------
    public:
        //! Default constructor
        TOMLOutputData() = default;

        //! Virtual destructor
        virtual ~TOMLOutputData() {}

        // Main Interface
        // --------------
    public:
        //! Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes)
        /*!
         * \param [in] context Current TOML context
         * \return Estimated count of bytes in the uncompressed TOML stream
         */
        virtual broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const = 0;

        //! Encodes **this** object to the stream
        /*!
         * \param [in] context Current TOML context
         * \return `true` on success, `false` otherwise
         */
        virtual bool encodeToTOML(const TOMLEncodingContext& context) const = 0;

        // Helpers
        // -------
    public:
        //! Returns an estimate for the maximum occupied space of the given sorted list of TOML data objects in an uncompressed TOML stream (total count of bytes)
        /*!
         * \param [in] context Current TOML context
         * \param [in] sectionNameLocal Local section name of the (governing) section
         * \param [in] sortedList Sorted list to encode
         * \return Estimated count of bytes in the uncompressed TOML stream
         */
        template <typename SortedList>
        static inline broccoli::io::encoding::CharacterStreamSize estimatedSortedListMaximumTOMLStreamSize(const TOMLEncodingContext& context, const std::string& sectionNameLocal, const SortedList& sortedList)
        {
            const TOMLEncodingContext contextList(context, sectionNameLocal);
            broccoli::io::encoding::CharacterStreamSize returnValue = contextList.encodeSectionHeaderPhantom();
            if (sortedList.size() > 10)
                returnValue += sortedList.size() * sortedList[0].estimatedMaximumTOMLStreamSize(TOMLEncodingContext(contextList, "9999"));
            else {
                for (size_t i = 0; i < sortedList.size(); i++)
                    returnValue += sortedList[i].estimatedMaximumTOMLStreamSize(TOMLEncodingContext(contextList, std::to_string(i)));
            }
            return returnValue;
        }

        //! Encodes a sorted list of TOML data objects to the stream
        /*!
         * \param [in] context Current TOML context
         * \param [in] sectionNameLocal Local section name of the (governing) section
         * \param [in] sortedList Sorted list to encode
         * \return `true` on success, `false` otherwise
         */
        template <typename SortedList>
        static inline bool encodeSortedListToTOML(const TOMLEncodingContext& context, const std::string& sectionNameLocal, const SortedList& sortedList)
        {
            const TOMLEncodingContext contextList(context, sectionNameLocal);
            contextList.encodeSectionHeader();
            for (size_t i = 0; i < sortedList.size(); i++) {
                if (i > 0)
                    contextList.encodeLineBreak();
                if (sortedList[i].encodeToTOML(TOMLEncodingContext(contextList, std::to_string(i))) == false)
                    return false;
            }
            return true;
        }
    };
} // namespace vision
} // namespace am2b
