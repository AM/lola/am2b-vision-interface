/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <algorithm>
#include <istream>
#include <streambuf>

// External libraries
#include "broccoli/core/string.hpp"
#include "broccoli/io/compression.hpp"
#include "broccoli/io/encoding.hpp"
#include "broccoli/io/filesystem.hpp"
#include "broccoli/io/serialization/serialization.hpp"
#include <Eigen/Dense>
#include <Eigen/StdVector>
#include <cpptoml.h>

// Internal libraries
#include "TOMLBaseContext.hpp"

namespace am2b {
namespace vision {
    //! Context used for decoding from TOML
    class TOMLDecodingContext : public TOMLBaseContext {
        // Construction
        // ------------
    public:
        //! Specialized constructor (for root context)
        TOMLDecodingContext(const broccoli::io::encoding::CharacterStream& stream)
        {
            struct StreamBuffer : std::streambuf {
                StreamBuffer(const char* data, const size_t& length)
                {
                    char* begin(const_cast<char*>(data));
                    this->setg(begin, begin, begin + length);
                }
            };
            struct InputStream : virtual StreamBuffer, std::istream {
                InputStream(const char* data, const size_t& length)
                    : StreamBuffer(data, length)
                    , std::istream(static_cast<std::streambuf*>(this))
                {
                }
            };
            try {
                InputStream inputStream(reinterpret_cast<const char*>(stream.data()), stream.size());
                cpptoml::parser parser{ inputStream };
                m_table = parser.parse();
                if (!m_table)
                    handleError("Could not construct TOML decoding context (root): pointer to root table is invalid!");
            } catch (const cpptoml::parse_exception& e) {
                handleError("Could not construct TOML decoding context (root): parsing error: " + std::string(e.what()));
            }
        }

        //! Specialized constructor (for child context)
        /*!
         * \param [in] parent The parent context
         * \param [in] sectionNameLocal Initializes \ref sectionNameLocal() - \copybrief sectionNameLocal()
         */
        TOMLDecodingContext(const TOMLDecodingContext& parent, const std::string& sectionNameLocal)
            : TOMLBaseContext(parent, sectionNameLocal)
        {
            if (!parent.m_table)
                handleError("Could not construct TOML decoding context (child): pointer to parent table is invalid!");
            else {
                m_table = parent.m_table->get_table(sectionNameLocal);
                if (!m_table)
                    handleError("Could not find table '" + sectionNameLocal + "' in '" + parent.m_sectionNameGlobal + "'!");
            }
        }

        //! Specialized constructor (for child context)
        /*!
         * \param [in] parent The parent context
         * \param [in] sectionNameLocal Initializes \ref sectionNameLocal() - \copybrief sectionNameLocal()
         * \param [in] table Initializes \ref table() - \copybrief table()
         */
        TOMLDecodingContext(const TOMLDecodingContext& parent, const std::string& sectionNameLocal, const std::shared_ptr<cpptoml::table>& table)
            : TOMLBaseContext(parent, sectionNameLocal)
            , m_table(table)
        {
        }

        // Members
        // -------
    protected:
        std::shared_ptr<cpptoml::table> m_table; //!< \copybrief table()

        // Getters
        // -------
    public:
        //! Checks, if the context is valid
        inline bool isValid() const { return m_table != nullptr; }
        //! Pointer to current table / section
        inline const std::shared_ptr<cpptoml::table>& table() const { return m_table; }

        // Decoding
        // --------
    public:
        //! Extracts a list of subsections from the context
        /*! \return List of subsections */
        inline std::vector<TOMLDecodingContext> getSubSections() const
        {
            assert(isValid());
            std::vector<TOMLDecodingContext> subSections;
            subSections.reserve(std::distance(m_table->begin(), m_table->end()));
            for (auto iterator = m_table->begin(); iterator != m_table->end(); ++iterator) {
                std::shared_ptr<cpptoml::table> subTable = iterator->second->as_table();
                if (subTable != nullptr)
                    subSections.emplace_back(*this, iterator->first, subTable);
            }
            return subSections;
        }

        //! Extracts a **sorted** list of subsections from the context (sorted according to local section name which has to be an unsigned integer)
        /*!
         * \param [out] sortedSubSections Sorted list of subsections
         * \return `true` on success, `false` otherwise
         */
        inline bool getSortedSubSections(std::vector<TOMLDecodingContext>& sortedSubSections) const
        {
            assert(isValid());
            using namespace broccoli::io;

            // Reset input
            sortedSubSections.clear();

            //! Element of a list of sorted subtables
            class SortedSubTable {
                // Construction
                // ------------
            public:
                //! Specialized constructor
                SortedSubTable(const uint64_t& keyAsInteger, const std::string& keyAsString, const std::shared_ptr<cpptoml::table>& table)
                    : m_keyAsInteger(keyAsInteger)
                    , m_keyAsString(keyAsString)
                    , m_table(table)
                {
                }

                // Members
                // -------
            public:
                uint64_t m_keyAsInteger = 0; //!< Key of table as unsigned integer
                std::string m_keyAsString = ""; //!< Key of table as string
                std::shared_ptr<cpptoml::table> m_table; //!< Pointer to table

                // Operators
                // ---------
            public:
                //! Comparison operator used for sorting
                inline bool operator<(const SortedSubTable& other) const { return m_keyAsInteger < other.m_keyAsInteger; }
            };

            // Pre-allocate memory
            std::vector<SortedSubTable> subTables;
            subTables.reserve(std::distance(m_table->begin(), m_table->end()));

            // Fill list of tables
            for (auto iterator = m_table->begin(); iterator != m_table->end(); ++iterator) {
                std::shared_ptr<cpptoml::table> subTable = iterator->second->as_table();
                if (subTable != nullptr) {
                    const std::string& keyAsString = iterator->first;
                    uint64_t keyAsInteger = 0;
                    if (encoding::decodeFromString(keyAsInteger, keyAsString) == false)
                        return handleError("Could not extract sorted subsection '" + keyAsString + "'! Section name '" + keyAsString + "' has to be an unsigned integer!");
                    subTables.emplace_back(keyAsInteger, keyAsString, subTable);
                }
            }

            // Abort, if there are no subtables
            if (subTables.size() == 0)
                return true;

            // Sort in ascending order
            std::sort(subTables.begin(), subTables.end());

            // Check for duplicate keys
            for (size_t i = 0; i < subTables.size() - 1; i++) {
                if (subTables[i + 1].m_keyAsInteger == subTables[i].m_keyAsInteger)
                    return handleError("Could not create sorted list of subsections! Duplicate section name '" + subTables[i].m_keyAsString + "' found!");
            }

            // Setup list of subsections from tables
            sortedSubSections.reserve(subTables.size());
            for (size_t i = 0; i < subTables.size(); i++)
                sortedSubSections.emplace_back(*this, subTables[i].m_keyAsString, subTables[i].m_table);

            // Success
            return true;
        }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T, typename std::enable_if<std::is_integral<T>::value || std::is_floating_point<T>::value || std::is_same<T, std::string>::value, int>::type = 0>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, T& data) const
        {
            try {
                const auto dataOption = cpptoml::get_impl<T>(tomlElement);
                if (!dataOption)
                    return handleError("Could not decode value of key '" + key + "'! Invalid data type!");
                data = *dataOption;
            } catch (const std::out_of_range&) {
                return handleError("Could not decode value of key '" + key + "'! Invalid data type!");
            }
            return true;
        }

        //! Decodes a raw fixed-size array from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T>
        inline bool decodeRawFixedSizeArray(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, T& data) const
        {
            const auto arrayOption = tomlElement->as_array();
            if (!arrayOption)
                return handleError("Could not decode value of key '" + key + "'! Invalid data type (expected an array)!");
            const std::vector<std::shared_ptr<cpptoml::base>>& array = arrayOption->get();
            if (array.size() != (size_t)data.size())
                return handleError("Could not decode value of key '" + key + "'! Array has invalid dimensions (expected: " + std::to_string(data.size()) + ", actual: " + std::to_string(array.size()) + ")!");
            for (size_t i = 0; i < array.size(); i++) {
                if (decodeRawValue(key, array[i], data[i]) == false)
                    return false;
            }
            return true;
        }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T, size_t N>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, std::array<T, N>& data) const { return decodeRawFixedSizeArray(key, tomlElement, data); }

        //! Decodes a raw dynamic-size array from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T>
        inline bool decodeRawDynamicSizeArray(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, T& data) const
        {
            const auto arrayOption = tomlElement->as_array();
            if (!arrayOption)
                return handleError("Could not decode value of key '" + key + "'! Invalid data type (expected an array)!");
            const std::vector<std::shared_ptr<cpptoml::base>>& array = arrayOption->get();
            data.resize(array.size());
            for (size_t i = 0; i < array.size(); i++) {
                if (decodeRawValue(key, array[i], data[i]) == false)
                    return false;
            }
            return true;
        }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T, typename Allocator = std::allocator<T>>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, std::vector<T, Allocator>& data) const { return decodeRawDynamicSizeArray(key, tomlElement, data); }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T, size_t MaximumStaticElementCount = 0, typename Allocator = std::allocator<T>>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, broccoli::memory::SmartVector<T, MaximumStaticElementCount, Allocator>& data) const { return decodeRawDynamicSizeArray(key, tomlElement, data); }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename Derived>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, Eigen::MatrixBase<Derived>& data) const
        {
            // Scalar
            if (data.RowsAtCompileTime != Eigen::Dynamic && data.rows() == 1 && data.ColsAtCompileTime != Eigen::Dynamic && data.cols() == 1)
                return decodeRawValue(key, tomlElement, data(0, 0));

            // Vector or matrix -> toml array
            const auto firstLevelArrayOption = tomlElement->as_array();
            if (!firstLevelArrayOption)
                return handleError("Could not decode value of key '" + key + "'! Invalid data type (expected an array)!");
            const std::vector<std::shared_ptr<cpptoml::base>>& firstLevelArray = firstLevelArrayOption->get();
            if (data.RowsAtCompileTime != Eigen::Dynamic && data.rows() == 1) {
                // Guaranteed only 1 row
                if (data.ColsAtCompileTime != Eigen::Dynamic) {
                    // Fixed size row vector
                    if (firstLevelArray.size() != data.cols())
                        return handleError("Could not decode value of key '" + key + "'! Invalid dimension for first-level array (expected: " + std::to_string(data.cols()) + ", actual: " + std::to_string(firstLevelArray.size()) + ")!");
                } else {
                    // Dynamic size row vector
                    data.resize(Eigen::NoChange, firstLevelArray.size());
                }
                for (size_t i = 0; i < firstLevelArray.size(); i++) {
                    if (decodeRawValue(key, firstLevelArray[i], data(0, i)) == false)
                        return false;
                }
            } else {
                // Arbitrary count of rows
                if (data.ColsAtCompileTime != Eigen::Dynamic && data.cols() == 1) {
                    // Guaranteed only 1 column -> column vector
                    if (data.RowsAtCompileTime != Eigen::Dynamic) {
                        // Fixed size column vector
                        if (firstLevelArray.size() != data.rows())
                            return handleError("Could not decode value of key '" + key + "'! Invalid dimension for first-level array (expected: " + std::to_string(data.rows()) + ", actual: " + std::to_string(firstLevelArray.size()) + ")!");
                    } else {
                        // Dynamic size column vector
                        data.resize(firstLevelArray.size(), Eigen::NoChange);
                    }
                    for (size_t i = 0; i < firstLevelArray.size(); i++) {
                        if (decodeRawValue(key, firstLevelArray[i], data(i, 0)) == false)
                            return false;
                    }
                } else {
                    // Arbitrary count of columns -> matrix
                    if (data.RowsAtCompileTime != Eigen::Dynamic) {
                        // Matrix with fixed row count
                        if (firstLevelArray.size() != data.rows())
                            return handleError("Could not decode value of key '" + key + "'! Invalid dimension for first-level array (expected: " + std::to_string(data.rows()) + ", actual: " + std::to_string(firstLevelArray.size()) + ")!");
                    } else {
                        // Matrix with dynamic row count
                        data.resize(firstLevelArray.size(), Eigen::NoChange);
                    }
                    size_t detectedColumnCount = 0;
                    for (size_t i = 0; i < firstLevelArray.size(); i++) {
                        const auto secondLevelArrayOption = firstLevelArray[i]->as_array();
                        if (!secondLevelArrayOption)
                            return handleError("Could not decode value of key '" + key + "'! Invalid data type (expected an 2-dimensional array)!");
                        const std::vector<std::shared_ptr<cpptoml::base>>& secondLevelArray = secondLevelArrayOption->get();
                        if (i == 0) {
                            // First row
                            if (data.ColsAtCompileTime != Eigen::Dynamic) {
                                // Matrix with fixed column count
                                if (secondLevelArray.size() != data.cols())
                                    return handleError("Could not decode value of key '" + key + "'! Invalid dimension for second-level array (expected: " + std::to_string(data.cols()) + ", actual: " + std::to_string(secondLevelArray.size()) + ")!");
                            } else {
                                // Matrix with dynamic column count
                                data.resize(Eigen::NoChange, secondLevelArray.size());
                            }
                            detectedColumnCount = secondLevelArray.size();
                        } else {
                            // Not first row
                            if (secondLevelArray.size() != detectedColumnCount)
                                return handleError("Could not decode value of key '" + key + "'! Inconsistent dimension for second-level array (expected: " + std::to_string(detectedColumnCount) + ", actual: " + std::to_string(secondLevelArray.size()) + ")!");
                        }
                        for (size_t j = 0; j < secondLevelArray.size(); j++) {
                            if (decodeRawValue(key, secondLevelArray[j], data(i, j)) == false)
                                return false;
                        }
                    }
                }
            }

            // Success
            return true;
        }

        //! Decodes a raw value from the given toml element
        /*!
         * \param [in] key TOML key for this element
         * \param [in] tomlElement TOML element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename Derived>
        inline bool decodeRawValue(const std::string& key, const std::shared_ptr<cpptoml::base>& tomlElement, Eigen::QuaternionBase<Derived>& data) const
        {
            const auto arrayOption = tomlElement->as_array();
            if (!arrayOption)
                return handleError("Could not decode value of key '" + key + "'! Invalid data type (expected an array)!");
            const std::vector<std::shared_ptr<cpptoml::base>>& array = arrayOption->get();
            if (array.size() != 4)
                return handleError("Could not decode value of key '" + key + "'! Array has invalid dimensions (expected: 4, actual: " + std::to_string(array.size()) + ")!");
            if (decodeRawValue(key, array[0], data.w()) == false || //
                decodeRawValue(key, array[1], data.x()) == false || //
                decodeRawValue(key, array[2], data.y()) == false || //
                decodeRawValue(key, array[3], data.z()) == false)
                return false;
            return true;
        }

        //! Decodes a value from the context
        /*!
         * \param [in] key TOML key for this element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        template <typename T>
        inline bool decodeValue(const std::string& key, T& data) const
        {
            assert(isValid());
            try {
                const auto tomlElement = m_table->get(key);
                return decodeRawValue(key, tomlElement, data);
            } catch (const std::out_of_range& error) {
                return handleError("Could not find key '" + key + "'!");
            }
        }

        //! Dummy method to terminate recursive call of corresponding variadic template
        inline bool decodeValue() const { return true; }

        //! Decodes a value from the context
        /*! \return `true` on success, `false` otherwise */
        template <typename firstType, typename secondType, typename... remainingTypes>
        inline bool decodeValue(const std::string& firstKey, firstType& firstValue, const std::string& secondKey, secondType& secondValue, remainingTypes&... remainingValues) const
        {
            return decodeValue(firstKey, firstValue) && decodeValue(secondKey, secondValue) && decodeValue(remainingValues...);
        }

        //! Tries to decode an **optional** value from the context
        /*!
         * \param [in] key TOML key for this element
         * \param [out] data The data to decode
         * \return Result as pair of flags [<value-found>, <decoding-successful>]
         */
        template <typename T>
        inline std::pair<bool, bool> decodeOptionalValue(const std::string& key, T& data) const
        {
            assert(isValid());
            std::pair<bool, bool> returnValue(false, false);
            try {
                const auto tomlElement = m_table->get(key);
                returnValue.first = true;
                returnValue.second = decodeRawValue(key, tomlElement, data);
                return returnValue;
            } catch (const std::out_of_range& error) {
                return returnValue;
            }
        }

        //! Decodes a data stream from the context (embedded or external)
        /*!
         * \param [in] keyPrefix TOML key prefix for this element
         * \param [out] data The data to decode
         * \return `true` on success, `false` otherwise
         */
        inline bool decodeDataStream(const std::string& keyPrefix, broccoli::io::serialization::BinaryStream& data) const
        {
            assert(isValid());
            using namespace broccoli::io;

            // Initialize helpers
            bool dataStreamEmbedded = true;
            compression::CompressionOptions dataStreamCompression = compression::CompressionOptions::gzip();
            serialization::BinaryStream compressedDataStream;

            // Search for key prefix
            for (auto iterator = m_table->begin(); iterator != m_table->end(); ++iterator) {
                const std::string currentKey = iterator->first;
                if (broccoli::core::stringStartsWith(currentKey, keyPrefix) == true) {
                    // Parse embedding options from key
                    std::string parsedKey = currentKey;
                    if (broccoli::core::stringEndsWith(parsedKey, "Base64") == true) {
                        dataStreamEmbedded = true;
                        parsedKey.resize(parsedKey.size() - 6);
                    } else if (broccoli::core::stringEndsWith(parsedKey, "Path") == true) {
                        dataStreamEmbedded = false;
                        parsedKey.resize(parsedKey.size() - 4);
                    } else
                        return handleError("Could not decode key '" + currentKey + "'! Key has invalid format!");
                    if (broccoli::core::stringEndsWith(parsedKey, "Raw") == true) {
                        dataStreamCompression = compression::CompressionOptions::raw();
                        parsedKey.resize(parsedKey.size() - 3);
                    } else if (broccoli::core::stringEndsWith(parsedKey, "Zlib") == true) {
                        dataStreamCompression = compression::CompressionOptions::zlib();
                        parsedKey.resize(parsedKey.size() - 4);
                    } else if (broccoli::core::stringEndsWith(parsedKey, "Gzip") == true) {
                        dataStreamCompression = compression::CompressionOptions::gzip();
                        parsedKey.resize(parsedKey.size() - 4);
                    } else
                        dataStreamCompression = compression::CompressionOptions::noCompression();
                    if (parsedKey != keyPrefix)
                        return handleError("Could not decode key '" + currentKey + "'! Key has invalid format!");

                    // Get text stream
                    const auto textStream = iterator->second->as<std::string>();
                    if (!textStream)
                        return handleError("Could not decode key '" + currentKey + "'! Invalid data type (expected string)!");

                    // Decode data
                    if (dataStreamEmbedded == true) {
                        if (dataStreamCompression.compressed() == false) {
                            if (encoding::decodeFromBase64((*textStream).get(), data) == false)
                                return handleError("Could not decode key '" + currentKey + "'! Could not decode base64 stream!");
                        } else {
                            if (encoding::decodeFromBase64((*textStream).get(), compressedDataStream) == false)
                                return handleError("Could not decode key '" + currentKey + "'! Could not decode base64 stream!");
                            if (broccoli::io::compression::decompress(compressedDataStream, data, dataStreamCompression) == false)
                                return handleError("Could not decode key '" + currentKey + "'! Could not decompress stream!");
                        }
                    } else {
                        if (filesystem::readFile((*textStream).get(), data, dataStreamCompression) == false)
                            return handleError("Could not decode key '" + currentKey + "'! Could not read file '" + (*textStream).get() + "'!");
                    }

                    // Success!
                    return true;
                }
            }

            // Key not found!
            return handleError("Could not find key with prefix '" + keyPrefix + "'!");
        }
    };
} // namespace vision
} // namespace am2b
