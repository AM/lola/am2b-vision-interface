/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <string>

// External libraries
#include <Eigen/Dense>

// Internal libraries
#include "ObjectSurfaceModel.hpp"
#include "ObjectVolumeModel.hpp"

namespace am2b {
namespace vision {
    //! Specification of an object as part of the environment model
    /*!
     * Each object has following properties
     *   * ID (unique)
     *   * Classification of the object
     *   * Pose (position and orientation) relative to vision-world frame
     *   * Approximation of the surface described in the local object coordinate system
     *   * Approximation of the volume described in the local object coordinate system
     *
     * Note that the surface and volume model are described in the local object coordinate system. Thus, if the object moves (without changing its shape),
     * only the pose has to be updated.
     */
    class ObjectSpecification {
        // Type definitions
        // ----------------
    public:
        using ID = broccoli::geometry::SSVSegment::ID; //!< **Unique** identifier of the object
        using ClassificationType = uint32_t;
        using Position = Eigen::Vector3d; //!< Position of the object frame relative to the vision-world frame measured in the vision-world frame [m]
        using Orientation = Eigen::Quaterniond; //!< Orientation of object frame relative to the vision-world frame
        using Confidence = CGMeshExtended::Confidence;
        using SurfaceModel = ObjectSurfaceModel; //!< Model of the object's surface described in local object coordinate system
        using VolumeModel = ObjectVolumeModel; //!< Model of the object's volume described in local object coordinate system

        //! Classification of objects (specifies the type of the object)
        enum class Classification : ClassificationType {
            UNKNOWN = 0, //!< Unknown object type
            WALL,
            FLOOR,
            CABINET,
            BED,
            CHAIR,
            SOFA,
            TABLE,
            DOOR,
            WINDOW,
            BOOKSHELF,
            PICTURE,
            COUNTER,
            DESK,
            CURTAIN,
            REFRIDGERATOR,
            SHOWER_CURTAIN,
            TOILET,
            SINK,
            BATHTUB,
            OTHER_FURNITURE,
            CLASSIFICATION_COUNT //!< Total count of elements
        };

        //! Returns the string representation of the given classification
        static inline std::string toString(const Classification& classification)
        {
            if (classification == Classification::UNKNOWN)
                return "UNKNOWN";
            if (classification == Classification::WALL)
                return "WALL";
            if (classification == Classification::FLOOR)
                return "FLOOR";
            if (classification == Classification::CABINET)
                return "CABINET";
            if (classification == Classification::BED)
                return "BED";
            if (classification == Classification::CHAIR)
                return "CHAIR";
            if (classification == Classification::SOFA)
                return "SOFA";
            if (classification == Classification::TABLE)
                return "TABLE";
            if (classification == Classification::DOOR)
                return "DOOR";
            if (classification == Classification::WINDOW)
                return "WINDOW";
            if (classification == Classification::BOOKSHELF)
                return "BOOKSHELF";
            if (classification == Classification::PICTURE)
                return "PICTURE";
            if (classification == Classification::COUNTER)
                return "COUNTER";
            if (classification == Classification::DESK)
                return "DESK";
            if (classification == Classification::CURTAIN)
                return "CURTAIN";
            if (classification == Classification::REFRIDGERATOR)
                return "REFRIDGERATOR";
            if (classification == Classification::SHOWER_CURTAIN)
                return "SHOWER_CURTAIN";
            if (classification == Classification::TOILET)
                return "TOILET";
            if (classification == Classification::SINK)
                return "SINK";
            if (classification == Classification::BATHTUB)
                return "BATHTUB";
            if (classification == Classification::OTHER_FURNITURE)
                return "OTHER_FURNITURE";

            // Unknown selection
            assert(false);
            return "UNKNOWN";
        }

        //! Returns the classification from the given string representation
        static inline Classification classificationFromString(const std::string& classification)
        {
            if (classification == "UNKNOWN")
                return Classification::UNKNOWN;
            if (classification == "WALL")
                return Classification::WALL;
            if (classification == "FLOOR")
                return Classification::FLOOR;
            if (classification == "CABINET")
                return Classification::CABINET;
            if (classification == "BED")
                return Classification::BED;
            if (classification == "CHAIR")
                return Classification::CHAIR;
            if (classification == "SOFA")
                return Classification::SOFA;
            if (classification == "TABLE")
                return Classification::TABLE;
            if (classification == "DOOR")
                return Classification::DOOR;
            if (classification == "WINDOW")
                return Classification::WINDOW;
            if (classification == "BOOKSHELF")
                return Classification::BOOKSHELF;
            if (classification == "PICTURE")
                return Classification::PICTURE;
            if (classification == "COUNTER")
                return Classification::COUNTER;
            if (classification == "DESK")
                return Classification::DESK;
            if (classification == "CURTAIN")
                return Classification::CURTAIN;
            if (classification == "REFRIDGERATOR")
                return Classification::REFRIDGERATOR;
            if (classification == "SHOWER_CURTAIN")
                return Classification::SHOWER_CURTAIN;
            if (classification == "TOILET")
                return Classification::TOILET;
            if (classification == "SINK")
                return Classification::SINK;
            if (classification == "BATHTUB")
                return Classification::BATHTUB;
            if (classification == "OTHER_FURNITURE")
                return Classification::OTHER_FURNITURE;

            // Unknown selection
            assert(false);
            return Classification::UNKNOWN;
        }
    };
} // namespace vision
} // namespace am2b
