/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "BinaryFile.hpp"
#include "TOMLFile.hpp"

namespace am2b {
namespace vision {
    //! Data container for the data sent between the control system and the vision system
    class InterfaceContainer : public BinaryFile, public TOMLFile {
        // Construction
        // ------------
    public:
        //! Default constructor
        InterfaceContainer() = default;
    };
} // namespace vision
} // namespace am2b
