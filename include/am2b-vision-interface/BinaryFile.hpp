/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/compression.hpp"
#include "broccoli/io/filesystem.hpp"

// Internal libraries
#include "BinaryData.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of a data object which can be written as a binary file
    class BinaryFile : public BinaryData {
        // Construction
        // ------------
    public:
        //! Default constructor
        BinaryFile() = default;

        //! Virtual destructor
        virtual ~BinaryFile() {}

        // Input/Output (binary)
        // ------------
    public:
        // Specifies the compression options for the payload (see base class for details)
        virtual broccoli::io::compression::CompressionOptions payloadCompressionOptions() const { return broccoli::io::compression::CompressionOptions::raw(); }

        //! Writes data of this container into a binary file
        /*!
         * \param [in] filePath Path to the output file
         * \param [in] compression Specifies compression options
         * \param [in] endianness The endianness of the binary representation
         * \return `true` on success, `false` otherwise
         */
        inline bool writeToBinaryFile(const std::string& filePath, const broccoli::io::compression::CompressionOptions& compression, const broccoli::io::serialization::Endianness& endianness = broccoli::io::serialization::Endianness::LITTLE) const
        {
            broccoli::io::serialization::BinaryStream stream;
            serialize(stream, endianness);
            return broccoli::io::filesystem::writeFile(filePath, stream, compression);
        }

        //! Writes data of this container into a binary file (automatically detects compression options)
        /*!
         * \param [in] filePath Path to the output file
         * \param [in] endianness The endianness of the binary representation
         * \return `true` on success, `false` otherwise
        */
        inline bool writeToBinaryFile(const std::string& filePath, const broccoli::io::serialization::Endianness& endianness = broccoli::io::serialization::Endianness::LITTLE) const { return writeToBinaryFile(filePath, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), endianness); }

        //! Reads data of this container from a binary file
        /*!
         * \param [in] filePath Path to the input file
         * \param [in] compression Specifies compression options
         * \param [in] endianness The endianness of the binary representation
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromBinaryFile(const std::string& filePath, const broccoli::io::compression::DecompressionOptions& compression, const broccoli::io::serialization::Endianness& endianness = broccoli::io::serialization::Endianness::LITTLE)
        {
            broccoli::io::serialization::BinaryStream stream;
            if (broccoli::io::filesystem::readFile(filePath, stream, compression) == false)
                return false;
            return (deSerialize(stream, 0, endianness) == stream.size());
        }

        //! Reads data of this container from a binary file (automatically detects compression options)
        /*!
         * \param [in] filePath Path to the input file
         * \param [in] endianness The endianness of the binary representation
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromBinaryFile(const std::string& filePath, const broccoli::io::serialization::Endianness& endianness = broccoli::io::serialization::Endianness::LITTLE) { return readFromBinaryFile(filePath, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), endianness); }
    };
} // namespace vision
} // namespace am2b
