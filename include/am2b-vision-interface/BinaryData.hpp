/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/serialization/serialization.hpp"

// Internal libraries
// ...

namespace am2b {
namespace vision {
    //! Abstraction of data objects which can be represented in binary format
    class BinaryData : public broccoli::io::serialization::SerializableData {
        // Construction
        // ------------
    public:
        //! Default constructor
        BinaryData() = default;

        //! Virtual destructor
        virtual ~BinaryData() {}

        // Input/Output (binary)
        // ------------
    public:
        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        virtual broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const = 0;

        // Helpers
        // -------
    public:
        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        static inline broccoli::io::serialization::BinaryStreamSize serializeHeaderPhantom() { return sizeof(broccoli::io::serialization::BinaryStreamSize); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename T, typename std::enable_if<std::is_integral<T>::value || std::is_floating_point<T>::value, int>::type = 0>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const T&) { return sizeof(T); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const broccoli::core::Time&) { return sizeof(broccoli::core::Time::m_seconds) + sizeof(broccoli::core::Time::m_nanoSeconds); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const std::string& data) { return sizeof(broccoli::io::serialization::BinaryStreamSize) + data.size(); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename Derived>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const Eigen::MatrixBase<Derived>& data) { return 2 * sizeof(broccoli::io::serialization::BinaryStreamSize) + data.rows() * data.cols() * sizeof(typename Eigen::MatrixBase<Derived>::Scalar); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename Derived>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const Eigen::QuaternionBase<Derived>&) { return 4 * sizeof(typename Eigen::QuaternionBase<Derived>::Scalar); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const BinaryData& data) { return serializeHeaderPhantom() + data.estimatedMaximumBinaryStreamSize(); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename T, size_t N>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const std::array<T, N>& data)
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = 0;
            if (N > 2)
                returnValue += N * serializePhantom(data[0]);
            else {
                for (size_t i = 0; i < N; i++)
                    returnValue += serializePhantom(data[i]);
            }
            return returnValue;
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename VectorLikeType>
        static inline broccoli::io::serialization::BinaryStreamSize serializeVectorLikePhantom(const VectorLikeType& data)
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = sizeof(broccoli::io::serialization::BinaryStreamSize);
            if (data.size() > 2)
                returnValue += data.size() * serializePhantom(data[0]);
            else {
                for (size_t i = 0; i < data.size(); i++)
                    returnValue += serializePhantom(data[i]);
            }
            return returnValue;
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename T, typename Allocator = std::allocator<T>>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const std::vector<T, Allocator>& data) { return serializeVectorLikePhantom<std::vector<T, Allocator>>(data); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename T, size_t MaximumStaticElementCount = 0, typename Allocator = std::allocator<T>>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const broccoli::memory::SmartVector<T, MaximumStaticElementCount, Allocator>& data) { return serializeVectorLikePhantom<broccoli::memory::SmartVector<T, MaximumStaticElementCount, Allocator>>(data); }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename T, unsigned int N, class Allocator = std::allocator<T>>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const broccoli::memory::MultiVector<T, N, Allocator>& data)
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = serializePhantom(data.size());
            const size_t elementCount = data.elementCount();
            if (elementCount > 2)
                returnValue += elementCount * serializePhantom(data[0]);
            else {
                for (size_t i = 0; i < elementCount; i++)
                    returnValue += serializePhantom(data[i]);
            }
            return returnValue;
        }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <unsigned int N, unsigned int L, class T, class Allocator = std::allocator<T>>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const broccoli::memory::MultiLevelGrid<N, L, T, Allocator>& data)
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = 0;
            for (unsigned int l = 0; l < L; l++) {
                const size_t cellCount = data.cellCount(l);
                if (l == 0)
                    returnValue += serializePhantom(data.size(l));
                if (cellCount > 2)
                    returnValue += cellCount * serializePhantom(data.cell(l, 0));
                else {
                    for (size_t i = 0; i < cellCount; i++)
                        returnValue += serializePhantom(data.cell(l, i));
                }
            }
            return returnValue;
        }

        //! Dummy method to terminate recursive call of corresponding variadic template
        /*! \return 0 */
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom() { return 0; }

        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        template <typename firstType, typename secondType, typename... remainingTypes>
        static inline broccoli::io::serialization::BinaryStreamSize serializePhantom(const firstType& firstValue, const secondType& secondValue, const remainingTypes&... remainingValues)
        {
            return serializePhantom(firstValue) + serializePhantom(secondValue) + serializePhantom(remainingValues...);
        }
    };
} // namespace vision
} // namespace am2b
