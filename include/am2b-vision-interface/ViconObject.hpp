/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "Eigen/Dense"

// Internal libraries
#include "BinaryData.hpp"
#include "TOMLData.hpp"

namespace am2b {
namespace vision {
    //! Data container for a single tracked Vicon object
    class ViconObject : public BinaryData, public TOMLData {
        // Construction
        // ------------
    public:
        //! Constructor
        ViconObject() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ViconObject& reference) const
        {
            return m_name == reference.m_name && //
                m_occluded == reference.m_occluded && //
                m_position == reference.m_position && //
                m_orientation.coeffs() == reference.m_orientation.coeffs();
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ViconObject& reference) const { return !operator==(reference); }

        // Members
        // -------
    public:
        std::string m_name = "UNKNOWN"; //!< Name of the object as specified in the Vicon Tracker software
        bool m_occluded = false; //!< Is the object occluded (not visible) to the cameras?
        Eigen::Vector3d m_position = Eigen::Vector3d::Zero(); //!< Position [m] of object relative to vicon-world frame measured in vicon-world frame
        Eigen::Quaterniond m_orientation = Eigen::Quaterniond::Identity(); //!< Orientation of object relative to vicon-world frame

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_name, m_occluded, m_position, m_orientation);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_name, m_occluded, m_position, m_orientation);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_name, m_occluded, m_position, m_orientation);
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + context.encodeValuePhantom("Name", m_name, "Occluded", m_occluded, "PositionXYZ", m_position, "OrientationWXYZ", m_orientation);
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            context.encodeSectionHeader();
            context.encodeValue("Name", m_name, "Occluded", m_occluded, "PositionXYZ", m_position, "OrientationWXYZ", m_orientation);
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            return context.decodeValue("Name", m_name, "Occluded", m_occluded, "PositionXYZ", m_position, "OrientationWXYZ", m_orientation);
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
