/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/ply/PLYFile.hpp"
#include <Eigen/Dense>
#include <Eigen/StdVector>

// Internal libraries
#include "InterfaceContainer.hpp"
#include "ObjectUpdateFull.hpp"
#include "Signal.hpp"
#include "TerrainPatchUpdate.hpp"

namespace am2b {
namespace vision {
    //! Data container for the data sent from the vision system to the control system
    /*!
     * Notes on provided pose data
     * ---------------------------
     * It is essential that both provided vision-TCP poses are linked to the same point in time since the transformation between
     * planning-world frame and vision-world frame will be computed from these poses. Both poses describe the same frame at the same
     * time, however, observed from different coordinate systems.
     */
    class VisionToControlContainer : public InterfaceContainer {
        // Construction
        // ------------
    public:
        //! Default constructor
        VisionToControlContainer() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const VisionToControlContainer& reference) const
        {
            return //
                // Synchronous data
                // ----------------
                // Timing
                m_timeDataAcquisition == reference.m_timeDataAcquisition && //
                m_timeSending == reference.m_timeSending && //
                m_timeReceiving == reference.m_timeReceiving && //
                // Pose of vision-TCP frame (in planning-world frame)
                m_PW_r_visionTCP == reference.m_PW_r_visionTCP && //
                m_PW_q_visionTCP.coeffs() == reference.m_PW_q_visionTCP.coeffs() && //
                // Pose of vision-TCP frame (in vision-world frame)
                m_VW_r_visionTCP == reference.m_VW_r_visionTCP && //
                m_VW_q_visionTCP.coeffs() == reference.m_VW_q_visionTCP.coeffs() && //
                // Asynchronous data
                // -----------------
                // High-level signals
                m_signals == reference.m_signals && //
                // Terrain
                m_terrainPatchesToReset == reference.m_terrainPatchesToReset && //
                m_terrainPatchesToUpdate == reference.m_terrainPatchesToUpdate && //
                // Objects
                m_objectsToRemove == reference.m_objectsToRemove && //
                m_objectsToUpdatePartial == reference.m_objectsToUpdatePartial && //
                m_objectsToUpdateFull == reference.m_objectsToUpdateFull;
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const VisionToControlContainer& reference) const { return !operator==(reference); }

        // Members (SYNCHRONOUS)
        // -------
    public:
        /* Timing
         * ------
         * Both systems (control and vision) have to use a common time basis which is called "master clock" in the following. The master
         * clock is linked to the synchronized real-time communication bus of the control system. The control system cyclically sends the
         * "current" time according to the master clock to the vision system. The vision system should use this information to synchronize
         * an internal clock used for communication with the control system.
         */
        double m_timeDataAcquisition = 0; //!< Time [s] of data aquisition (reading sensor input) according to the master clock
        double m_timeSending = 0; //!< Time [s] of sending this message according to the master clock (to be set by the sender)
        double m_timeReceiving = 0; //!< Time [s] of receiving this message according to the master clock (to be set by the receiver)

        /* Pose of vision-TCP frame (in planning-world frame)
         * ------------------------
         * This is the pose of the vision-TCP frame as observed by the planning system, thus, measured in the planning-world frame.
         * The pose originates from odometry and torso-IMU data. Note that there is a constant transformation between the vision-TCP
         * frame and the optical center of the camera system.
         */
        Eigen::Vector3d m_PW_r_visionTCP = Eigen::Vector3d::Zero(); //!< Position of vision-TCP frame relative to planning-world frame measured in planning-world frame [m]
        Eigen::Quaterniond m_PW_q_visionTCP = Eigen::Quaterniond::Identity(); //!< Orientation of vision-TCP frame relative to planning-world frame

        /* Pose of vision-TCP frame (in vision-world frame)
         * ------------------------
         * This is the pose of the vision-TCP frame as observed by the vision system, thus, measured in the vision-world frame.
         * The pose originates from SLAM data. Note that there is a constant transformation between the vision-TCP frame and
         * the optical center of the camera system.
         */
        Eigen::Vector3d m_VW_r_visionTCP = Eigen::Vector3d::Zero(); //!< Position of vision-TCP frame relative to vision-world frame measured in vision-world frame [m]
        Eigen::Quaterniond m_VW_q_visionTCP = Eigen::Quaterniond::Identity(); //!< Orientation of vision-TCP frame relative to vision-world frame

        // Members (ASYNCHRONOUS)
        // -------
    public:
        // High-level signals
        std::vector<Signal> m_signals; //!< List of high-level signals to the control system (first element = oldest, last element = newest)

        /* Terrain
         * -------
         * See TerrainSpecification for details.
         */
        std::vector<TerrainSpecification::PatchLocation> m_terrainPatchesToReset; //!< List of terrain patches to be resetted (identified through location)
        std::vector<TerrainPatchUpdate> m_terrainPatchesToUpdate; //!< List of terrain patches to be updated

        /* Objects
         * -------
         * See ObjectSpecification for details.
         */
        std::vector<ObjectSpecification::ID> m_objectsToRemove; //!< List of objects to be removed from the scene
        std::vector<ObjectUpdatePartial, Eigen::aligned_allocator<ObjectUpdatePartial>> m_objectsToUpdatePartial; //!< List of objects to be updated (partial - object haves to exist already) in the scene
        std::vector<ObjectUpdateFull, Eigen::aligned_allocator<ObjectUpdateFull>> m_objectsToUpdateFull; //!< List of objects to be updated (full - objects may be new) in the scene

        // Serialization
        // -------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                       m_PW_r_visionTCP, m_PW_q_visionTCP, //
                       m_VW_r_visionTCP, m_VW_q_visionTCP, //
                       m_signals, //
                       m_terrainPatchesToReset, m_terrainPatchesToUpdate, //
                       m_objectsToRemove, m_objectsToUpdatePartial, m_objectsToUpdateFull);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            stream.reserve(stream.size() + (3 * estimatedMaximumBinaryStreamSize()) / 2 /* <-- + 50% */);
            return broccoli::io::serialization::serialize(stream, endianness, //
                m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                m_PW_r_visionTCP, m_PW_q_visionTCP, //
                m_VW_r_visionTCP, m_VW_q_visionTCP, //
                m_signals, //
                m_terrainPatchesToReset, m_terrainPatchesToUpdate, //
                m_objectsToRemove, m_objectsToUpdatePartial, m_objectsToUpdateFull);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, //
                m_timeDataAcquisition, m_timeSending, m_timeReceiving, //
                m_PW_r_visionTCP, m_PW_q_visionTCP, //
                m_VW_r_visionTCP, m_VW_q_visionTCP, //
                m_signals, //
                m_terrainPatchesToReset, m_terrainPatchesToUpdate, //
                m_objectsToRemove, m_objectsToUpdatePartial, m_objectsToUpdateFull);
        }

        // Input/Output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            broccoli::io::encoding::CharacterStreamSize returnValue = 0;

            // Header
            returnValue += context.encodePlainPhantom(defaultTOMLFileHeader());

            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLEncodingContext contextTiming(context, "Timing");
            returnValue += contextTiming.encodeSectionHeaderPhantom();
            returnValue += contextTiming.encodeValuePhantom("TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving);

            // Pose of vision-TCP frame (in planning-world frame)
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            returnValue += contextVisionTCPInPlanningWorld.encodeSectionHeaderPhantom();
            returnValue += contextVisionTCPInPlanningWorld.encodeValuePhantom("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP);

            // Pose of vision-TCP frame (in vision-world frame)
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextVisionTCPInVisionWorld(context, "VisionTCPInVisionWorld");
            returnValue += contextVisionTCPInVisionWorld.encodeSectionHeaderPhantom();
            returnValue += contextVisionTCPInVisionWorld.encodeValuePhantom("PositionXYZ", m_VW_r_visionTCP, "OrientationWXYZ", m_VW_q_visionTCP);

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            returnValue += context.encodeLineBreakPhantom();
            returnValue += estimatedSortedListMaximumTOMLStreamSize(context, "Signals", m_signals);

            // Terrain
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextTerrain(context, "Terrain");
            returnValue += contextTerrain.encodeSectionHeaderPhantom();
            returnValue += contextTerrain.encodeValuePhantom("TerrainPatchesToReset", m_terrainPatchesToReset);
            returnValue += estimatedSortedListMaximumTOMLStreamSize(contextTerrain, "TerrainPatchesToUpdate", m_terrainPatchesToUpdate);

            // Objects
            returnValue += context.encodeLineBreakPhantom();
            const TOMLEncodingContext contextObjects(context, "Objects");
            returnValue += contextObjects.encodeSectionHeaderPhantom();
            returnValue += contextObjects.encodeValuePhantom("ObjectsToRemove", m_objectsToRemove);
            returnValue += estimatedSortedListMaximumTOMLStreamSize(contextObjects, "ObjectsToUpdatePartial", m_objectsToUpdatePartial);
            returnValue += contextObjects.encodeLineBreakPhantom();
            returnValue += estimatedSortedListMaximumTOMLStreamSize(contextObjects, "ObjectsToUpdateFull", m_objectsToUpdateFull);

            // Return sum
            return returnValue;
        }

        // Encodes **this** object to the given stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            // Pre-allocate memory
            context.stream().reserve(context.stream().size() + (3 * estimatedMaximumTOMLStreamSize(context)) / 2 /* <-- +50% */);

            // Header
            context.encodePlain(defaultTOMLFileHeader());

            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLEncodingContext contextTiming(context, "Timing");
            contextTiming.encodeSectionHeader();
            contextTiming.encodeValue("TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving);

            // Pose of vision-TCP frame (in planning-world frame)
            context.encodeLineBreak();
            const TOMLEncodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            contextVisionTCPInPlanningWorld.encodeSectionHeader();
            contextVisionTCPInPlanningWorld.encodeValue("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP);

            // Pose of vision-TCP frame (in vision-world frame)
            context.encodeLineBreak();
            const TOMLEncodingContext contextVisionTCPInVisionWorld(context, "VisionTCPInVisionWorld");
            contextVisionTCPInVisionWorld.encodeSectionHeader();
            contextVisionTCPInVisionWorld.encodeValue("PositionXYZ", m_VW_r_visionTCP, "OrientationWXYZ", m_VW_q_visionTCP);

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            context.encodeLineBreak();
            if (encodeSortedListToTOML(context, "Signals", m_signals) == false)
                return false;

            // Terrain
            context.encodeLineBreak();
            const TOMLEncodingContext contextTerrain(context, "Terrain");
            contextTerrain.encodeSectionHeader();
            contextTerrain.encodeValue("TerrainPatchesToReset", m_terrainPatchesToReset);
            if (encodeSortedListToTOML(contextTerrain, "TerrainPatchesToUpdate", m_terrainPatchesToUpdate) == false)
                return false;

            // Objects
            context.encodeLineBreak();
            const TOMLEncodingContext contextObjects(context, "Objects");
            contextObjects.encodeSectionHeader();
            contextObjects.encodeValue("ObjectsToRemove", m_objectsToRemove);
            if (encodeSortedListToTOML(contextObjects, "ObjectsToUpdatePartial", m_objectsToUpdatePartial) == false)
                return false;
            contextObjects.encodeLineBreak();
            if (encodeSortedListToTOML(contextObjects, "ObjectsToUpdateFull", m_objectsToUpdateFull) == false)
                return false;

            // Success
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            // Members (SYNCHRONOUS)
            // -------
            // Timing
            const TOMLDecodingContext contextTiming(context, "Timing");
            if (contextTiming.isValid() == false || contextTiming.decodeValue("TimeDataAcquisition", m_timeDataAcquisition, "TimeSending", m_timeSending, "TimeReceiving", m_timeReceiving) == false)
                return false;

            // Pose of vision-TCP frame (in planning-world frame)
            const TOMLDecodingContext contextVisionTCPInPlanningWorld(context, "VisionTCPInPlanningWorld");
            if (contextVisionTCPInPlanningWorld.isValid() == false || contextVisionTCPInPlanningWorld.decodeValue("PositionXYZ", m_PW_r_visionTCP, "OrientationWXYZ", m_PW_q_visionTCP) == false)
                return false;

            // Pose of vision-TCP frame (in vision-world frame)
            const TOMLDecodingContext contextVisionTCPInVisionWorld(context, "VisionTCPInVisionWorld");
            if (contextVisionTCPInVisionWorld.isValid() == false || contextVisionTCPInVisionWorld.decodeValue("PositionXYZ", m_VW_r_visionTCP, "OrientationWXYZ", m_VW_q_visionTCP) == false)
                return false;

            // Members (ASYNCHRONOUS)
            // -------
            // High-level signals
            if (decodeSortedListFromTOML(context, "Signals", m_signals) == false)
                return false;

            // Terrain
            const TOMLDecodingContext contextTerrain(context, "Terrain");
            if (contextTerrain.isValid() == false || //
                contextTerrain.decodeValue("TerrainPatchesToReset", m_terrainPatchesToReset) == false || //
                decodeSortedListFromTOML(contextTerrain, "TerrainPatchesToUpdate", m_terrainPatchesToUpdate) == false)
                return false;

            // Objects
            const TOMLDecodingContext contextObjects(context, "Objects");
            if (contextObjects.isValid() == false || //
                contextObjects.decodeValue("ObjectsToRemove", m_objectsToRemove) == false || //
                decodeSortedListFromTOML(contextObjects, "ObjectsToUpdatePartial", m_objectsToUpdatePartial) == false || //
                decodeSortedListFromTOML(contextObjects, "ObjectsToUpdateFull", m_objectsToUpdateFull) == false)
                return false;

            // Success
            return true;
        }

        // Special Output
        // --------------
    public:
        //! Generates a mesh combining **all** terrain patches of this container (from terrain patch updates)
        /*!
         * \param [in] cellScaling Scaling of cell quads in x- and y-direction (to allow visual distinction of neighboring cells with same height - use 1.0 for no scaling)
         * \return Mesh representation of all terrain patches combined (described in vision-world frame)
         */
        CGMeshExtended combineTerrainPatchesToMesh(const double& cellScaling = 0.95) const
        {
            // Initialize helpers
            const double halfQuadSizeX = 0.5 * TerrainSpecification::cellDimensionX() * cellScaling;
            const double halfQuadSizeY = 0.5 * TerrainSpecification::cellDimensionY() * cellScaling;
            const std::array<Eigen::Vector3d, 4> vertexShift{ { //
                Eigen::Vector3d(-halfQuadSizeX, -halfQuadSizeY, 0.0), //
                Eigen::Vector3d(halfQuadSizeX, -halfQuadSizeY, 0.0), //
                Eigen::Vector3d(halfQuadSizeX, halfQuadSizeY, 0.0), //
                Eigen::Vector3d(-halfQuadSizeX, halfQuadSizeY, 0.0) } };
            static constexpr double confidenceRange = TerrainSpecification::maximumConfidence() - TerrainSpecification::minimumConfidence();

            // Compute total count of terrain cells (all patches)
            size_t totalCellCount = 0;
            for (size_t i = 0; i < m_terrainPatchesToUpdate.size(); i++)
                totalCellCount += m_terrainPatchesToUpdate[i].m_heightMap.elementCount();

            // Helper struct for terrain quads
            struct TerrainQuad {
            public:
                TerrainQuad(const Eigen::Vector3d& center, const broccoli::geometry::CGMesh::ColorType& color, const CGMeshExtended::Confidence& confidence)
                    : m_center(center)
                    , m_color(color)
                    , m_confidence(confidence)
                {
                }
                Eigen::Vector3d m_center; // Center of this cell
                broccoli::geometry::CGMesh::ColorType m_color; // Color of this cell
                CGMeshExtended::Confidence m_confidence; // Confidence of this cell
            };

            // Process geometry for each terrain patch
            std::vector<TerrainQuad, Eigen::aligned_allocator<TerrainQuad>> quads;
            quads.reserve(totalCellCount);
            for (size_t i = 0; i < m_terrainPatchesToUpdate.size(); i++) {
                const TerrainPatchUpdate& patch = m_terrainPatchesToUpdate[i];
                const Eigen::Vector3d patchOrigin(patch.m_gridLocation[0] * TerrainSpecification::patchDimensionX(), patch.m_gridLocation[1] * TerrainSpecification::patchDimensionY(), 0.0); // Origin of patch in vision-world frame
                const std::array<size_t, 2>& rasterSize = patch.m_heightMap.size();
                for (size_t x = 0; x < rasterSize[0]; x++) {
                    for (size_t y = 0; y < rasterSize[1]; y++) {
                        const TerrainSpecification::Confidence& confidence = patch.m_confidenceMap(x, y);
                        if (confidence > std::numeric_limits<TerrainSpecification::Confidence>::lowest()) // Only draw observed cells
                            quads.emplace_back(patchOrigin + Eigen::Vector3d(((double)x + 0.5) * TerrainSpecification::cellDimensionX(), ((double)y + 0.5) * TerrainSpecification::cellDimensionY(), TerrainSpecification::heightFromValue(patch.m_heightMap(x, y))), CGMeshExtended::interpolateColor((double)confidence / confidenceRange), confidence);
                    }
                }
            }

            // Compute vertex and triangle count
            const size_t triangleCount = 2 * quads.size(); // Two triangles for each quad
            const size_t vertexCount = 4 * quads.size(); // Four vertices for each quad

            // Setup mesh and pre-allocate memory
            CGMeshExtended mesh;
            mesh.m_vertexBuffer.resize(Eigen::NoChange, vertexCount);
            mesh.m_colorBuffer.resize(Eigen::NoChange, vertexCount);
            mesh.m_confidenceBuffer.resize(Eigen::NoChange, vertexCount);
            mesh.m_indexBuffer.resize(3, triangleCount);

            // Write quads to mesh
            size_t currentVertexIndex = 0;
            size_t currentTriangleIndex = 0;
            for (size_t i = 0; i < quads.size(); i++) {
                mesh.m_indexBuffer.col(currentTriangleIndex++) << currentVertexIndex, currentVertexIndex + 1, currentVertexIndex + 2;
                mesh.m_indexBuffer.col(currentTriangleIndex++) << currentVertexIndex, currentVertexIndex + 2, currentVertexIndex + 3;
                for (size_t v = 0; v < 4; v++) {
                    mesh.m_vertexBuffer.col(currentVertexIndex) = quads[i].m_center + vertexShift[v];
                    mesh.m_colorBuffer.col(currentVertexIndex) = quads[i].m_color;
                    mesh.m_confidenceBuffer[currentVertexIndex] = quads[i].m_confidence;
                    currentVertexIndex++;
                }
            }

            // Pass back mesh
            return mesh;
        }

        //! Generates a mesh combining **all** object surfaces of this container (from full object updates)
        /*!
         * \param [in] convertConfidenceToColor Computes per-vertex colors from the per-vertex confidence values (uses default color gradient)
         * \return Mesh representation of all object surfaces combined (described in vision-world frame)
         */
        CGMeshExtended combineObjectSurfacesToMesh(const bool& convertConfidenceToColor = false) const
        {
            // Pre-process object surfaces (apply per-object position and orientation)
            std::vector<CGMeshExtended, Eigen::aligned_allocator<CGMeshExtended>> globalObjectSurfaces; // "Global" representation of object surfaces ("global" = in vision-world frame)
            globalObjectSurfaces.reserve(m_objectsToUpdateFull.size());
            for (size_t i = 0; i < m_objectsToUpdateFull.size(); i++) {
                globalObjectSurfaces.push_back(m_objectsToUpdateFull[i].m_surfaceModel.m_mesh);
                globalObjectSurfaces.back().rotate(m_objectsToUpdateFull[i].m_orientation);
                globalObjectSurfaces.back().translate(m_objectsToUpdateFull[i].m_position);
            }

            // Merge meshes
            std::vector<CGMeshExtended const*> meshesToMerge;
            meshesToMerge.reserve(globalObjectSurfaces.size());
            for (size_t i = 0; i < globalObjectSurfaces.size(); i++)
                meshesToMerge.push_back(&globalObjectSurfaces[i]);
            CGMeshExtended mesh = CGMeshExtended::merge(meshesToMerge);

            // Convert confidence to color (optional)
            if (convertConfidenceToColor == true)
                mesh.convertConfidenceToColor();

            // Pass back mesh
            return mesh;
        }

        //! Generates a mesh combining **all** object volumes of this container (from full object updates)
        /*!
         * \param [in] stepsPerPi Count of discretizing steps for approximating the angle \f$ \pi \f$ (has to be greater than 1)
         * \return Mesh representation of all object volumes combined (described in vision-world frame)
         */
        CGMeshExtended combineObjectVolumesToMesh(const uint64_t& stepsPerPi = 16) const
        {
            // Pre-process object volumes (compute meshes and apply per-object position and orientation)
            std::vector<CGMeshExtended, Eigen::aligned_allocator<CGMeshExtended>> globalObjectVolumes; // "Global" representation of object volumes ("global" = in vision-world frame)
            globalObjectVolumes.reserve(m_objectsToUpdateFull.size());
            for (size_t i = 0; i < m_objectsToUpdateFull.size(); i++) {
                globalObjectVolumes.push_back(m_objectsToUpdateFull[i].m_volumeModel.m_ssvSegment.createMesh(stepsPerPi, false)); // Create local object to be sure to take global position and orientation from object (overwrites position and orientation of segment)
                globalObjectVolumes.back().rotate(m_objectsToUpdateFull[i].m_orientation);
                globalObjectVolumes.back().translate(m_objectsToUpdateFull[i].m_position);
            }

            // Merge meshes
            std::vector<CGMeshExtended const*> meshesToMerge;
            meshesToMerge.reserve(globalObjectVolumes.size());
            for (size_t i = 0; i < globalObjectVolumes.size(); i++)
                meshesToMerge.push_back(&globalObjectVolumes[i]);
            return CGMeshExtended::merge(meshesToMerge);
        }

        //! Generates a mesh combining **all** geometric elements of this container
        /*! \return Mesh representation of all geometric elements combined (relative to vision-world frame) */
        CGMeshExtended combineAllToMesh() const
        {
            const CGMeshExtended terrainMesh = combineTerrainPatchesToMesh();
            const CGMeshExtended objectSurfacesMesh = combineObjectSurfacesToMesh();
            const CGMeshExtended objectVolumesMesh = combineObjectVolumesToMesh();
            return CGMeshExtended::merge({ &terrainMesh, &objectSurfacesMesh, &objectVolumesMesh });
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
