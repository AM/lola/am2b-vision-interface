/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLInputFile.hpp"
#include "TOMLOutputFile.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of a data object which can be read and written to/from a TOML file
    class TOMLFile : public TOMLInputFile, public TOMLOutputFile {
    };
} // namespace vision
} // namespace am2b
