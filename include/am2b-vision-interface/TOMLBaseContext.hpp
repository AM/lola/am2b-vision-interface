/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <assert.h>
#include <memory>
#include <string>
#include <vector>

// External libraries
// ...

// Internal libraries
// ...

namespace am2b {
namespace vision {
    //! Base context for encoding/decoding to/from TOML
    class TOMLBaseContext {
        // Construction
        // ------------
    public:
        //! Default constructor (for root context)
        TOMLBaseContext()
            : m_errors(std::make_shared<std::vector<std::string>>())
        {
            m_errors->reserve(20); // Assuming a maximum count of 10 levels
        }

        //! Specialized constructor (for child context)
        /*!
         * \param [in] parent The parent context
         * \param [in] sectionNameLocal Initializes \ref sectionNameLocal() - \copybrief sectionNameLocal()
         */
        TOMLBaseContext(const TOMLBaseContext& parent, const std::string& sectionNameLocal)
            : m_sectionNameLocal(sectionNameLocal)
            , m_sectionNameGlobal((parent.m_sectionNameGlobal.size() == 0) ? sectionNameLocal : parent.m_sectionNameGlobal + "." + sectionNameLocal)
            , m_errors(parent.m_errors)
        {
        }

        // Members
        // -------
    protected:
        std::string m_sectionNameLocal = ""; //!< \copybrief sectionNameLocal()
        std::string m_sectionNameGlobal = ""; //!< \copybrief sectionNameGlobal()
        std::shared_ptr<std::vector<std::string>> m_errors; //!< \copybrief errors()

        // Getters
        // -------
    public:
        //! **Local** name of current section (without parent levels)
        inline const std::string& sectionNameLocal() const { return m_sectionNameLocal; }

        //! **Global** name of current section (with parent levels)
        inline const std::string& sectionNameGlobal() const { return m_sectionNameGlobal; }

        //! List of error messages (first element is lowest level error, last element is highest level error)
        inline std::vector<std::string>& errors() const { return *m_errors; }

        //! Returns a string consisting of the *stacked* error list (highest level first)
        std::string errorsStacked() const
        {
            // Initialize helpers
            std::string returnValue = "";

            // Skip, if there are no errors at all
            if (m_errors->size() == 0)
                return returnValue;

            // Pre-allocate memory
            size_t totalLength = 0;
            for (size_t i = 0; i < m_errors->size(); i++)
                totalLength += (*m_errors)[i].length();
            totalLength += m_errors->size() - 1; // Whitespaces between errors
            returnValue.reserve(totalLength);

            // Stack error messages
            for (size_t i = 0; i < m_errors->size(); i++) {
                if (i > 0)
                    returnValue += " ";
                returnValue += (*m_errors)[m_errors->size() - 1 - i];
            }

            // Pass back stacked list
            return returnValue;
        }

        // Error handling
        // --------------
    public:
        //! Helper to handle TOML I/O errors
        /*!
         * \param [in] errorMessage A message to explain the current error
         * \return `false` (always!)
         */
        inline bool handleError(const std::string& errorMessage) const
        {
            m_errors->push_back(errorMessage);
            if (m_sectionNameGlobal.size() > 0)
                m_errors->emplace_back("Error in section '[" + m_sectionNameGlobal + "]'!");
            return false;
        }
    };
} // namespace vision
} // namespace am2b
