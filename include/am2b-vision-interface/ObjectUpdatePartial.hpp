/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "BinaryData.hpp"
#include "ObjectSpecification.hpp"
#include "TOMLData.hpp"

namespace am2b {
namespace vision {
    //! Data container for a partial update of an object
    class ObjectUpdatePartial : public BinaryData, public TOMLData {
        // Type definitions
        // ----------------
    public:
        using ID = ObjectSpecification::ID;
        using ClassificationType = ObjectSpecification::ClassificationType;
        using Classification = ObjectSpecification::Classification;
        using Position = ObjectSpecification::Position;
        using Orientation = ObjectSpecification::Orientation;

        // Construction
        // ------------
    public:
        //! Constructor
        ObjectUpdatePartial() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator **equality**
        inline bool operator==(const ObjectUpdatePartial& reference) const
        {
            return m_id == reference.m_id && //
                m_classification == reference.m_classification && //
                m_position == reference.m_position && //
                m_orientation.coeffs() == reference.m_orientation.coeffs();
        }

        //! Comparison operator **inequality**
        inline bool operator!=(const ObjectUpdatePartial& reference) const { return !operator==(reference); }

        // Members
        // -------
    public:
        ID m_id = 0; //!< \copybrief ObjectSpecification::ID
        Classification m_classification = Classification::UNKNOWN; //!< \copybrief ObjectSpecification::Classification
        Position m_position = Position::Zero(); //!< \copybrief ObjectSpecification::Position
        Orientation m_orientation = Orientation::Identity(); //!< \copybrief ObjectSpecification::Orientation

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + serializePhantom(m_id, static_cast<ClassificationType>(m_classification), m_position, m_orientation);
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_id, static_cast<ClassificationType>(m_classification), m_position, m_orientation);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            ClassificationType classification = 0;
            const auto returnValue = broccoli::io::serialization::deSerialize(stream, index, endianness, m_id, classification, m_position, m_orientation);
            if (classification >= static_cast<ClassificationType>(Classification::CLASSIFICATION_COUNT)) {
                assert(false);
                return 0;
            }
            m_classification = static_cast<Classification>(classification);
            return returnValue;
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + context.encodeValuePhantom("ID", m_id, "Classification", ObjectSpecification::toString(m_classification), "PositionXYZ", m_position, "OrientationWXYZ", m_orientation);
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            context.encodeSectionHeader();
            context.encodeValue("ID", m_id, "Classification", ObjectSpecification::toString(m_classification), "PositionXYZ", m_position, "OrientationWXYZ", m_orientation);
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            std::string classificationAsString = "";
            if (context.decodeValue("ID", m_id, "Classification", classificationAsString, "PositionXYZ", m_position, "OrientationWXYZ", m_orientation) == false)
                return false;
            m_classification = ObjectSpecification::classificationFromString(classificationAsString);
            if (m_classification == Classification::UNKNOWN)
                return context.handleError("Unknown classification '" + classificationAsString + "'!");
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
