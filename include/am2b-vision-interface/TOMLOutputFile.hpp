/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "TOMLOutputData.hpp"

namespace am2b {
namespace vision {
    //! Abstraction of a data object which can be written to a TOML file
    class TOMLOutputFile : public TOMLOutputData {
    public:
        //! Default header for TOML files
        static inline const std::string& defaultTOMLFileHeader()
        {
            static const std::string header = "# This file is part of am2b.\n# Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich\n# https://www.mw.tum.de/am\n\n";
            return header;
        }

        //! Writes data of this container into a TOML file
        /*!
         * \param [in] filePath Path to the output file
         * \param [in] compression Specifies compression options
         * \param [in] context TOML encoding context
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool writeToTOMLFile(const std::string& filePath, const broccoli::io::compression::CompressionOptions& compression, const TOMLEncodingContext& context, std::string* const error = nullptr) const
        {
            // Encode
            if (encodeToTOML(context) == false) {
                if (error != nullptr)
                    *error = "Failed to write TOML file '" + filePath + "'! Encoding failed! " + context.errorsStacked();
                return false;
            }

            // Write file
            if (broccoli::io::filesystem::writeFile(filePath, context.stream(), compression) == false) {
                if (error != nullptr)
                    *error = "Failed to write TOML file '" + filePath + "'! File IO error!";
                return false;
            }

            // Success
            return true;
        }

        //! Writes data of this container into a TOML file (automatically detects compression options)
        /*!
         * \param [in] filePath Path to the output file
         * \param [in] context TOML encoding context
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
        */
        inline bool writeToTOMLFile(const std::string& filePath, const TOMLEncodingContext& context, std::string* const error = nullptr) const { return writeToTOMLFile(filePath, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), context, error); }
    };
} // namespace vision
} // namespace am2b
