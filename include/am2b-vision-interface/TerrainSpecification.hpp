/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <array>
#include <cmath>
#include <limits>
#include <stdint.h>

// External libraries
#include "broccoli/memory/DefaultInitializationAllocator.hpp"
#include "broccoli/memory/MultiVector.hpp"

// Internal libraries
// ...

namespace am2b {
namespace vision {
    //! Specification of terrain as part of the environment model
    /*!
     * The terrain is aligned with the vision-world frame which means that the xy-plane is parallel to the "ground" and the z-axis represents the vertical axis.
     * Moreover, the center of the grid is the origin of the vision-world frame (= bottom-left corner of patch at x=0, y=0 in sketch below).
     *
     * The terrain is defined as a grid of terrain patches. The idea of splitting the terrain into patches is to limit the amount of data transmitted between
     * the vision and control system and to allow parallel processing of patches. Each patch has an x- and y-index which specifies its location in the terrain
     * grid. These indices are linked to the origin of the vision-world frame (see sketch below). Note that this means that the indices can be positive AND
     * negative integers.
     *
     *             Y (vision-world frame)
     *             ^
     *     |   |   |   |   |   |
     *   --x---x---x---x---x---x--
     *  2  | P | P | P | P | P |
     *   --x---x---x---x---x---x--
     *  1  | P | P | P | P | P |
     *   --x---x---x---x---x---x--
     *  0  | P | P | P | P | P |
     *   --x---x---O---x---x---x--> X (vision-world frame)
     * -1  | P | P | P | P | P |
     *   --x---x---x---x---x---x--
     * -2  | P | P | P | P | P |
     *   --x---x---x---x---x---x--
     *     |   |   |   |   |   |
     *      -2  -1   0   1   2   <-- x-index
     *  ^
     *  |_ y-index
     *
     * A patch represents a matrix of cells, where each cell finally contains the terrain data (height, confidence) at the CENTER of the cell. For communication
     * between the vision and control system, a patch is represented as simple matrix. However, the row- and column-count are a power of two, such that it can
     * be represented and processed as quadtree (internally within the vision / control system).
     *
     * The height- and confidence-map of a patch are represented as two-dimensional arrays (matrix). The first and second index of the array correspond to the x- and
     * y-position of the cell relative to the patch origin, respectively (aligned with vision-world frame). The maps can be interpreted as images, where x points
     * down (rows) and y points to the right (columns).
     *
     *   O---x---x---x---x--> y-index (columns)
     *   | C | C | C | C |
     *   x---x---x---x---x
     *   | C | C | C | C |
     *   x---x---x---x---x
     *   | C | C | C | C |
     *   x---x---x---x---x
     *   |
     *   v x-index (rows)
     */
    class TerrainSpecification {
        // Type definitions
        // ----------------
    public:
        using HeightValue = uint16_t; //!< Datatype for height values (0=lowest value **below** ground, 32767=default "ground" height, 65535=highest value **above** ground)
        using Confidence = uint8_t; //!< Datatype for confidence values (0=unknown, 1=lowest accuracy, 255=highest accuracy)
        using PatchLocationIndex = int8_t; //!< Datatype for an index specifying the terrain patch location in a certain direction of the terrain grid
        using PatchLocation = std::array<PatchLocationIndex, 2>; //!< Datatype for the combined x- and y-indices specifying the terrain patch location in the terrain grid ([0]=x-index, [1]=y-index)
        using PatchHeightMap = broccoli::memory::MultiVector<HeightValue, 2, broccoli::memory::DefaultInitializationAllocator<HeightValue>>; //!< Datatype for map (x, y) of height values for each cell in a patch
        using PatchConfidenceMap = broccoli::memory::MultiVector<Confidence, 2, broccoli::memory::DefaultInitializationAllocator<Confidence>>; //!< Datatype for map (x, y) of confidence values for each cell in a patch

        // Constants
        // ---------
    public:
        // Height specification
        //! The minimum height value (below "ground")
        static constexpr HeightValue minimumHeightValue() { return std::numeric_limits<HeightValue>::lowest(); }
        //! The maximum height value (above "ground")
        static constexpr HeightValue maximumHeightValue() { return std::numeric_limits<HeightValue>::max(); }
        //! The default height value (alias "ground")
        static constexpr HeightValue defaultHeightValue() { return (maximumHeightValue() - minimumHeightValue()) / 2; }
        //! Resolution of height values [m] (step size)
        static constexpr double heightResolution() { return 0.001; }
        //! The minimum height [m]
        static constexpr double minimumHeight() { return -heightResolution() * (defaultHeightValue() - minimumHeightValue()); }
        //! The maximum height [m]
        static constexpr double maximumHeight() { return heightResolution() * (maximumHeightValue() - defaultHeightValue()); }
        //! The default height [m] (alias "ground")
        static constexpr double defaultHeight() { return 0.0; }
        //! Returns the corresponding height [m] from the given height value
        static inline double heightFromValue(const HeightValue& value)
        {
            if (value < defaultHeightValue())
                return -heightResolution() * (defaultHeightValue() - value);
            else
                return heightResolution() * (value - defaultHeightValue());
        }
        //! Returns the corresponding height value from the given height [m]
        static inline HeightValue heightToValue(const double& height)
        {
            int64_t targetValue = std::round(height / heightResolution());
            targetValue += defaultHeightValue();
            if (targetValue < (int64_t)minimumHeightValue())
                return minimumHeightValue();
            else if (targetValue > (int64_t)maximumHeightValue())
                return maximumHeightValue();
            else
                return targetValue;
        }
        // Confidence specification
        //! The minimum confidence
        static constexpr Confidence minimumConfidence() { return std::numeric_limits<Confidence>::lowest(); }
        //! The maximum confidence
        static constexpr Confidence maximumConfidence() { return std::numeric_limits<Confidence>::max(); }
        //! The default confidence
        static constexpr Confidence defaultConfidence() { return minimumConfidence(); }
        //! Returns `true`, if the given confidence value is considered as "observed", `false` otherwise
        static inline bool observed(const Confidence& confidence) { return confidence > minimumConfidence(); }
        // Cell specification
        //! Dimension of a single terrain cell in x-direction [m]
        static constexpr double cellDimensionX() { return 0.01; }
        //! Dimension of a single terrain cell in y-direction [m]
        static constexpr double cellDimensionY() { return cellDimensionX(); }
        // Patch specification
        //! Count of levels of a quadtree representing a terrain patch
        static constexpr unsigned int patchQuadtreeLevels() { return 7; }
        //! Count of terrain cells per terrain patch in x-direction
        static constexpr size_t cellsPerPatchX() { return 1 << (patchQuadtreeLevels() - 1); }
        //! Count of terrain cells per terrain patch in y-direction
        static constexpr size_t cellsPerPatchY() { return cellsPerPatchX(); }
        //! Total count of terrain cells per terrain patch (area)
        static constexpr size_t cellsPerPatch() { return cellsPerPatchX() * cellsPerPatchY(); }
        //! Dimension of terrain patch in x-direction [m]
        static constexpr double patchDimensionX() { return cellDimensionX() * cellsPerPatchX(); }
        //! Dimension of terrain patch in y-direction [m]
        static constexpr double patchDimensionY() { return cellDimensionY() * cellsPerPatchY(); }
        //! Returns the size of the terrain patch (count of terrain cells in x- and y-direction)
        static constexpr std::array<size_t, 2> patchSize() { return { { cellsPerPatchX(), cellsPerPatchY() } }; }
        // Grid specification
        //! Returns the minimum possible patch location index in x-direction
        static constexpr PatchLocationIndex patchLocationIndexMinimumX() { return std::numeric_limits<PatchLocationIndex>::lowest(); }
        //! Returns the minimum possible patch location index in y-direction
        static constexpr PatchLocationIndex patchLocationIndexMinimumY() { return patchLocationIndexMinimumX(); }
        //! Returns the maximum possible patch location index in x-direction
        static constexpr PatchLocationIndex patchLocationIndexMaximumX() { return std::numeric_limits<PatchLocationIndex>::max(); }
        //! Returns the maximum possible patch location index in y-direction
        static constexpr PatchLocationIndex patchLocationIndexMaximumY() { return patchLocationIndexMaximumX(); }
        //! Count of terrain patches per terrain grid in x-direction
        static constexpr size_t patchesPerGridX() { return patchLocationIndexMaximumX() - patchLocationIndexMinimumX() + 1; }
        //! Count of terrain patches per terrain grid in y-direction
        static constexpr size_t patchesPerGridY() { return patchLocationIndexMaximumY() - patchLocationIndexMinimumY() + 1; }
        //! Total count of terrain patches per terrain grid (area)
        static constexpr size_t patchesPerGrid() { return patchesPerGridX() * patchesPerGridY(); }
        //! Dimension of terrain grid in x-direction [m]
        static constexpr double gridDimensionX() { return patchDimensionX() * patchesPerGridX(); }
        //! Dimension of terrain grid in y-direction [m]
        static constexpr double gridDimensionY() { return patchDimensionY() * patchesPerGridY(); }
        //! Returns the size of the terrain grid (count of terrain patches in x- and y-direction)
        static constexpr std::array<size_t, 2> gridSize() { return { { patchesPerGridX(), patchesPerGridY() } }; }
    };
} // namespace vision
} // namespace am2b
