/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
// ...

// Internal libraries
#include "BinaryData.hpp"
#include "CGMeshExtended.hpp"
#include "TOMLData.hpp"

namespace am2b {
namespace vision {
    //! Model of the surface of an object as part of the environment model
    /*!
     * The surface is modeled through an *indexed* triangle mesh with following **per-vertex** information:
     *   * position of the vertex (x, y, z)
     *   * normal of the surface (x, y, z - has to be normalized and pointing "outside")
     *   * color of the surface (rgb)
     *   * confidence (estimated error of shape at this point)
     */
    class ObjectSurfaceModel : public BinaryData, public TOMLData {
    public:
        // Construction
        // ------------
    public:
        //! Default constructor
        ObjectSurfaceModel() = default;

        // Operators
        // ---------
    public:
        //! Comparison operator: **equality**
        bool operator==(const ObjectSurfaceModel& reference) const { return m_mesh == reference.m_mesh; }

        //! Comparison operator: **inequality**
        bool operator!=(const ObjectSurfaceModel& reference) const { return !(*this == reference); }

        // Members
        // -------
    public:
        CGMeshExtended m_mesh; //!< Triangle mesh describing the object's surface (in local object coordinate system)

        // Input/Output (binary)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes) (see base class for details)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            return serializeHeaderPhantom() + m_mesh.estimatedMaximumBinaryStreamSize();
        }

        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_mesh);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // Not needed
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_mesh);
        }

        // Input/output (TOML)
        // ------------
    public:
        // Returns an estimate for the maximum occupied space in an uncompressed TOML stream (total count of bytes) (see base class for details)
        broccoli::io::encoding::CharacterStreamSize estimatedMaximumTOMLStreamSize(const TOMLEncodingContext& context) const
        {
            return context.encodeSectionHeaderPhantom() + context.encodeDataStreamPhantom("Mesh", 2048 /* <-- header */ + m_mesh.estimatedMaximumBinaryStreamSize() * 2 /* <-- safety factor */);
        }

        // Encodes **this** object to the stream (see base class for details)
        bool encodeToTOML(const TOMLEncodingContext& context) const
        {
            // Initialize helpers
            using namespace broccoli::geometry;
            using namespace broccoli::io;
            PLYResult plyResult = PLYResult::UNKNOWN;

            // Convert mesh to ply file
            PLYFile plyFile;
            std::string plyFileError = "";
            if (m_mesh.convertToPLY(plyFile, &plyFileError) == false)
                return context.handleError("Could not convert mesh to PLY file: " + plyFileError + "!");

            // Encode ply file
            encoding::CharacterStream plyFileStream;
            plyResult = PLYResult::UNKNOWN;
            if (plyFile.encode(plyFileStream, PLYFormat::Type::BINARY_LITTLE_ENDIAN, &plyResult) == false)
                return context.handleError("Could not encode PLY file: ply-result='" + PLYResultToString(plyResult) + "'!");

            // Encode to TOML
            context.encodeSectionHeader();
            if (context.encodeDataStream("Mesh", plyFileStream, context.sectionNameGlobal() + ".Mesh.ply") == false)
                return false;

            // Success
            return true;
        }

        // Decodes **this** object from the context (see base class for details)
        bool decodeFromTOML(const TOMLDecodingContext& context)
        {
            // Initialize helpers
            using namespace broccoli::geometry;
            using namespace broccoli::io;
            PLYResult plyResult = PLYResult::UNKNOWN;

            // Decode from TOML
            encoding::CharacterStream plyFileStream;
            if (context.decodeDataStream("Mesh", plyFileStream) == false)
                return false;

            // Decode ply file
            PLYFile plyFile;
            plyResult = PLYResult::UNKNOWN;
            if (plyFile.decode(plyFileStream, &plyResult) == false)
                return context.handleError("Could not decode PLY file: ply-result='" + PLYResultToString(plyResult) + "'!");

            // Convert ply file to mesh
            std::string plyFileError = "";
            if (m_mesh.convertFromPLY(plyFile, &plyFileError) == false)
                return context.handleError("Could not convert PLY file to mesh: " + plyFileError + "!");

            // Success
            return true;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };
} // namespace vision
} // namespace am2b
