/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
// ...

// External libraries
#include "broccoli/io/network/NetworkSocket.hpp"

// Internal libraries
// ...

namespace am2b {
namespace vision {
    //! Base class for network sockets connecting the vision and control system
    template <class SendObjectType, class ReceiveObjectType, class OptionType>
    class BaseSocket : public broccoli::io::NetworkSocket<SendObjectType, ReceiveObjectType> {
        // Construction
        // ------------
    public:
        //! Constructor (runs in parent thread)
        BaseSocket(const OptionType& options = OptionType())
            : broccoli::io::NetworkSocket<SendObjectType, ReceiveObjectType>(options)
        {
        }

        //! Virtual destructor
        virtual ~BaseSocket()
        {
        }

        // Protected memory
        // ----------------
    protected:
        // Start protected area of mutex of background thread
        broccoli::core::Time m_localSystemTimeToMasterClockShift; //!< \copybrief localSystemTimeToMasterClockShift()
        // End protected area of mutex of background thread

        // Getters (thread-safe)
        // ---------------------
    protected:
        //! Time shift [s] to apply to local system time to obtain current time relative to master clock \details **Thread-safe getter**
        inline broccoli::core::Time localSystemTimeToMasterClockShift() const { return broccoli::parallel::ThreadSafeContainer::getProtectedData(m_localSystemTimeToMasterClockShift); }

    public:
        //! Current time [s] relative to master clock \details **Thread-safe getter**
        inline double currentTimeInMasterClock() const { return (broccoli::core::Time::currentTime() + localSystemTimeToMasterClockShift()).toDouble(); }

        // Setters (thread-safe)
        // ---------------------
    protected:
        //! **Thread-safe setter for:** \copybrief localSystemTimeToMasterClockShift()
        inline void setLocalSystemTimeToMasterClockShift(const broccoli::core::Time& newValue) { broccoli::parallel::ThreadSafeContainer::setProtectedData(m_localSystemTimeToMasterClockShift, newValue); }

        //! **Thread-safe setter for:** \copybrief currentTimeInMasterClock()
        inline void setCurrentTimeInMasterClock(const double& newValue)
        {
            const broccoli::core::Time currentLocalSystemTime = broccoli::core::Time::currentTime();
            setLocalSystemTimeToMasterClockShift(broccoli::core::Time(newValue) - currentLocalSystemTime);
        }
    };
} // namespace vision
} // namespace am2b
