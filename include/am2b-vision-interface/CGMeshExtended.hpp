/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mec.ed.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <unordered_map>
#include <vector>

// External libraries
#include "broccoli/core/math.hpp"
#include "broccoli/geometry/CGMesh.hpp"
#include "broccoli/geometry/CGMeshTools.hpp"
#include "broccoli/io/ply/PLYFileConverter.hpp"

// Internal libraries
#include "BinaryData.hpp"

namespace am2b {
namespace vision {
    //! Extended version of the `CGMesh` class (features additional confidence buffer and convenience methods for input/output)
    class CGMeshExtended : public broccoli::geometry::CGMesh {
    public:
        // Type definitions
        using CGMesh = broccoli::geometry::CGMesh;
        using CGMeshResult = broccoli::geometry::CGMeshResult;
        using Confidence = uint8_t; //!< Data type for per-vertex confidence values in the confidence buffer (0=lowest accuracy, 255=highest accuracy)

        // Construction
        // ------------
    public:
        //! Constructor (empty mesh)
        CGMeshExtended() = default;

        //! Copy constructor
        CGMeshExtended(const CGMeshExtended& original)
            : CGMesh(original)
            , m_confidenceBuffer(original.m_confidenceBuffer)
        {
        }

        //! Copy constructor (from base class)
        CGMeshExtended(const CGMesh& original)
            : CGMesh(original)
        {
        }

        //! Copy assignment operator
        CGMeshExtended& operator=(const CGMeshExtended& reference)
        {
            if (this == &reference)
                return *this;
            CGMesh::operator=(reference);
            m_confidenceBuffer = reference.m_confidenceBuffer;
            return *this;
        }

        //! Copy assignment operator (from base class)
        CGMeshExtended& operator=(const CGMesh& reference)
        {
            CGMesh::operator=(reference);
            m_confidenceBuffer.resize(Eigen::NoChange, 0);
            return *this;
        }

        //! Virtual destructor
        virtual ~CGMeshExtended()
        {
        }

        // Operators
        // ---------
    public:
        //! Comparison operator: **equality** (allows a certain tolerance for floating point values to counteract rounding errors)
        bool operator==(const CGMeshExtended& reference) const
        {
            static constexpr double tolerance = 1e-6;

            // Compare members
            if (compareMatrixWithTolerance(m_vertexBuffer, reference.m_vertexBuffer, tolerance) == false)
                return false;
            if (compareMatrixWithTolerance(m_normalBuffer, reference.m_normalBuffer, tolerance) == false)
                return false;
            if (m_colorBuffer.rows() != reference.m_colorBuffer.rows() || m_colorBuffer.cols() != reference.m_colorBuffer.cols() || !m_colorBuffer.isApprox(reference.m_colorBuffer))
                return false;
            if (m_materialBuffer != reference.m_materialBuffer)
                return false;
            if (m_indexBuffer.rows() != reference.m_indexBuffer.rows() || m_indexBuffer.cols() != reference.m_indexBuffer.cols() || !m_indexBuffer.isApprox(reference.m_indexBuffer))
                return false;
            if (m_confidenceBuffer.rows() != reference.m_confidenceBuffer.rows() || m_confidenceBuffer.cols() != reference.m_confidenceBuffer.cols() || !m_confidenceBuffer.isApprox(reference.m_confidenceBuffer))
                return false;

            // All members are equal -> equal
            return true;
        }

        //! Comparison operator: **inequality** (allows a certain tolerance for floating point values to counteract rounding errors)
        bool operator!=(const CGMeshExtended& reference) const { return !(*this == reference); }

        // Members
        // -------
    public:
        Eigen::Matrix<Confidence, 1, Eigen::Dynamic> m_confidenceBuffer; //!< Buffer of **per-vertex confidence** (empty if no confidence information is available)

        // Generic
        // -------
    public:
        // Checks, if the mesh is properly defined (see base class for details)
        virtual bool isValid(CGMeshResult* const result = nullptr) const
        {
            // Check, if base class is valid
            if (CGMesh::isValid(result) == false)
                return false;

            // Check, if confidence buffer is of appropriate size (per-vertex confidence values)
            if (m_confidenceBuffer.cols() > 0 && m_confidenceBuffer.cols() != m_vertexBuffer.cols()) {
                // Confidence buffer is available but it does not match the vertex buffer
                if (result != nullptr)
                    *result = CGMeshResult::UNKNOWN;
                return false;
            }

            // Otherwise -> valid
            if (result != nullptr)
                *result = CGMeshResult::SUCCESS;
            return true;
        }

        // Serialization
        // -------------
    public:
        //! Returns an estimate for the maximum occupied space in an uncompressed binary stream (total count of bytes)
        broccoli::io::serialization::BinaryStreamSize estimatedMaximumBinaryStreamSize() const
        {
            broccoli::io::serialization::BinaryStreamSize returnValue = BinaryData::serializeHeaderPhantom();
            returnValue += BinaryData::serializePhantom(m_vertexBuffer, m_normalBuffer, m_colorBuffer);
            returnValue += sizeof(broccoli::io::serialization::BinaryStreamSize);
            if (m_materialBuffer.size() > 0) {
                const broccoli::geometry::CGMaterial& firstMaterial = m_materialBuffer[0];
                returnValue += m_materialBuffer.size() * BinaryData::serializePhantom(firstMaterial.m_name, //
                                   firstMaterial.m_ambientColor, firstMaterial.m_ambientColorFromVertexColor, firstMaterial.m_ambientIntensity, //
                                   firstMaterial.m_diffuseColor, firstMaterial.m_diffuseColorFromVertexColor, firstMaterial.m_diffuseIntensity, //
                                   firstMaterial.m_specularColor, firstMaterial.m_specularColorFromVertexColor, firstMaterial.m_specularIntensity, //
                                   firstMaterial.m_shininess, //
                                   firstMaterial.m_alpha, firstMaterial.m_alphaFromVertexColor, //
                                   firstMaterial.m_shadeless);
            }
            returnValue += BinaryData::serializePhantom(m_indexBuffer, m_confidenceBuffer);
            return returnValue;
        }

    protected:
        // Serialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize serializePayload(broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::Endianness& endianness) const
        {
            return broccoli::io::serialization::serialize(stream, endianness, m_vertexBuffer, m_normalBuffer, m_colorBuffer, m_materialBuffer, m_indexBuffer, m_confidenceBuffer);
        }

        // Deserialization of payload (see base class for details)
        broccoli::io::serialization::BinaryStreamSize deSerializePayload(const broccoli::io::serialization::BinaryStream& stream, const broccoli::io::serialization::BinaryStreamSize& index, const broccoli::io::serialization::BinaryStreamSize& payloadSize, const broccoli::io::serialization::Endianness& endianness)
        {
            (void)payloadSize; // unused
            return broccoli::io::serialization::deSerialize(stream, index, endianness, m_vertexBuffer, m_normalBuffer, m_colorBuffer, m_materialBuffer, m_indexBuffer, m_confidenceBuffer);
        }

        // Conversion to/from PLY file format
        // ----------------------------------
    public:
        //! Converts the mesh to a corresponding PLY data structure
        /*!
         * \param [out] plyFile The ply file as output
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool convertToPLY(broccoli::io::PLYFile& plyFile, std::string* const error = nullptr) const
        {
            // Initialize helpers
            using namespace broccoli::geometry;
            using namespace broccoli::io;
            PLYResult plyResult = PLYResult::UNKNOWN;
            CGMeshResult meshResult = CGMeshResult::UNKNOWN;

            // Convert "base" mesh to PLY structure
            if (PLYFileConverter::convert(*this, plyFile, &plyResult, &meshResult) == false) {
                if (error != nullptr)
                    *error = "ply-result='" + PLYResultToString(plyResult) + "', mesh-result='" + CGMeshResultString(meshResult) + "'";
                assert(false);
                return false;
            }

            // Inject confidence buffer to ply file
            if (m_confidenceBuffer.cols() > 0) {
                auto vertexElementTypeIndex = plyFile.getElementTypeIndexByName("vertex");
                if (vertexElementTypeIndex >= 0) {
                    PLYElementType& vertexElementType = plyFile.m_elementTypes[vertexElementTypeIndex];
                    PLYPropertyType confidenceProperty;
                    confidenceProperty.m_name = "confidence";
                    confidenceProperty.m_valueType = PLYScalar::Type::UCHAR;
                    vertexElementType.m_propertyTypes.push_back(confidenceProperty);
                    for (size_t i = 0; i < vertexElementType.m_elementCount; i++) {
                        PLYPropertyValue newConfidenceValue;
                        newConfidenceValue.setScalar((UCHAR_t)m_confidenceBuffer(0, i));
                        vertexElementType.m_data[i].push_back(newConfidenceValue);
                    }
                }
            }

            // Success
            return true;
        }

        //! Converts the given PLY data structure to the mesh
        /*!
         * \param [in] plyFile The ply file as input
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool convertFromPLY(const broccoli::io::PLYFile& plyFile, std::string* const error = nullptr)
        {
            // Initialize helpers
            using namespace broccoli::geometry;
            using namespace broccoli::io;
            PLYResult plyResult = PLYResult::UNKNOWN;

            // Reset all buffers
            *this = CGMeshExtended();

            // Convert ply file to "base" mesh
            if (PLYFileConverter::convert(plyFile, *this, &plyResult) == false) {
                if (error != nullptr)
                    *error = "ply-result='" + PLYResultToString(plyResult) + "'";
                assert(false);
                return false;
            }

            // Extract confidence buffer from ply file
            m_confidenceBuffer.resize(Eigen::NoChange, 0);
            auto vertexElementTypeIndex = plyFile.getElementTypeIndexByName("vertex");
            if (vertexElementTypeIndex >= 0) {
                const PLYElementType& vertexElementType = plyFile.m_elementTypes[vertexElementTypeIndex];
                auto confidencePropertyTypeIndex = vertexElementType.getPropertyTypeIndexByName("confidence");
                if (confidencePropertyTypeIndex >= 0) {
                    m_confidenceBuffer.resize(Eigen::NoChange, vertexElementType.m_elementCount);
                    for (size_t i = 0; i < vertexElementType.m_elementCount; i++) {
                        if (vertexElementType.m_data[i][confidencePropertyTypeIndex].scalarConverted(vertexElementType.m_propertyTypes[confidencePropertyTypeIndex].m_valueType, m_confidenceBuffer(0, i)) == false) {
                            if (error != nullptr)
                                *error = "Could not decode confidence value from PLY file! Invalid buffer!";
                            assert(false);
                            return false;
                        }
                    }
                }
            }

            // Success
            return true;
        }

        // Input/output to/from PLY file
        // -----------------------------
    public:
        //! Writes the mesh to a `.ply` file (ascii or binary)
        /*!
        * \param [in] filePath Destination file path
        * \param [in] format The format to be used
        * \param [in] compression Specifies compression options
        * \param [out] error Contains error description in case of an error
        * \return `true` on success, `false` otherwise
        */
        inline bool writeToPLYFile(const std::string& filePath, const broccoli::io::PLYFormat::Type& format, const broccoli::io::compression::CompressionOptions& compression, std::string* const error = nullptr) const
        {
            // Convert mesh to ply file
            broccoli::io::PLYFile plyFile;
            if (convertToPLY(plyFile, error) == false) {
                assert(false);
                return false;
            }

            // Write ply file
            broccoli::io::PLYResult plyResult = broccoli::io::PLYResult::UNKNOWN;
            if (plyFile.writeFile(filePath, format, compression, &plyResult) == false) {
                if (error != nullptr)
                    *error = "ply-result='" + broccoli::io::PLYResultToString(plyResult) + "'";
                assert(false);
                return false;
            }

            // Success
            return true;
        }

        //! Writes the mesh to a `.ply` file (ascii or binary) (automatically detects compression options)
        /*!
        * \param [in] filePath Destination file path
        * \param [in] format The format to be used
         * \param [out] error Contains error description in case of an error
        * \return `true` on success, `false` otherwise
        */
        inline bool writeToPLYFile(const std::string& filePath, const broccoli::io::PLYFormat::Type& format = broccoli::io::PLYFormat::Type::BINARY_LITTLE_ENDIAN, std::string* const error = nullptr) const { return writeToPLYFile(filePath, format, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), error); }

        //! Reads and parses the mesh from the given `.ply` file
        /*!
         * \param [in] filePath Path to the `.ply` file (file may be in ascii or binary format)
         * \param [in] compression Specifies compression options
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromPLYFile(const std::string& filePath, const broccoli::io::compression::DecompressionOptions& compression, std::string* const error = nullptr)
        {
            // Read ply file
            broccoli::io::PLYFile plyFile;
            broccoli::io::PLYResult plyResult = broccoli::io::PLYResult::UNKNOWN;
            if (plyFile.readFile(filePath, compression, &plyResult) == false) {
                if (error != nullptr)
                    *error = "ply-result='" + broccoli::io::PLYResultToString(plyResult) + "'";
                assert(false);
                return false;
            }

            // Convert ply file to mesh
            if (convertFromPLY(plyFile, error) == false) {
                assert(false);
                return false;
            }

            // Success
            return true;
        }

        //! Reads and parses the mesh from the given `.ply` file (automatically detects compression options)
        /*!
         * \param [in] filePath Path to the `.ply` file (file may be in ascii or binary format)
         * \param [out] error Contains error description in case of an error
         * \return `true` on success, `false` otherwise
         */
        inline bool readFromPLYFile(const std::string& filePath, std::string* const error = nullptr) { return readFromPLYFile(filePath, broccoli::io::filesystem::filePathOrNameToCompressionOptions(filePath), error); }

        // Tools
        // -----
    public:
        //! Interpolates a color using a given color gradient (linear interpolation)
        /*!
         * \param [in] value Interpolation parameter \f$ \in [0.0,\,1.0] \f$
         * \param [in] colorGradient A list (e.g. `std::array` or `std::vector`) of colors `CGMesh::ColorType` - first color corresponds to a value of 0.0, last color corresponds to a value of 1.0
         * \return Interpolated color
         */
        template <typename ColorGradient>
        static inline CGMesh::ColorType interpolateColor(const double& value, const ColorGradient& colorGradient)
        {
            assert(colorGradient.size() > 1);
            if (value <= 0.0)
                return colorGradient.front();
            else if (value >= 1.0)
                return colorGradient.back();
            else {
                const double segmentLength = 1.0 / (colorGradient.size() - 1);
                const size_t index = value / segmentLength;
                const double x = (value - index * segmentLength) / segmentLength;
                if (x <= 0.0)
                    return colorGradient[index];
                else if (x >= 1.0)
                    return colorGradient[index + 1];
                else {
                    const double one_minus_x = 1.0 - x;
                    CGMesh::ColorType color;
                    for (Eigen::Index i = 0; i < color.rows(); i++)
                        color[i] = one_minus_x * colorGradient[index][i] + x * colorGradient[index + 1][i];
                    return color;
                }
            }
        }

        //! Interpolates a color using the default color gradient (linear interpolation)
        /*!
         * \param [in] value Interpolation parameter \f$ \in [0.0,\,1.0] \f$
         * \return Interpolated color
         */
        static inline CGMesh::ColorType interpolateColor(const double& value) { return interpolateColor(value, defaultColorGradient()); }

        //! Converts the confidence buffer to a corresponding color buffer (interpolates color for each vertex) (linear interpolation)
        /*! \param [in] colorGradient A list (e.g. `std::array` or `std::vector`) of colors `CGMesh::ColorType` - first color corresponds to minimum confidence, last color corresponds to maximum confidence. */
        template <typename ColorGradient>
        inline void convertConfidenceToColor(const ColorGradient& colorGradient)
        {
            static constexpr double confidenceRange = std::numeric_limits<Confidence>::max() - std::numeric_limits<Confidence>::lowest();
            m_colorBuffer.resize(Eigen::NoChange, m_confidenceBuffer.cols());
            for (Eigen::Index i = 0; i < m_confidenceBuffer.cols(); i++)
                m_colorBuffer.col(i) = interpolateColor((double)m_confidenceBuffer[i] / confidenceRange, colorGradient);
        }

        //! Converts the confidence buffer to a corresponding color buffer (interpolates color for each vertex) (linear interpolation)
        inline void convertConfidenceToColor() { convertConfidenceToColor(defaultColorGradient()); }

        //! Creates a new mesh by merging the given meshes together
        /*!
         * \param [in] inputMeshes List of pointers to input meshes
         * \param [in] defaultConfidence Default confidence to use in case no confidence information is available
         * \param [in] defaultVertexColor Default vertex color to use in case no color information is available
         * \return Output mesh
         */
        static inline CGMeshExtended merge(const std::vector<CGMeshExtended const*>& inputMeshes, const Confidence& defaultConfidence = std::numeric_limits<Confidence>::max(), const Eigen::Matrix<ColorChannelType, 4, 1>& defaultVertexColor = (Eigen::Matrix<ColorChannelType, 4, 1>() << 255, 255, 255, 255).finished())
        {
            // Perform merge for base meshes
            std::vector<CGMesh const*> inputBaseMeshes;
            inputBaseMeshes.reserve(inputMeshes.size());
            for (size_t i = 0; i < inputMeshes.size(); i++)
                inputBaseMeshes.push_back(inputMeshes[i]);
            CGMeshExtended outputMesh = broccoli::geometry::CGMeshTools::merge(inputBaseMeshes, defaultVertexColor);

            // Check input meshes
            IndexType vertexBufferSize = 0;
            bool confidenceBufferAvailable = false;
            for (size_t i = 0; i < inputMeshes.size(); i++) {
                assert(inputMeshes[i]->isValid());
                vertexBufferSize += inputMeshes[i]->m_vertexBuffer.cols();
                if (inputMeshes[i]->m_confidenceBuffer.cols() > 0)
                    confidenceBufferAvailable = true;
            }

            // Allocate confidence buffer
            if (confidenceBufferAvailable == true)
                outputMesh.m_confidenceBuffer.resize(Eigen::NoChange, vertexBufferSize);

            // Copy data of confidence buffer
            IndexType currentVertexBufferPosition = 0;
            for (size_t i = 0; i < inputMeshes.size(); i++) {
                if (inputMeshes[i]->m_confidenceBuffer.cols() > 0)
                    outputMesh.m_confidenceBuffer.block(0, currentVertexBufferPosition, 1, inputMeshes[i]->m_confidenceBuffer.cols()) = inputMeshes[i]->m_confidenceBuffer;
                else if (confidenceBufferAvailable == true && inputMeshes[i]->m_vertexBuffer.cols() > 0) {
                    // No confidence available in input mesh, but output mesh requires confidence -> fill with default confidence
                    for (Eigen::Index j = currentVertexBufferPosition; j < currentVertexBufferPosition + inputMeshes[i]->m_vertexBuffer.cols(); j++)
                        outputMesh.m_confidenceBuffer[j] = defaultConfidence;
                }
                currentVertexBufferPosition += inputMeshes[i]->m_vertexBuffer.cols();
            }

            // Pass back output mesh
            return outputMesh;
        }

        //! Creates a new mesh by merging the given two meshes together
        /*!
         * \param [in] firstMesh First mesh to merge
         * \param [in] secondMesh Second mesh to merge
         * \param [in] defaultConfidence Default confidence to use in case no confidence information is available
         * \param [in] defaultVertexColor Default vertex color to use in case no color information is available
         * \return Output mesh
         */
        static inline CGMeshExtended merge(const CGMeshExtended& firstMesh, const CGMeshExtended& secondMesh, const Confidence& defaultConfidence = std::numeric_limits<Confidence>::max(), const Eigen::Matrix<ColorChannelType, 4, 1>& defaultVertexColor = (Eigen::Matrix<ColorChannelType, 4, 1>() << 255, 255, 255, 255).finished()) { return merge(std::vector<CGMeshExtended const*>{ &firstMesh, &secondMesh }, defaultConfidence, defaultVertexColor); }

        //! Computes the maximum (squared) edge length of all triangles
        inline double maximumSquaredEdgeLength() const
        {
            double value = 0.0;
            for (Eigen::Index t = 0; t < m_indexBuffer.cols(); t++) {
                value = std::max((m_vertexBuffer.col(m_indexBuffer(1, t)) - m_vertexBuffer.col(m_indexBuffer(0, t))).squaredNorm(), value); // Edge 0: e0=v1-v0
                value = std::max((m_vertexBuffer.col(m_indexBuffer(2, t)) - m_vertexBuffer.col(m_indexBuffer(0, t))).squaredNorm(), value); // Edge 1: e1=v2-v0
                value = std::max((m_vertexBuffer.col(m_indexBuffer(2, t)) - m_vertexBuffer.col(m_indexBuffer(1, t))).squaredNorm(), value); // Edge 2: e2=v2-v1
            }
            return value;
        }

        //! Performs an **adaptive** subdivision of the mesh depending on local metrics (edge length)
        /*!
         * \param [in] desiredMaximumEdgeLength The desired maximum edge length after this operation
         * \param [in] maximumIterations Maximum count of subdivision iterations (use "0" for no limit)
         * \return The count of performed subdivision iterations (if "0": no subdivision was necessary)
         */
        inline size_t adaptiveSubdivision(const double& desiredMaximumEdgeLength, const size_t& maximumIterations = 0)
        {
            assert(isValid());

            // Initialize helpers
            const double desiredMaximumSquaredEdgeLength = desiredMaximumEdgeLength * desiredMaximumEdgeLength;

            // Determine count of necessary subdivisions
            size_t subdivisions = 0;
            double splittedSquaredEdgeLength = maximumSquaredEdgeLength();
            while (splittedSquaredEdgeLength > desiredMaximumSquaredEdgeLength) {
                subdivisions++;
                splittedSquaredEdgeLength *= 0.25; // Split edge at mid-point -> SQUARED edge length is reduced by 75 percent
            }
            if (maximumIterations > 0 && subdivisions > maximumIterations)
                subdivisions = maximumIterations;

            // Abort, if subdivision is not necessary
            if (subdivisions == 0)
                return 0;

            // Type definitions
            using EdgeKey = std::pair<CGMesh::IndexType, CGMesh::IndexType>; // Unique key of an edge (first: lower vertex index in vertex buffer, second: greater vertex index in vertex buffer)
            auto createEdgeKey = [](const CGMesh::IndexType& firstVertexIndex, const CGMesh::IndexType& secondVertexIndex) -> EdgeKey {
                if (firstVertexIndex < secondVertexIndex)
                    return EdgeKey{ firstVertexIndex, secondVertexIndex };
                else
                    return EdgeKey{ secondVertexIndex, firstVertexIndex };
            };
            struct EdgeKeyHash {
                inline size_t operator()(const EdgeKey& key) const { return ((static_cast<size_t>(key.first) << 32) | static_cast<size_t>(key.second)); }
            };
            struct Edge {
                Edge(const CGMesh::IndexType& firstVertexIndex, const CGMesh::IndexType& secondVertexIndex, const double& lengthSquared)
                    : m_firstVertexIndex(firstVertexIndex)
                    , m_secondVertexIndex(secondVertexIndex)
                    , m_lengthSquared(lengthSquared)
                {
                }
                CGMesh::IndexType m_firstVertexIndex; // Index of first vertex defining this edge
                CGMesh::IndexType m_secondVertexIndex; // Index of second vertex defining this edge
                double m_lengthSquared; // Squared length of this edge
                CGMesh::IndexType m_newVertexIndex; // Index of new created vertex in vertex buffer (only set if this edge needs to be splitted)
            };
            struct Triangle {
                Triangle(const size_t& e0Idx, const size_t& e1Idx, const size_t& e2Idx, const size_t& edgesToSplit)
                    : m_edgeIndices{ { e0Idx, e1Idx, e2Idx } }
                    , m_edgesToSplit(edgesToSplit)
                {
                }
                std::array<size_t, 3> m_edgeIndices; // Indices of edges in edge buffer ([0]: e0=v1-v0, [1]: e1=v2-v0, [2]: e2=v2-v1)
                size_t m_edgesToSplit; // Count of edges to split in this triangle
            };

            // Check which buffers are available
            const bool withNormal = (m_normalBuffer.cols() == m_vertexBuffer.cols());
            const bool withColor = (m_colorBuffer.cols() == m_vertexBuffer.cols());
            const bool withConfidence = (m_confidenceBuffer.cols() == m_vertexBuffer.cols());
            const bool withMaterials = (m_indexBuffer.rows() == 4);

            // Compute maximum count of triangles and edges after this operation
            size_t maximumTriangleCount = m_indexBuffer.cols(); // Initialize with old triangle count
            for (size_t s = 0; s < subdivisions; s++)
                maximumTriangleCount *= 4; // Assume that each triangle is split into 4 sub-triangles ("worst case")
            const size_t maximumEdgeCount = maximumTriangleCount * 3; // Assume 3 edges for each triangle (unlinked triangles -> "worst case")

            // Perform iterative subdivision
            std::vector<Edge> edgeBuffer; // Buffer of all edges (before splitting)
            edgeBuffer.reserve(maximumEdgeCount);
            std::unordered_map<EdgeKey, size_t, EdgeKeyHash> edgeMap; // Mapping of edges (key: edge key, value: index of edge in the edge buffer)
            edgeMap.reserve(maximumEdgeCount);
            std::vector<Triangle> triangleBuffer; // Buffer of all triangles (before splitting)
            triangleBuffer.reserve(maximumTriangleCount);
            for (size_t s = 0; s < subdivisions; s++) {
                // Initialize counters
                Eigen::Index newVertexCount = m_vertexBuffer.cols(); // Count of vertices after this iteration (append vertices to buffer)
                Eigen::Index newTriangleCount = 0; // Count of triangles after this iteration (create new buffer)

                // Reset buffers
                edgeBuffer.clear();
                edgeMap.clear();
                triangleBuffer.clear();

                // Generate buffers (identify edges, link edges to triangles and fill edge buffer and edge map)
                for (Eigen::Index t = 0; t < m_indexBuffer.cols(); t++) {
                    // Get vertex indices of this triangle
                    const CGMesh::IndexType& i0 = m_indexBuffer(0, t);
                    const CGMesh::IndexType& i1 = m_indexBuffer(1, t);
                    const CGMesh::IndexType& i2 = m_indexBuffer(2, t);

                    // Process triangle
                    size_t edgesToSplitInThisTriangle = 0; // Count of edges to be split in this triangle
                    auto processEdge = [&](const CGMesh::IndexType& firstVertexIndex, const CGMesh::IndexType& secondVertexIndex) -> size_t {
                        auto insertResult = edgeMap.insert({ createEdgeKey(firstVertexIndex, secondVertexIndex), edgeBuffer.size() });
                        if (insertResult.second == true) {
                            // Edge has not been added to the buffer yet -> create new edge in edge buffer
                            edgeBuffer.emplace_back(firstVertexIndex, secondVertexIndex, (m_vertexBuffer.col(firstVertexIndex) - m_vertexBuffer.col(secondVertexIndex)).squaredNorm());
                            if (edgeBuffer.back().m_lengthSquared > desiredMaximumSquaredEdgeLength) {
                                // This edge will be splitted
                                edgeBuffer.back().m_newVertexIndex = newVertexCount++;
                                edgesToSplitInThisTriangle++;
                            }
                        } else {
                            // Edge is already in buffer
                            if (edgeBuffer[insertResult.first->second].m_lengthSquared > desiredMaximumSquaredEdgeLength)
                                edgesToSplitInThisTriangle++;
                        }
                        return insertResult.first->second; // Pass back index of edge in edge buffer
                    };
                    const size_t e0Idx = processEdge(i0, i1); // Edge 0: e0=v1-v0
                    const size_t e1Idx = processEdge(i0, i2); // Edge 1: e1=v2-v0
                    const size_t e2Idx = processEdge(i1, i2); // Edge 2: e2=v2-v1
                    triangleBuffer.emplace_back(e0Idx, e1Idx, e2Idx, edgesToSplitInThisTriangle);
                    newTriangleCount += 1 + edgesToSplitInThisTriangle;
                }

                // Abort, if no edge has to be split (same triangle count)
                if (newTriangleCount == m_indexBuffer.cols())
                    return s; // Return count of iterations so far

                // Allocate memory for new mesh
                m_vertexBuffer.conservativeResize(Eigen::NoChange, newVertexCount);
                if (withNormal == true)
                    m_normalBuffer.conservativeResize(Eigen::NoChange, newVertexCount);
                if (withColor == true)
                    m_colorBuffer.conservativeResize(Eigen::NoChange, newVertexCount);
                if (withConfidence == true)
                    m_confidenceBuffer.conservativeResize(Eigen::NoChange, newVertexCount);
                decltype(m_indexBuffer) oldIndexBuffer = m_indexBuffer;
                m_indexBuffer.resize(Eigen::NoChange, newTriangleCount);

                // Compute new per-vertex buffers
                for (size_t e = 0; e < edgeBuffer.size(); e++) {
                    const Edge& edge = edgeBuffer[e];
                    if (edge.m_lengthSquared > desiredMaximumSquaredEdgeLength) {
                        // This edge is splitted
                        m_vertexBuffer.col(edge.m_newVertexIndex) = 0.5 * (m_vertexBuffer.col(edge.m_firstVertexIndex) + m_vertexBuffer.col(edge.m_secondVertexIndex));
                        if (withNormal == true)
                            m_normalBuffer.col(edge.m_newVertexIndex) = (m_normalBuffer.col(edge.m_firstVertexIndex) + m_normalBuffer.col(edge.m_secondVertexIndex)).normalized();
                        if (withColor == true) {
                            for (Eigen::Index c = 0; c < 4; c++)
                                m_colorBuffer(c, edge.m_newVertexIndex) = broccoli::core::math::clamp(std::lround(0.5 * (((double)m_colorBuffer(c, edge.m_firstVertexIndex)) + ((double)m_colorBuffer(c, edge.m_secondVertexIndex)))), (long)0, (long)255);
                        }
                        if (withConfidence == true)
                            m_confidenceBuffer[edge.m_newVertexIndex] = broccoli::core::math::clamp(std::lround(0.5 * (((double)m_confidenceBuffer[edge.m_firstVertexIndex]) + ((double)m_confidenceBuffer[edge.m_secondVertexIndex]))), (long)0, (long)255);
                    }
                }

                // Iterate through "old" index buffer to assemble new index buffer
                size_t currentTriangleIndex = 0;
                for (Eigen::Index t = 0; t < oldIndexBuffer.cols(); t++) {
                    // Helper to add a triangle to the mesh
                    auto addTriangle = [&](const CGMesh::IndexType& v0, const CGMesh::IndexType& v1, const CGMesh::IndexType& v2) {
                        m_indexBuffer(0, currentTriangleIndex) = v0;
                        m_indexBuffer(1, currentTriangleIndex) = v1;
                        m_indexBuffer(2, currentTriangleIndex) = v2;
                        if (withMaterials == true)
                            m_indexBuffer(3, currentTriangleIndex) = oldIndexBuffer(3, t); // Copy material index
                        currentTriangleIndex++;
                    };

                    // Subdivide triangle
                    const Triangle& triangle = triangleBuffer[t];
                    const CGMesh::IndexType& i0 = oldIndexBuffer(0, t);
                    const CGMesh::IndexType& i1 = oldIndexBuffer(1, t);
                    const CGMesh::IndexType& i2 = oldIndexBuffer(2, t);
                    if (triangle.m_edgesToSplit == 0)
                        addTriangle(i0, i1, i2); // No edges to split -> just use original triangle
                    else {
                        const Edge& e0 = edgeBuffer[triangle.m_edgeIndices[0]];
                        const Edge& e1 = edgeBuffer[triangle.m_edgeIndices[1]];
                        const Edge& e2 = edgeBuffer[triangle.m_edgeIndices[2]];
                        if (triangle.m_edgesToSplit == 1) {
                            // One edge to split
                            if (e0.m_lengthSquared > desiredMaximumSquaredEdgeLength) {
                                // New vertex is on e0
                                addTriangle(i0, e0.m_newVertexIndex, i2);
                                addTriangle(i1, i2, e0.m_newVertexIndex);
                            } else if (e1.m_lengthSquared > desiredMaximumSquaredEdgeLength) {
                                // New vertex is on e1
                                addTriangle(i0, i1, e1.m_newVertexIndex);
                                addTriangle(i1, i2, e1.m_newVertexIndex);
                            } else {
                                // New vertex is on e2
                                addTriangle(i0, i1, e2.m_newVertexIndex);
                                addTriangle(i0, e2.m_newVertexIndex, i2);
                            }
                        } else if (triangle.m_edgesToSplit == 2) {
                            // Two edges to split
                            if (e2.m_lengthSquared <= desiredMaximumSquaredEdgeLength) {
                                // New vertices are on e0 and e1
                                addTriangle(i0, e0.m_newVertexIndex, e1.m_newVertexIndex);
                                addTriangle(e0.m_newVertexIndex, i1, e1.m_newVertexIndex);
                                addTriangle(i1, i2, e1.m_newVertexIndex);
                            } else if (e1.m_lengthSquared <= desiredMaximumSquaredEdgeLength) {
                                // New vertices are on e0 and e2
                                addTriangle(i0, e0.m_newVertexIndex, e2.m_newVertexIndex);
                                addTriangle(e0.m_newVertexIndex, i1, e2.m_newVertexIndex);
                                addTriangle(i0, e2.m_newVertexIndex, i2);
                            } else {
                                // New vertices are on e1 and e2
                                addTriangle(i0, e2.m_newVertexIndex, e1.m_newVertexIndex);
                                addTriangle(i0, i1, e2.m_newVertexIndex);
                                addTriangle(e1.m_newVertexIndex, e2.m_newVertexIndex, i2);
                            }
                        } else {
                            // All edges to split
                            addTriangle(i0, e0.m_newVertexIndex, e1.m_newVertexIndex);
                            addTriangle(e0.m_newVertexIndex, e2.m_newVertexIndex, e1.m_newVertexIndex);
                            addTriangle(e0.m_newVertexIndex, i1, e2.m_newVertexIndex);
                            addTriangle(e1.m_newVertexIndex, e2.m_newVertexIndex, i2);
                        }
                    }
                }
            }

            // Pass back count of subdivisions
            return subdivisions;
        }

        // Helpers
        // -------
    protected:
        //! Compares the given matrices allowing a certain tolerance
        template <typename Derived, typename ToleranceValue>
        static inline bool compareMatrixWithTolerance(const Eigen::MatrixBase<Derived>& first, const Eigen::MatrixBase<Derived>& second, const ToleranceValue& tolerance)
        {
            if (first.rows() != second.rows() || first.cols() != second.cols())
                return false;
            for (Eigen::Index i = 0; i < first.rows(); i++) {
                for (Eigen::Index j = 0; j < first.cols(); j++) {
                    if (first(i, j) > second(i, j)) {
                        if (first(i, j) - second(i, j) > tolerance)
                            return false;
                    } else {
                        if (second(i, j) - first(i, j) > tolerance)
                            return false;
                    }
                }
            }
            return true;
        }

        //! Creates a default color gradient (red, green, blue)
        static inline std::array<CGMesh::ColorType, 3> createDefaultColorGradient()
        {
            std::array<CGMesh::ColorType, 3> gradient;
            gradient[0] << 255, 0, 0, 255;
            gradient[1] << 0, 255, 0, 255;
            gradient[2] << 0, 0, 255, 255;
            return gradient;
        }

        //! Returns reference to global instance of default color gradient
        static inline const std::array<CGMesh::ColorType, 3>& defaultColorGradient()
        {
            static const std::array<CGMesh::ColorType, 3> gradient = createDefaultColorGradient();
            return gradient;
        }

    public:
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW // <-- Proper 128 bit alignment of member data necessary for Eigen vectorization
    };

} // namespace vision
} // namespace am2b
