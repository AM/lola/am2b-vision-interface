/*
 * This file is part of am2b.
 * Copyright (C) 2021 Chair of Applied Mechanics, Technical University of Munich
 * https://www.mw.tum.de/am
 */

#pragma once // Load this file only once

// Standard libraries
#include <assert.h>
#include <stdint.h>
#include <string>

// External libraries
// ...

// Internal libraries
// ...

namespace am2b {
namespace vision {
    //! Database specifying all high-level signals between the vision system and the control system (bi-directional)
    class SignalDataBase {
    public:
        // Type specifications
        using DataTypeType = uint8_t;
        using IDType = uint32_t;

        //! Specification of the data type (type of data attached to signal)
        enum class DataType : DataTypeType {
            UNKNOWN = 0, //!< Unknown data type
            EMPTY, //!< Without any data
            BOOL, //!< Boolean
            UINT8, //!< Unsigned 8-bit integer
            UINT16, //!< Unsigned 16-bit integer
            UINT32, //!< Unsigned 32-bit integer
            UINT64, //!< Unsigned 64-bit integer
            INT8, //!< Signed 8-bit integer
            INT16, //!< Signed 16-bit integer
            INT32, //!< Signed 32-bit integer
            INT64, //!< Signed 64-bit integer
            FLOAT, //!< Floating point data (32-bit)
            DOUBLE, //!< Floating point data (64-bit)
            STRING, //!< String data
            DATATYPE_COUNT //!< Total count of elements
        };

        //! Returns the string representation of the given data type
        static inline std::string toString(const DataType& type)
        {
            if (type == DataType::UNKNOWN)
                return "UNKNOWN";
            if (type == DataType::EMPTY)
                return "EMPTY";
            if (type == DataType::BOOL)
                return "BOOL";
            if (type == DataType::UINT8)
                return "UINT8";
            if (type == DataType::UINT16)
                return "UINT16";
            if (type == DataType::UINT32)
                return "UINT32";
            if (type == DataType::UINT64)
                return "UINT64";
            if (type == DataType::INT8)
                return "INT8";
            if (type == DataType::INT16)
                return "INT16";
            if (type == DataType::INT32)
                return "INT32";
            if (type == DataType::INT64)
                return "INT64";
            if (type == DataType::FLOAT)
                return "FLOAT";
            if (type == DataType::DOUBLE)
                return "DOUBLE";
            if (type == DataType::STRING)
                return "STRING";

            // Unknown selection
            assert(false);
            return "UNKNOWN";
        }

        //! Returns the datatype from the given static type
        template <typename T>
        static constexpr DataType dataTypeFromStaticType()
        {
            if (std::is_same<T, bool>::value)
                return DataType::BOOL;
            if (std::is_same<T, uint8_t>::value)
                return DataType::UINT8;
            if (std::is_same<T, uint16_t>::value)
                return DataType::UINT16;
            if (std::is_same<T, uint32_t>::value)
                return DataType::UINT32;
            if (std::is_same<T, uint64_t>::value)
                return DataType::UINT64;
            if (std::is_same<T, int8_t>::value)
                return DataType::INT8;
            if (std::is_same<T, int16_t>::value)
                return DataType::INT16;
            if (std::is_same<T, int32_t>::value)
                return DataType::INT32;
            if (std::is_same<T, int64_t>::value)
                return DataType::INT64;
            if (std::is_same<T, float>::value)
                return DataType::FLOAT;
            if (std::is_same<T, double>::value)
                return DataType::DOUBLE;
            if (std::is_same<T, std::string>::value)
                return DataType::STRING;
            return DataType::UNKNOWN;
        }

        //! List of all signal IDs
        enum class ID : IDType {
            UNKNOWN = 0, //!< Unknown signal
            // Signals from control to vision
            VISION_LOG_START, //!< Signal to vision: start logging
            VISION_LOG_STOP, //!< Signal to vision: stop logging
            VISION_CALIBRATION_START, //!< Signal to vision: start calibration procedure
            VISION_RECONSTRUCTION_START, //!< Signal to vision: start scene reconstruction
            VISION_RECONSTRUCTION_STOP, //!< Signal to vision: stop scene reconstruction
            VISION_SCENE_RESET, //!< Signal to vision: clear and reset the complete scene
            VISION_SCENE_UPDATE_CYCLIC_START, //!< Signal to vision: start cyclic sending of scene updates
            VISION_SCENE_UPDATE_CYCLIC_STOP, //!< Signal to vision: stop cyclic sending of scene updates
            VISION_SCENE_UPDATE_SINGLE, //!< Signal to vision: send a single scene update
            // Signals from vision to control
            CONTROL_LOG_STARTED, //!< Signal to control: started logging
            CONTROL_LOG_STOPPED, //!< Signal to control: stopped logging
            CONTROL_CALIBRATION_STARTED, //!< Signal to control: started calibration procedure
            CONTROL_CALIBRATION_FINISHED, //!< Signal to control: finished calibration procedure
            CONTROL_RECONSTRUCTION_STARTED, //!< Signal to control: started scene reconstruction
            CONTROL_RECONSTRUCTION_STOPPED, //!< Signal to control: stopped scene reconstruction
            CONTROL_SCENE_RESET, //!< Signal to control: clear and reset the complete scene
            // Misc:
            ID_COUNT //!< Total count of elements
        };

        //! Returns the string representation of the given ID
        static inline std::string toString(const ID& id)
        {
            if (id == ID::UNKNOWN)
                return "UNKNOWN";

            // Signals from control to vision
            if (id == ID::VISION_LOG_START)
                return "VISION_LOG_START";
            if (id == ID::VISION_LOG_STOP)
                return "VISION_LOG_STOP";
            if (id == ID::VISION_CALIBRATION_START)
                return "VISION_CALIBRATION_START";
            if (id == ID::VISION_RECONSTRUCTION_START)
                return "VISION_RECONSTRUCTION_START";
            if (id == ID::VISION_RECONSTRUCTION_STOP)
                return "VISION_RECONSTRUCTION_STOP";
            if (id == ID::VISION_SCENE_RESET)
                return "VISION_SCENE_RESET";
            if (id == ID::VISION_SCENE_UPDATE_CYCLIC_START)
                return "VISION_SCENE_UPDATE_CYCLIC_START";
            if (id == ID::VISION_SCENE_UPDATE_CYCLIC_STOP)
                return "VISION_SCENE_UPDATE_CYCLIC_STOP";
            if (id == ID::VISION_SCENE_UPDATE_SINGLE)
                return "VISION_SCENE_UPDATE_SINGLE";

            // Signals from vision to control
            if (id == ID::CONTROL_LOG_STARTED)
                return "CONTROL_LOG_STARTED";
            if (id == ID::CONTROL_LOG_STOPPED)
                return "CONTROL_LOG_STOPPED";
            if (id == ID::CONTROL_CALIBRATION_STARTED)
                return "CONTROL_CALIBRATION_STARTED";
            if (id == ID::CONTROL_CALIBRATION_FINISHED)
                return "CONTROL_CALIBRATION_FINISHED";
            if (id == ID::CONTROL_RECONSTRUCTION_STARTED)
                return "CONTROL_RECONSTRUCTION_STARTED";
            if (id == ID::CONTROL_RECONSTRUCTION_STOPPED)
                return "CONTROL_RECONSTRUCTION_STOPPED";
            if (id == ID::CONTROL_SCENE_RESET)
                return "CONTROL_SCENE_RESET";

            // Unknown selection
            assert(false);
            return "UNKNOWN";
        }

        //! Returns the signal ID from the given string representation
        static inline ID idFromString(const std::string& id)
        {
            if (id == "UNKNOWN")
                return ID::UNKNOWN;

            // Signals from control to vision
            if (id == "VISION_LOG_START")
                return ID::VISION_LOG_START;
            if (id == "VISION_LOG_STOP")
                return ID::VISION_LOG_STOP;
            if (id == "VISION_CALIBRATION_START")
                return ID::VISION_CALIBRATION_START;
            if (id == "VISION_RECONSTRUCTION_START")
                return ID::VISION_RECONSTRUCTION_START;
            if (id == "VISION_RECONSTRUCTION_STOP")
                return ID::VISION_RECONSTRUCTION_STOP;
            if (id == "VISION_SCENE_RESET")
                return ID::VISION_SCENE_RESET;
            if (id == "VISION_SCENE_UPDATE_CYCLIC_START")
                return ID::VISION_SCENE_UPDATE_CYCLIC_START;
            if (id == "VISION_SCENE_UPDATE_CYCLIC_STOP")
                return ID::VISION_SCENE_UPDATE_CYCLIC_STOP;
            if (id == "VISION_SCENE_UPDATE_SINGLE")
                return ID::VISION_SCENE_UPDATE_SINGLE;

            // Signals from vision to control
            if (id == "CONTROL_LOG_STARTED")
                return ID::CONTROL_LOG_STARTED;
            if (id == "CONTROL_LOG_STOPPED")
                return ID::CONTROL_LOG_STOPPED;
            if (id == "CONTROL_CALIBRATION_STARTED")
                return ID::CONTROL_CALIBRATION_STARTED;
            if (id == "CONTROL_CALIBRATION_FINISHED")
                return ID::CONTROL_CALIBRATION_FINISHED;
            if (id == "CONTROL_RECONSTRUCTION_STARTED")
                return ID::CONTROL_RECONSTRUCTION_STARTED;
            if (id == "CONTROL_RECONSTRUCTION_STOPPED")
                return ID::CONTROL_RECONSTRUCTION_STOPPED;
            if (id == "CONTROL_SCENE_RESET")
                return ID::CONTROL_SCENE_RESET;

            // Unknown selection
            assert(false);
            return ID::UNKNOWN;
        }

        //! Returns the data type associated with the given signal id
        static inline DataType dataTypeFromID(const ID& id)
        {
            if (id == ID::UNKNOWN)
                return DataType::UNKNOWN; // UNKNOWN

            // Signals from control to vision
            if (id == ID::VISION_LOG_START)
                return DataType::INT64; // Start-time of log in seconds relative to 00:00:00 UTC on 1 January 1970 (Unix Epoch)
            if (id == ID::VISION_LOG_STOP)
                return DataType::INT64; // Stop-time of log in seconds relative to 00:00:00 UTC on 1 January 1970 (Unix Epoch)
            if (id == ID::VISION_CALIBRATION_START)
                return DataType::EMPTY; // ...
            if (id == ID::VISION_RECONSTRUCTION_START)
                return DataType::EMPTY; // ...
            if (id == ID::VISION_RECONSTRUCTION_STOP)
                return DataType::EMPTY; // ...
            if (id == ID::VISION_SCENE_RESET)
                return DataType::EMPTY; // ...
            if (id == ID::VISION_SCENE_UPDATE_CYCLIC_START)
                return DataType::DOUBLE; // Cycle time in seconds (if <= 0.0, then use default cycle time)
            if (id == ID::VISION_SCENE_UPDATE_CYCLIC_STOP)
                return DataType::EMPTY; // ...
            if (id == ID::VISION_SCENE_UPDATE_SINGLE)
                return DataType::EMPTY; // ...

            // Signals from vision to control
            if (id == ID::CONTROL_LOG_STARTED)
                return DataType::EMPTY; // ...
            if (id == ID::CONTROL_LOG_STOPPED)
                return DataType::BOOL; // true on success, false otherwise
            if (id == ID::CONTROL_CALIBRATION_STARTED)
                return DataType::EMPTY; // ...
            if (id == ID::CONTROL_CALIBRATION_FINISHED)
                return DataType::BOOL; // true on success, false otherwise
            if (id == ID::CONTROL_RECONSTRUCTION_STARTED)
                return DataType::EMPTY; // ...
            if (id == ID::CONTROL_RECONSTRUCTION_STOPPED)
                return DataType::EMPTY; // ...
            if (id == ID::CONTROL_SCENE_RESET)
                return DataType::EMPTY; // ...

            // Unknown selection
            assert(false);
            return DataType::UNKNOWN;
        }
    };
} // namespace vision
} // namespace am2b
