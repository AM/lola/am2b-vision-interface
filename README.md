# am2b-vision-interface
This project represents a specification and implementation of the interface between a computer vision system and the walking pattern generation system of the humanoid robot LOLA developed at the Chair of Applied Mechanics, Technical University of Munich (https://www.mec.ed.tum.de/en/am/research/current-projects/robotics/humanoid-robot-lola/).

## Dependencies
 * Open-source library **broccoli** (https://gitlab.lrz.de/AM/broccoli) (MIT License)
 * Open-source library **cpptoml** (https://github.com/skystrife/cpptoml.git) (MIT License)

## Citation
For citation in scientific papers, please use the following BibLaTeX entry:
```
@ELECTRONIC{Am2bVisionInterface,
  author = {Seiwald, Philipp},
  year = {2022},
  title = {{am2b-vision-interface: Interface between a Computer Vision System and the Walking Pattern Generation System of the Humanoid Robot LOLA}},
  url = {https://gitlab.lrz.de/AM/lola/am2b-vision-interface},
  doi = {10.14459/2021mp1686394}
}
```

## Acknowledgements
This work was supported by the German Research Foundation (DFG) (grant number [407378162](https://gepris.dfg.de/gepris/projekt/407378162)).
